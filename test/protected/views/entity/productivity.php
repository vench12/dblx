﻿<?php
$this->pageTitle = 'Производительность единиц-объектов | ' . Yii::app()->name;
$this->breadcrumbs = array('Единицы-объекты' => array('/entity'), 'Производительность единиц-объектов');
?>
<h1>Производительность единицы-объекта: <small><?= $modelEntity->name; ?></small></h1>

<?php if($modelEntity->use_process != 1) { ?>
	<p>Объект не учувствует в процессе построения графика, поэтому он не зависим от материалов.</p>
	<p>Что бы сделать объект зависимым от материалов установите ему флаг «Объект использует процесс»</p>
<?php 
	return;
} ?>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/entity/productivity'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
		<?= $form->hiddenField($modelEntity, 'entity_id'); ?>
		
		<?= $form->dropDownList($model, 'material_id', Materials::model()->listMaterials(), array('empty' => '--Выбор материала--', 'class' => 'chzn-select')); ?>
		
		<?= $form->textFieldRow($model, 'productivity', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		
		<?= $form->dropDownList($model, 'unit_id', Units::model()->listUnits(), array('empty' => '--Выбор ед. изм.--', 'class' => 'chzn-select')); ?>
		
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
        $this->endWidget(); 
		?>
	</div>
	<div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить производительность',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('/entity/productivityadd', 'id' => $modelEntity->getPrimaryKey()),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search($modelEntity->getPrimaryKey()),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'material_id',
					'value' => '$data->materials->name',
					'filter' => Materials::model()->listMaterials(),
				),
				array(
					'name' => 'productivity',
				),
				array(
					'name' => 'unit_id',
					'value' => '$data->units->name',
					'filter' => Units::model()->listUnits(),
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{productivityedit}',
					'buttons' => array
					(
						'productivityedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/entity/productivityedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>
