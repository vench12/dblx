<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'entity_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>		
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'unit_id', Units::model()->listUnits(), array('class' => 'span5', 'empty' => '--Выбор ед. изм.--'));  ?>
	</div>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'entity_type_id', EntityType::model()->listTypes(), array('class' => 'span5', 'empty' => '--Выбор типа--'));  ?>
	</div>
	
	<div>
		<?php echo $form->checkBoxRow($model, 'use_process'); ?>
		<p>Если установлен флаг, то объект будет принимать участие в графике работ, время которого будет рассчитываться исходя из производительности и количества.</p>
	</div>
        <div>
		<?php echo $form->checkBoxRow($model, 'use_mobile'); ?>
		<p>Если установлен флаг, то будет доступна мобилизация объекта в проекте.</p>
	</div>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('entity/entityRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>
