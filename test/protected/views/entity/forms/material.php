<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'material_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>		
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<?php echo $form->textFieldRow($model, 'coeff_of_loss', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<?php echo $form->checkboxRow($model, 'is_backfill', array()); ?>
	</div>
	<p>Если установлен флаг, то материал будет доступен в списке материалов для засыпки.</p>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('entity/materialRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>
