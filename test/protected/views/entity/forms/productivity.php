﻿<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'productivity_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>		
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'material_id', Materials::model()->listMaterials(), array('class' => 'span5', 'empty' => '--Выбор материала--'));  ?>
	</div>
	
	<div>
		<?php echo $form->textFieldRow($model, 'productivity', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'unit_id', Units::model()->listUnits(), array('class' => 'span5', 'empty' => '--Выбор ед. изм.--'));  ?>
	</div>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('entity/productivityRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>
