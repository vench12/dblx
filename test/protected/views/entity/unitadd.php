<?php
$this->pageTitle = 'Добавить единицу измерения | ' . Yii::app()->name;
$this->breadcrumbs = array('Единицы измерения' => array('/entity/units'), 'Добавить единицу измерения');
?>
<h1>Добавить единицу измерения</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/unit', array(
			'model' => $model,
		));
		?>
	</div>
</div>
