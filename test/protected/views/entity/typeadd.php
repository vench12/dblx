<?php
$this->pageTitle = 'Добавить тип объекта | ' . Yii::app()->name;
$this->breadcrumbs = array('Типы объектов' => array('/entity/types'), 'Добавить тип объекта');
?>
<h1>Добавить тип объекта</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/type', array(
			'model' => $model,
		));
		?>
	</div>
</div>
