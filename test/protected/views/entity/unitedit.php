<?php
$this->pageTitle = 'Редактирование единицы измерения | ' . Yii::app()->name;
$this->breadcrumbs = array('Единицы измерения' => array('/entity/units'), 'Редактирование единицы измерения');
?>
<h1>Единица измерения: <small><?= $model->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/unit', array(
			'model' => $model,
		));
		?>
	</div>
</div>
