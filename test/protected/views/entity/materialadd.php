<?php
$this->pageTitle = 'Добавить материал | ' . Yii::app()->name;
$this->breadcrumbs = array('Материалы' => array('/entity/materials'), 'Добавить материал');
?>
<h1>Добавить материал</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/material', array(
			'model' => $model,
		));
		?>
	</div>
</div>
