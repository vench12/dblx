<?php
$this->pageTitle = 'Единицы-объекты ';
$this->breadcrumbs = array('Единицы-объекты');
?>
<h1>Единицы-объекты</h1>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/entity'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
		<?= $form->textFieldRow($model, 'name', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		
		<?= $form->dropDownList($model, 'unit_id', Units::model()->listUnits(), array('empty' => '--Выбор ед. изм.--', 'class' => 'chzn-select')); ?>
		
		<?= $form->dropDownList($model, 'entity_type_id', EntityType::model()->listTypes(), array('empty' => '--Выбор типа--', 'class' => 'chzn-select')); ?>
		
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
        $this->endWidget(); 
		?>
	</div>
	<div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить единицу-объект',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('entity/entityadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'entity_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 30px'),
				),
				array(
					'name' => 'name',
				),
				array(
					'name' => 'unit_id',
					'value' => '$data->units->name',
					'filter' => Units::model()->listUnits(),
				),
				array(
					'name' => 'entity_type_id',
					'value' => '!is_null($data->entityType) ? $data->entityType->name : ""',
					'filter' => EntityType::model()->listTypes(),
				),
				array
				(
					'header' => 'Продуктивность демонтажа',
					'class' => 'CButtonColumn',
					'template' => '{productivity}',
					'buttons' => array
					(
						'productivity' => array
						(
							'label' => 'Продуктивность демонтажа',							
							'url' => 'Yii::app()->createUrl("/entity/productivity", array("Entity[entity_id]" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '220px',
					),
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{entityedit}',
					'buttons' => array
					(
						'entityedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/entity/entityedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('filter_form', "   
	chosenDestroy();
	$('#filter_form button:reset').click(function () {
		chosenReset();
	});
");
?>
