<?php
$this->pageTitle = 'Редактирование типа объекта | ' . Yii::app()->name;
$this->breadcrumbs = array('Типы объектов' => array('/entity/types'), 'Редактирование типа объекта');
?>
<h1>Тип объекта: <small><?= $model->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/type', array(
			'model' => $model,
		));
		?>
	</div>
</div>
