﻿<?php
$this->pageTitle = 'Добавить производительность | ' . Yii::app()->name;
$this->breadcrumbs = array('Единицы-объекты' => array('/entity'), $modelEntity->name => array('/entity/productivity', 'Entity[entity_id]' => $modelEntity->getPrimaryKey()), 'Добавить производительность');
?>
<h1>Добавить производительность единицы-объекта: <small><?= $modelEntity->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/productivity', array(
			'model' => $model,
		));
		?>
	</div>
</div>
