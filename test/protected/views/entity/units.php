<?php
$this->pageTitle = 'Единицы измерения | ' . Yii::app()->name;
$this->breadcrumbs = array('Единицы измерения');
?>
<h1>Единицы измерения</h1>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/entity/units'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
		<?= $form->textFieldRow($model, 'name', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		<?= $form->textFieldRow($model, 'name_short', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
        $this->endWidget(); 
		?>
	</div>
	<div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить единицу измерения',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('entity/unitadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'unit_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 30px'),
				),
				array(
					'name' => 'name',
				),
				array(
					'name' => 'name_short',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{unitedit}',
					'buttons' => array
					(
						'unitedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/entity/unitedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>
