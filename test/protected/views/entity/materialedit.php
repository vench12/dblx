<?php
$this->pageTitle = 'Редактирование материала | ' . Yii::app()->name;
$this->breadcrumbs = array('Материалы' => array('/entity/materials'), 'Редактирование материала');
?>
<h1>Материал: <small><?= $model->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/material', array(
			'model' => $model,
		));
		?>
	</div>
</div>
