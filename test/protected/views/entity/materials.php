<?php
$this->pageTitle = 'Материалы | ' . Yii::app()->name;
$this->breadcrumbs = array('Материалы');
?>
<h1>Материалы</h1>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/entity/materials'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
		<?= $form->textFieldRow($model, 'name', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		<?= $form->textFieldRow($model, 'coeff_of_loss', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
        $this->endWidget(); 
		?>
	</div>
	<div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить материал',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('entity/materialadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'material_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 30px'),
				),
				array(
					'name' => 'name',
				),
				array(
					'name' => 'coeff_of_loss',
				),
				array(
					'name' => 'is_backfill',
					'value' => '$data->isBackfill()',
					'htmlOptions' => array(
						'class' => 'text-center',
					),
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{materialedit}',
					'buttons' => array
					(
						'materialedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/entity/materialedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>
