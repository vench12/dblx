<?php
$this->pageTitle = 'Добавить единицу-объект | ' . Yii::app()->name;
$this->breadcrumbs = array('Единицы-объекты' => array('/entity'), 'Добавить единицу-объект');
?>
<h1>Добавить единицу-объект</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/entity', array(
			'model' => $model,
		));
		?>
	</div>
</div>
