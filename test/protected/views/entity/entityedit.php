<?php
$this->pageTitle = 'Редактирование единицы-объекта | ' . Yii::app()->name;
$this->breadcrumbs = array('Единицы-объекты' => array('/entity'), 'Редактирование единицы-объекта');
?>
<h1>Единица-объект: <small><?= $model->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/entity', array(
			'model' => $model,
		));
		?>
	</div>
</div>