<?php
$this->pageTitle = 'Редактирование города | ' . Yii::app()->name;
$this->breadcrumbs = array('Регионы' => array('/city'), 'Редактирование города');
?>
<h1>Город: <small><?= $model->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/city', array(
			'model' => $model,
		));
		?>
	</div>
</div>
