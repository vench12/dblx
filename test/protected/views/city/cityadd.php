<?php
$this->pageTitle = 'Добавить город | ' . Yii::app()->name;
$this->breadcrumbs = array('Города' => array('/city'), 'Добавить город');
?>
<h1>Добавить город</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/city', array(
			'model' => $model,
		));
		?>
	</div>
</div>
