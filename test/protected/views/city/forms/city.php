<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'region_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>		
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?= $form->dropDownListRow($model, 'region_id', CHtml::listData(Regions::model()->findAll(), 'region_id', 'name'), array('empty' => '--Выбор региона--', 'class' => 'span5 chzn-select')); ?>
	</div>
	
	<div>
		<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('city/cityRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('filter_form', "   
	chosenDestroy();
	$('#filter_form button:reset').click(function () {
		chosenReset();
	});
");
?>
