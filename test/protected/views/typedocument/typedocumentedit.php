<?php
$this->pageTitle = 'Редактирование типа документов | ' . Yii::app()->name;
$this->breadcrumbs = array('Типы документов' => array('/typedocument'), 'Редактирование типа документов');
?>
<h1>Тип документов: <small><?= $model->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/typedocument', array(
			'model' => $model,
		));
		?>
	</div>
</div>
