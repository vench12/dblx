<?php
$this->pageTitle = 'Добавить тип документов | ' . Yii::app()->name;
$this->breadcrumbs = array('Типы документов' => array('/typedocument'), 'Добавить тип документов');
?>
<h1>Добавить тип документов</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/typedocument', array(
			'model' => $model,
		));
		?>
	</div>
</div>
