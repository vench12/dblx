<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
	<title><?= CHtml::encode($this->pageTitle.' | ' . Yii::app()->name); ?></title>
    
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl;?>/css/chosen/chosen.css" />
    
    <?php Yii::app()->getClientScript()->registerScriptFile( Yii::app()->request->baseUrl.'/js/main.js'); ?>
    <?php Yii::app()->getClientScript()->registerScriptFile( Yii::app()->request->baseUrl.'/js/chosen.jquery.min.js'); ?>
    <?php Yii::app()->clientScript->registerScript(
            'tab_choice', "$('.btTabs a:contains(\"".CHttpRequest::getParam("tab")."\")').tab('show');", 
            CClientScript::POS_READY); ?>   
    
    <?php Yii::app()->getClientScript()->registerCssFile( Yii::app()->request->baseUrl.'/themes/light/css/style.css?1'); ?>
    
</head>



<body>
    

 
<?php
$this->widget('bootstrap.widgets.TbNavbar', array(
	'type' => 'inverse', // null or 'inverse'
	'brand' => Yii::app()->name,
	'brandUrl' => Yii::app()->homeUrl,
	'collapse' => true,
	'fluid' => false,
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => array(
				array('label' => 'Проекты', 'url' => array('/project'), 'visible' => !Yii::app()->user->isGuest, 'active' => Yii::app()->controller->getId() == 'project'),
				array('label' => 'Поставщики', 'url' => array('/provider'), 'visible' => !Yii::app()->user->isGuest, 'active' => Yii::app()->controller->getId() == 'provider'),
				array('label' => 'Админ-панель', 'url'=>'#', 'items' => array(
					array('label' => 'Пользователи'),
					array('label' => 'Пользователи', 'url' => array('/adminUsers/users/index')),
					'---',
					array('label' => 'Единицы'),
					array('label' => 'Единицы объекты', 'url' => array('/entity'), 'active' => Yii::app()->controller->getId() == 'entity' && Yii::app()->controller->action->getId() == 'index'),
					array('label' => 'Единицы измерения', 'url' => array('/entity/units'), 'active' => Yii::app()->controller->getId() == 'entity' && Yii::app()->controller->action->getId() == 'units'),
					array('label' => 'Типы объектов', 'url' => array('/entity/types'), 'active' => Yii::app()->controller->getId() == 'entity' && Yii::app()->controller->action->getId() == 'types'),
					array('label' => 'Материалы', 'url' => array('/entity/materials'), 'active' => Yii::app()->controller->getId() == 'entity' && Yii::app()->controller->action->getId() == 'materials'),
					'---',
					array('label' => 'Шаблоны'),
					array('label' => 'Шаблоны', 'url' => array('/template'), 'active' => Yii::app()->controller->getId() == 'template'),
					array('label' => 'Типы зданий', 'url' => array('/typebuilding'), 'active' => Yii::app()->controller->getId() == 'typebuilding'),
					array('label' => 'Типы строительных площадок', 'url' => array('/typebuildingplace'), 'active' => Yii::app()->controller->getId() == 'typebuildingplace'),
					'---',
					array('label' => 'Документация и параметры проекта'),
					array('label' => 'Типы документов', 'url' => array('/typedocument'), 'active' => Yii::app()->controller->getId() == 'typedocument'),
					array('label' => 'Параметры проекта (вопросы)', 'url' => array('/question'), 'active' => Yii::app()->controller->getId() == 'question'),
					array('label' => 'Отношения типов док-ов и пар-ов проекта', 'url' => array('/questionsdocs'), 'active' => Yii::app()->controller->getId() == 'questionsdocs'),
					'---',
					array('label' => 'Прочее'),
					array('label' => 'Регионы', 'url' => array('/region'), 'active' => Yii::app()->controller->getId() == 'region'),
					array('label' => 'Города', 'url' => array('/city'), 'active' => Yii::app()->controller->getId() == 'city'),
				),
				'visible' => !Yii::app()->user->isGuest,
				'active' => (!is_null(Yii::app()->controller->module) && Yii::app()->controller->module->name) || in_array(Yii::app()->controller->getId(), array('entity', 'template', 'typebuilding', 'typebuildingplace', 'region', 'city', 'typedocument', 'question', 'questionsdocs'))),
			),
		),
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'htmlOptions'=>array('class' => 'pull-right'),
			'items' => array(
				array('label' => 'Вход', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
				array('label' => 'Выход', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
			),
		),
	),
)); ?>

<div class="container" id="page">
    <div>
		<!-- Breadcrumbs -->	
		<?php if(isset($this->breadcrumbs) && is_array($this->breadcrumbs)) { ?>	
			<div class="row">
				<div class="span12">
					<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
						'links' =>$this->breadcrumbs,
					)); ?>
				</div>
			</div>
		<?php } ?>	
		<!-- .Breadcrumbs -->
		<!-- Content -->	
    	<?= $content; ?>
		<!-- .Content -->
    </div>
    <hr />
    <footer>
        <?php echo date('Y'); ?> - Copyright &copy;, Все права защищены.
    </footer>
	<br />
</div>

</body>

</html>
