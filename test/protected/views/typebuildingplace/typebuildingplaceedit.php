<?php

$this->pageTitle = 'Редактировать тип площадки';
$this->breadcrumbs = array('Типы площадок'=>array('/typebuildingplace'),'Редактировать тип площадки');  
?>
<h1>Редактировать тип площадки <small>- <?=$model->name?></small></h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/typebuildingplace', array(
	'model'=>$model,
));
?>
</div>
</div>