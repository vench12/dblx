<?php

$this->pageTitle = 'Добавить тип площадки';
$this->breadcrumbs = array('Типы площадок'=>array('/typebuildingplace'),'Добавить тип площадки');  
?>
<h1>Добавить тип площадки </h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/typebuildingplace', array(
	'model'=>$model,
));
?>
</div>
</div>
