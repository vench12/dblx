<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'typebuildingplace_form', 
			'method'=>'post', 
            'htmlOptions' => array('class' => 'well well-small', 'enctype'=>"multipart/form-data"),
));
?>

		<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
		<div>
			<?php echo $form->labelEx($model, 'name'); ?>
			<?php echo $form->textField($model, 'name', array('size'=>'100', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'name'); ?>
		</div>
		
		<div>
			<?php echo $form->fileFieldRow($model, 'imageField'); ?>
			
			
			<?php if($model->image) echo CHtml::image($model->getImageUrl(), $model->name, array('class'=>'img-polaroid')); ?>
		</div>
		
		<div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('/typebuildingplace/typebuildingplaceRemove', array('id'=>$model->getPrimaryKey())), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
			}
			?> 
		</div>
<?php $this->endWidget(); ?>