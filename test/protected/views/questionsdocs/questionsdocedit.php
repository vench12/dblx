<?php
$this->pageTitle = 'Редактировать отношение | ' . Yii::app()->name;
$this->breadcrumbs = array('Отношения типов док-ов и пар-ов проекта' => array('/questionsdocs'), 'Редактирование отношения');
?>
<h1>Отношение: <small>параметр проекта <i><?= $model->question->question; ?></i> и тип документа <i><?= $model->typedocument->name; ?></i></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/questionsdoc', array(
			'model' => $model,
		));
		?>
	</div>
</div>