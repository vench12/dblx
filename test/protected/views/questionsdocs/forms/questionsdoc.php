<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'questionsdocs_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>		
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'question_id', Question::model()->listQuestions(), array('class' => 'span5', 'empty' => '--Выбор параметра проекта--'));  ?>
	</div>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'doc_type_id', TypeDocument::model()->listTypes(), array('class' => 'span5', 'empty' => '--Выбор типа документа--'));  ?>
	</div>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('questionsdocs/QuestionsDocRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>