<?php
$this->pageTitle = 'Отношения типов док-ов и пар-ов проекта | ' . Yii::app()->name;
$this->breadcrumbs = array('Отношения типов док-ов и пар-ов проекта');
?>
<h1>Отношения типов документов и параметров проекта</h1>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/questionsdocs'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
		<?= $form->dropDownList($model, 'question_id', Question::model()->listQuestions(), array('class' => 'span3', 'empty' => '--Выбор параметрара проекта--', 'class' => 'chzn-select'));  ?>
		<?= $form->dropDownList($model, 'doc_type_id', TypeDocument::model()->listTypes(), array('class' => 'span3', 'empty' => '--Выбор типа документа--', 'class' => 'chzn-select'));  ?>
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
        $this->endWidget(); 
		?>
	</div>
	<div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить отношение',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('questionsdocs/questionsdocadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'question_id',
					'value' => '$data->question->question',
				),	
				array(
					'name' => 'doc_type_id',
					'value' => '$data->typedocument->name',
				),	
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{questiondocedit}',
					'buttons' => array
					(
						'questiondocedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/questionsdocs/questionsdocedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('filter_form', "   
	chosenDestroy();
	$('#filter_form button:reset').click(function () {
		chosenReset();
	});
");
?>