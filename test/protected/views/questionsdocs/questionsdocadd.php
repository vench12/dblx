<?php
$this->pageTitle = 'Добавить отношение | ' . Yii::app()->name;
$this->breadcrumbs = array('Отношения типов док-ов и пар-ов проекта' => array('/questionsdocs'), 'Добавить отношение');
?>
<h1>Добавить отношение</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/questionsdoc', array(
			'model' => $model,
		));
		?>
	</div>
</div>