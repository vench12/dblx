<?php
$this->pageTitle = 'Редактирование поставщика';
$this->breadcrumbs = array( 'Поставщики'=>array('/provider'), 'Редактирование поставщика');
?>
<h1>Поставщик: <small><?php echo $model->name; ?></small></h1>

<div class="btTabs">
<?php
$this->widget(
    'bootstrap.widgets.TbTabs',
    array(
    'type' => 'tabs',
    'tabs' => array( 
		array(
			'label' => 'Основное', 
			'active' => true,
			'content' => $this->renderPartial('forms/provider', array(
				'model'=>$model,
			 ), true)
	    ), array(
			'label' => 'Предложения', 
			'content' => $this->renderPartial('providerPrices', array(
				'model'=>$model,
				'pricesEntity'=>$pricesEntity,
			), true)
	    ),
     ),
));
?>
</div>