<?php

$this->pageTitle = 'Поставщики | ' . Yii::app()->name;
$this->breadcrumbs = array('Поставщики');  
?>
<h1>Поставщики</h1>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/provider'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
	   
		<?= $form->textFieldRow($model, 'name', array('class'=>'input-medium', 'prepend'=>'<i class="icon-search"></i>')); ?>
		
		<?= $form->dropDownList($model, 'city_id', CHtml::listData(Cities::model()->findAll(), 'city_id', 'name'), array('empty' => '--Выбор города--', 'class' => 'chzn-select')); ?>
		
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
		$this->endWidget(); 
		?>
	</div>
	
    <div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить поставщика',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('/provider/provideradd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php 
		$this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $dataProvider,
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'provider_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 30px'),
				),
				array(
					'name' => 'name',
				),				
				array(
					'name'=>'city_id',
					'value'=>'$data->city->name',
				), 
				array(
					'name' => 'address',
					'sortable'=>false,
				), 
				array(
					'name'=>'phone',
					'sortable'=>false,
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{viewcard}',
					'buttons' => array
					(
						'viewcard' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/provider/provideredit", array("id"=>$data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('filter_form', "   
	chosenDestroy();
	$('#filter_form button:reset').click(function () {
		chosenReset();
	});
");
?>