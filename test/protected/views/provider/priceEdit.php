<?php

$this->pageTitle = 'Редактировать предложение';
$this->breadcrumbs = array( 'Поставщики'=>array('/provider'), $model->name=>array('/provider/providerEdit', 'id'=>$model->getPrimaryKey()) ,'Редактировать предложение');
?>
<h1>Редактировать предложение</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/price', array(
	'model'=>$pricesEntity,
));
?>
</div>
</div>