<?php

$this->pageTitle = 'Добавить поставщика';
$this->breadcrumbs = array( 'Поставщики'=>array('/provider'), 'Добавить поставщика');
?>
<h1>Добавить поставщика</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/provider', array(
	'model'=>$model,
));
?>
</div>
</div>
