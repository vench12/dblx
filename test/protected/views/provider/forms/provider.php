<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'provider_form', 
			'method'=>'post', 
            'htmlOptions' => array('class' => 'well well-small'),
));
?>
		
<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
		<div>
			<?php echo $form->textFieldRow($model, 'name', array('class'=>'span5')); ?>
		</div>
		
		<div>
			<?php echo $form->textAreaRow($model, 'address', array('class'=>'span5')); ?>
		</div> 
		<div>
			<?php echo $form->textFieldRow($model, 'phone', array('class'=>'span5')); ?>
		</div> 
		
		<div> 	
			<?php 
			$data = CHtml::listData(Regions::model()->findAll(), 'region_id', 'name');
			echo $form->dropDownListRow($model, 'region_id', $data, array( 'class'=>'span5', 'empty'=>'--Выбор региона--')); 
			Yii::app()->clientScript->registerScript('provider_form', "    
				$('#Provider_region_id').change(function(){
					$('#Provider_city_id').html('');
					$.get('".Yii::app()->createUrl("/provider/citiesOptions")."', {region_id:this.value}, function(data){ $('#Provider_city_id').html(data); });
				}); 
			");
			?>
			<?php echo $form->error($model, 'region_id'); ?>
		</div>
		
		<div> 
			<?php  
			$data = CHtml::listData(Cities::model()->findAll('region_id = :region_id', array(
				':region_id'=>$model->region_id, 
			)), 'city_id', 'name'); 
			echo $form->dropDownListRow($model, 'city_id', $data, array( 'class'=>'span5', 'empty'=>'--Выбор города--'));  
			?>
			<?php echo $form->error($model, 'city_id'); ?>
		</div>
		
		<div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('/provider/ProviderRemove', array('id'=>$model->getPrimaryKey())), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
			}
			?> 
		</div>	

<?php $this->endWidget(); ?>
 	