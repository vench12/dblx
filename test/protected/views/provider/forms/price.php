<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'price_form', 
			'method'=>'post', 
            'htmlOptions' => array('class' => 'well well-small'),
));
?>
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>

	<div> 	
		<?php 
		$data = CHtml::listData(Entity::model()->findAll(), 'entity_id', 'name');
		echo $form->dropDownListRow($model, 'entity_id', $data, array( 'class'=>'span5', 'empty'=>'--Выбор предложения--'));  
		?>
	</div>
	<div>
		<?php echo $form->textFieldRow($model, 'comment', array('class'=>'span5')); ?>
	</div>
	<div>
		<?php echo $form->textFieldRow($model, 'price', array('class'=>'span2')); ?>
	</div>
	<div>
	<?php 
	echo $form->datepickerRow(
		$model,
		'date_create',
		array(
			'options' => array('language' => 'ru', 'format'=>Yii::app()->params['jsDateFormat']), 
			'prepend' => '<i class="icon-calendar"></i>'
		)
	); 
	?>
	</div>
	<div>		 
		<?php echo $form->checkBoxRow($model, 'main'); ?>			 
	</div>
	<div>		 
		<?php echo $form->checkBoxRow($model, 'actual'); ?>			 
	</div>

	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord) {
			echo CHtml::link("Удалить", 
							 Yii::app()->createUrl('/provider/priceRemove', array('pid'=>$model->getPrimaryKey())), 
							 array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>	

<?php $this->endWidget(); ?>