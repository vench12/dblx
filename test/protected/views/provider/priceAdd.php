<?php

$this->pageTitle = 'Добавить предложение';
$this->breadcrumbs = array( 'Поставщики'=>array('/provider'), $model->name=>array('/provider/providerEdit', 'id'=>$model->getPrimaryKey()) ,'Добавить предложение');
?>
<h1>Добавить предложение</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/price', array(
	'model'=>$pricesEntity,
));
?>
</div>
</div>