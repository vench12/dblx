<?php
echo CHtml::link("Добавить предложение", 
		Yii::app()->createUrl('/provider/PriceAdd', array('id'=>$model->getPrimaryKey())),
		array('class'=>'btn btn-primary'));

if (isset($pricesEntity) )
{
	$this->widget('bootstrap.widgets.TbGridView', array(
		'type' => 'striped bordered condensed',
		'dataProvider' => $pricesEntity->buildDataProvider(),
		'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
		'columns' => array(
			array(
				'name' => 'prices_id',
				'header' => '#',
				'htmlOptions' => array('style' => 'width: 30px'),
			),
			array(
				'name'=>'entity_id',
				'value' => 'isset($data->entity) ? $data->entity->name : ""',
			), 
			array(
				'name'=>'comment',
			),
			array(
				'name'=>'type',
				'header'=>'Тип',
				'value'=>'isset($data->entity) ? $data->entity->entityType->name : ""',
			),
			array(
				'name'=>'price',
				'value'=>'isset($data->entity) ? (Utilites::priceFormat($data->price) ." за ".$data->entity->units->name_short) : Utilites::priceFormat($data->price)',
			), 
			array(
				'name'=>'date_create', 
				'type'=>'date',
			), 
			array(
				'name'=>'actual',
				'value' => '$data->isActual()',
				'htmlOptions' => array(
					'class' => 'text-center',
				),
			),
			array(
				'name'=>'main', 
				'value' => '$data->isMain()',
				'htmlOptions' => array(
					'class' => 'text-center',
				),
			),
			array
			(
				'header' => 'Действия',
				'class' => 'CButtonColumn',
				'template' => '{viewcard}',
				'buttons' => array
				(
					'viewcard' => array
					(
						'label' => 'Редактировать',							
						'url' => 'Yii::app()->createUrl("/provider/PriceEdit", array("id"=>$data->provider_id,"pid"=>$data->getPrimaryKey()))',
						'options' => array( ),
					),
				),
				'htmlOptions' => array(
					'align' => 'center',
					'width' => '150px',
				),
			),
		),
	));
}
?>