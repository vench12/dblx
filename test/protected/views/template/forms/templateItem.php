<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'template_form', 
			'method'=>'post', 
            'htmlOptions' => array('class' => 'well well-small'),
));
?>

		<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
		
		<div>
			<?php  
				$data = Entity::getDataList();
				echo $form->dropDownListRow($model, 'entity_id', $data, array('class'=>'span5', 'empty'=>'--Выбор объекта--',)); 
			?>
		</div>
		<div>
			<?php echo $form->textFieldRow($model, 'count', array('class'=>'span2',)); ?> 
		</div>
		
		
		
		<div>
			<?php echo $form->checkBoxRow($model, 'required', array()); ?> 
		</div> 
	
		<div>
			<?php echo $form->textFieldRow($model, 'order_field', array('class'=>'span2',)); ?> 
		</div>
		
		<div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('/template/templateItemRemove', array('id'=>$model->getPrimaryKey())), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
			}
			?> 
		</div>
<?php $this->endWidget(); ?>
