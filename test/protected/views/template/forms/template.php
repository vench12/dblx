<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'template_form', 
			'method'=>'post', 
            'htmlOptions' => array('class' => 'well well-small'),
));
?>

		<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
		<div>
			<?php echo $form->labelEx($model, 'name'); ?>
			<?php echo $form->textField($model, 'name', array('size'=>'100', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'name'); ?>
		</div>

		<div>
			<?php  
				$data = CHtml::listData(TypeBuilding::model()->findAll(), 'type_build_id', 'name');
				echo $form->dropDownListRow($model, 'type_build_id', $data, array('class'=>'span5', 'empty'=>'--Выбор типа здания--',)); 
			?>
		</div>
		<div>
			<?php 
				$data = CHtml::listData(TypeBuildingPlace::model()->findAll(), 'type_build_place_id', 'name');
				echo $form->dropDownListRow($model, 'type_build_place_id', $data, array('class'=>'span5', 'empty'=>'--Выбор типа площадки--',)); 
			?>
		</div>
		
		<div>
			<?php echo $form->textFieldRow($model, 'min_height', array('class'=>'span2',)); ?>
			<p>Поле со значением 0 будет учавствовать в поиске любого здания</p>
		</div>
		
		<div>
			<?php echo $form->textFieldRow($model, 'max_height', array('class'=>'span2',)); ?>
			<p>Поле со значением 0 будет учавствовать в поиске любого здания</p>
		</div>

		<div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('/template/templateRemove', array('id'=>$model->getPrimaryKey())), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
			}
			?> 
		</div>
<?php $this->endWidget(); ?>
