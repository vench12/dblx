<?php

$this->pageTitle = 'Добавить шаблон';
$this->breadcrumbs = array( 'Шаблоны'=>array('/template'), 'Добавить шаблон');
?>
<h1>Добавить шаблон</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/template', array(
	'model'=>$model,
));
?>
</div>
</div>