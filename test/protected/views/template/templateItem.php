<?php
echo CHtml::link("Добавить элемент", 
		Yii::app()->createUrl('/template/templateItemAdd', array('id'=>$model->getPrimaryKey())),
		 array('class'=>'btn btn-primary'));
?>


<?php 
  
$this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $templateItem->buildDataProvider(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'template_item_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 20px'),
				),
				array(
					'name'=>'entity_id',
					'value'=>'$data->entity->name',
				), 
				array(
					'name'=>'count',
				),
				array(
					'name'=>'required', 
					'type'=>'boolean',
				), 		
				array(
					'name'=>'order_field',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{viewcard}',
					'buttons' => array
					(
						'viewcard' => array
						(
							'label' => 'Редакировать',							
							'url' => 'Yii::app()->createUrl("/template/templateItemEdit", array("id"=>$data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
)); ?>
