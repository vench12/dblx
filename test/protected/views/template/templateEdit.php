<?php

$this->pageTitle = 'Редактировать шаблон';
$this->breadcrumbs = array( 'Шаблоны'=>array('/template'), 'Редактировать шаблон');
?>
<h1>Редактировать шаблон: <small><?= $model->name?></small></h1>

 


<?php
$this->widget(
    'bootstrap.widgets.TbTabs',
    array(
    'type' => 'tabs',  
    'tabs' => array( 
		array(
			'label' => 'Основное', 
			'active' => !isset($_GET['p']),
			'content' => $this->renderPartial('forms/template', array(
				'model'=>$model,
			 ), true)
	    ), array(
			'label' => 'Производственные силы', 
			'active' => isset($_GET['p']),
			'content' => $this->renderPartial('templateItem', array(
				'model'=>$model,
				'templateItem'=>$templateItem,
			), true)
	    ),
     ),
));
?>