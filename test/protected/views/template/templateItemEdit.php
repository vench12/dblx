<?php

$this->pageTitle = 'Редактировать объект';
$this->breadcrumbs = array( 'Шаблоны'=>array('/template'), $model->template->name=>array('/template/templateEdit', 'id'=>$model->template_id, 'p'=>2) ,'Редактировать объект');
?>
<h1>Редактировать объект</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/templateItem', array(
	'model'=>$model,
));
?>
</div>
</div>