<?php

$this->pageTitle = 'Шаблоны ';
$this->breadcrumbs = array('Шаблоны');  
?>
<h1>Шаблоны <small>- методы сноса зданий</small></h1>

<div class="row">
	<div class="span10">
		<?php 
		$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/template'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
	   
		<?= $form->textFieldRow($model, 'name', array('class'=>'input-medium', 'prepend'=>'<i class="icon-search"></i>')); ?>
		
		<?= $form->dropDownList($model, 'type_build_id', CHtml::listData(TypeBuilding::model()->findAll(), 'type_build_id', 'name'), array('empty' => '--тип здания--', 'class' => 'chzn-select')); ?>
		
		<?= $form->dropDownList($model, 'type_build_place_id', CHtml::listData(TypeBuildingPlace::model()->findAll(), 'type_build_place_id', 'name'), array('empty' => '--тип площадки--', 'class' => 'chzn-select')); ?>
		
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
		$this->endWidget(); 
		?>
	</div>
	
    <div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить шаблон',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('/template/templateadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php 
		$this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $dataProvider,
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'template_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 30px'),
				),
				array(
					'name' => 'name',
				),	
				array(
					'name'=>'type_build_id',
					'value'=>'$data->typeBuilding->name',
				),
				array(
					'name'=>'type_build_place_id',
					'value'=>'$data->typeBuildingPlace->name',
				),
				array(
					'header'=>'Высота',
					'value'=>'$data->getHeightText()',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{templateedit}',
					'buttons' => array
					(
						'templateedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/template/templateedit", array("id"=>$data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('filter_form', "   
	chosenDestroy();
	$('#filter_form button:reset').click(function () {
		chosenReset();
	});
");
?>