<?php

$this->pageTitle = 'Добавить объект';
$this->breadcrumbs = array( 'Шаблоны'=>array('/template'), $model->name=>array('/template/templateEdit', 'id'=>$model->getPrimaryKey(), 'p'=>2) ,'Добавить объект');
?>
<h1>Добавить объект</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/templateItem', array(
	'model'=>$templateItem,
));
?>
</div>
</div>