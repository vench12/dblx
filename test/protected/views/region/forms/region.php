<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'region_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>		
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('region/regionRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>
