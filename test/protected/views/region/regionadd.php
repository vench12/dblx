<?php
$this->pageTitle = 'Добавить регион | ' . Yii::app()->name;
$this->breadcrumbs = array('Регионы' => array('/project'), 'Добавить регион');
?>
<h1>Добавить регион</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/region', array(
			'model' => $model,
		));
		?>
	</div>
</div>
