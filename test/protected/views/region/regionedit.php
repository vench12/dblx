<?php
$this->pageTitle = 'Редактирование региона | ' . Yii::app()->name;
$this->breadcrumbs = array('Регионы' => array('/region'), 'Редактирование региона');
?>
<h1>Регион: <small><?= $model->name; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/region', array(
			'model' => $model,
		));
		?>
	</div>
</div>
