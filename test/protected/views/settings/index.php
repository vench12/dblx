<?php
$this->pageTitle = 'Общие настройки' ;
$this->breadcrumbs = array('Общие настройки');
?>
<h1>Общие настройки</h1>

<?php
$gridViewId = 'TbGridView-settings'; 
$jsSuccess = 'js:function(){ $.fn.yiiGridView.update("'.$gridViewId.'"); }';
?>
<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search(),
                        'id'=>$gridViewId,
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
				    'name' => 'name',
                                    'htmlOptions' => array( 
                                                'width' => '20%',
                                     ),
				),
                                array(
					'name' => 'descript',
                                        'htmlOptions' => array( 
                                                'width' => '30%',
                                         ),
				),
                                array(
					'name' => 'type',
                                        'value'=>'$data->getType()',
                                        'htmlOptions' => array( 
                                                'width' => '20%',
                                        ),
				),                                 
                                array(
                                        'class'=>'bootstrap.widgets.TbEditableColumn',
                                        'name'=>'value',                                       
                                        'editable'=>array(
                                                'inputclass'=>'input-large',
                                                'url'=>array('/settings/updateValue'),
                                                'title'=>'Поле: {label}',
                                                'success'=>$jsSuccess, 
                                                'type'=>'textarea',
                                                'options'=>array(
                                                    'cols'=>10,
                                                    'rows'=>5, 
                                                ),    
                                        ),
                                        
                                ),
			),
		)); ?>
    </div>
</div>