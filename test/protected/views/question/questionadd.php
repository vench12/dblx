<?php
$this->pageTitle = 'Добавить параметр проекта | ' . Yii::app()->name;
$this->breadcrumbs = array('Параметры проекта (вопросы)' => array('/question'), 'Добавить тип документов');
?>
<h1>Добавить параметр проекта (вопрос)</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/question', array(
			'model' => $model,
		));
		?>
	</div>
</div>