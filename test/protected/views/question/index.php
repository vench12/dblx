<?php
$this->pageTitle = 'Параметры проекта (вопросы) | ' . Yii::app()->name;
$this->breadcrumbs = array('Параметры проекта (вопросы)');
?>
<h1>Параметры проекта (вопросы)</h1>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/question'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
		<?= $form->textFieldRow($model, 'question', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		
		<?php echo $form->dropDownList($model, 'group_question_id', GroupQuestion::model()->listGroups(), array('class' => 'span2', 'empty' => '--Выбор группы--', 'class' => 'chzn-select'));  ?>
		
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
        $this->endWidget(); 
		?>
	</div>
	<div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить параметр проекта',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('question/questionadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'question_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 30px'),
				),
				array(
					'name' => 'question',
				),
				array(
					'name' => 'group_question_id',
					'value' => '$data->group->name',
				),	
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{questionedit}',
					'buttons' => array
					(
						'questionedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/question/questionedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('filter_form', "   
	chosenDestroy();
	$('#filter_form button:reset').click(function () {
		chosenReset();
	});
");
?>