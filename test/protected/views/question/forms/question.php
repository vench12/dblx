<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'question_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>		
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php echo $form->textFieldRow($model, 'question', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'parent_question_id', Question::model()->listQuestions(), array('class' => 'span5', 'empty' => '--Нет зависимости--'));  ?>
	</div>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'group_question_id', GroupQuestion::model()->listGroups(), array('class' => 'span5', 'empty' => '--Выбор группы--'));  ?>
	</div>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('question/questionRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>