<?php
$this->pageTitle = 'Редактировать параметр проекта | ' . Yii::app()->name;
$this->breadcrumbs = array('Параметры проекта (вопросы)' => array('/question'), 'Редактирование параметра проекта');
?>
<h1>Параметр проекта (вопрос): <small><?= $model->question; ?></small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/question', array(
			'model' => $model,
		));
		?>
	</div>
</div>