<?php

$this->pageTitle = 'Добавить тип здания';
$this->breadcrumbs = array('Типы зданий'=>array('/typebuilding'),'Добавить тип здания');  
?>
<h1>Добавить тип здания </h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/typebuilding', array(
	'model'=>$model,
));
?>
</div>
</div>