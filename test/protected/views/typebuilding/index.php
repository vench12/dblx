<?php

$this->pageTitle = 'Типы зданий';
$this->breadcrumbs = array('Типы зданий');  
?>
<h1>Типы зданий </h1>
<div class="row">
	<div class="span10">
		<?php 
		$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/typebuilding'), 
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
	   
		<?= $form->textFieldRow($model, 'name', array('class'=>'input-medium', 'prepend'=>'<i class="icon-search"></i>')); ?>
		 
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
		$this->endWidget(); 
		?>
	</div>
	
    <div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить тип',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('/typebuilding/typebuildingadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php 
		$this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $dataProvider,
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'type_build_id',
					'header' => '#',
					'htmlOptions' => array('style' => 'width: 30px'),
				),
				array(
					'name' => 'name',
				),	
		 
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{viewcard}',
					'buttons' => array
					(
						'viewcard' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/typebuilding/typebuildingedit", array("id"=>$data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>