<?php

$this->pageTitle = 'Редактировать тип здания';
$this->breadcrumbs = array('Типы зданий'=>array('/typebuilding'),'Редактировать тип здания');  
?>
<h1>Редактировать тип здания <small>- <?=$model->name?></small></h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/typebuilding', array(
	'model'=>$model,
));
?>
</div>
</div>