﻿<?php
$this->pageTitle = 'Редактирование материала'; 
$this->breadcrumbs = array('Проекты' => array('/project'), $building->project->name=>array('/project/projectedit', 'id'=>$building->project_id), 'Редактирование материала');
?>
<h1>Редактирование материала <small>- здание <?=$building->name?></small></h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/material', array(
	'model'=>$model,
));
?>
</div>
</div>