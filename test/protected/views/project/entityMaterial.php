<?php
/** 
* Страница выводит материалы объекта
*/
$accessEdit = $model->useProcess() && is_null($model->templateItem);
?>
<b><?php echo isset($model->entity) ? trim($model->entity->name) : ''; ?></b>

<?php 
$data = $model->getMaterialsList();
if($accessEdit || (is_array($data) && sizeof($data ) > 0)) {
?>
<br /><small>материалы:</small>
<?php
if($accessEdit) {
	$idDiv =  'DivEBAddEdit-'. $model->entity_building_id;  
	$idLink =  'LinkEBAddEdit-'. $model->entity_building_id;
	?>
	<?php /*echo CHtml::link('Редактировать', '#!', array('id'=>$idLink)); 
	Yii::app()->clientScript->registerScript($idLink, ''
	. '$("#'.$idLink.'").click(function(){  '
	. 'var win = $("#mDefault"); win.modal("show");'
	. 'win.find(".modal-body").html("Загрузка..."); win.find("h3").html("Материалы"); '
	. '$.get("",{id:'.$model->entity_building_id.'},function(data){win.find(".modal-body").html(data);}); '
	. '})');
	
	Yii::app()->clientScript->registerScript($idLink, '$("#'.$idLink.'").click(function(e){ $("#'.$idDiv.'").toggle(500); e.defaultPrevented(); });');
	*/ ?>
	<?php ?>
	<div class="hide" id="<?=$idDiv?>">
	<?php
	/* $this->renderPartial('entityBuildingAddEdit', array(
	'model'=>$model,
	), false, false); */
	?>
	</div>
<?php } ?>

	<?php 
	if(is_array($data) && sizeof($data) > 0)
	{
		echo '<ul>';
		foreach($data as $item)
			echo '<li>' . $item['name'] . ' - <b>' . $item['size'] . '</b></li>';
		echo '</ul>';
	}
	?>
<?php
}
?>