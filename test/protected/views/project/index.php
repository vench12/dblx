<?php
$this->pageTitle = 'Проекты ';
$this->breadcrumbs = array('Проекты');
?>
<h1>Проекты</h1>

<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('/project'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
		<?= $form->textFieldRow($model, 'name', array('class' => 'input-medium', 'prepend' => '<i class="icon-search"></i>')); ?>
		
		<?= $form->dropDownList($model, 'city_id', CHtml::listData(Cities::model()->findAll(), 'city_id', 'name'), array('empty' => '--Выбор города--', 'class' => 'chzn-select')); ?>
		
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
        $this->endWidget(); 
		?>
	</div>
	<div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить новый проект',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('project/projectadd'),
		)); ?>
    </div>
</div>

<div class="row">
	<div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'number',
				),
				array(
					'name' => 'name',
				),
				array(
					'name' => 'status_id',
					'value' => '$data->status->name',
					'filter' => StatusProject::model()->listStatuses(),
				),		
				array(
					'name' => 'city_id',
					'value' => '$data->city->name',
				),
				array(
					'name' => 'address',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{projectedit}',
					'buttons' => array
					(
						'projectedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/project/projectedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('filter_form', "   
	chosenDestroy();
	$('#filter_form button:reset').click(function () {
		chosenReset();
	});
");
?>
