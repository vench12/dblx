<?php

/*
 * Представление графика
 */
?>

<?php
$this->pageTitle = 'График'; 
$this->breadcrumbs = array('Проекты' => array('/project'), $model->name=>array('/project/projectedit', 'id'=>$model->getPrimaryKey()), 'График');

$baseDir = Yii::app()->basePath.'/extensions/dhtmlxgantt';
$assets = Yii::app()->getAssetManager()->publish($baseDir);
Yii::app()->getClientScript()->registerCssFile($assets.'/codebase/skins/dhtmlxgantt_meadow.css');
//Yii::app()->getClientScript()->registerCssFile($assets.'/codebase/dhtmlxgantt.css');
Yii::app()->getClientScript()->registerScriptFile($assets.'/codebase/dhtmlxgantt.js');
Yii::app()->getClientScript()->registerScriptFile($assets.'/sources/locale/locale_ru.js');
Yii::app()->getClientScript()->registerCSS('sc', 
  '.dhx_section_time select { width:120px; } '
. '.dhx_gantt_duration {display:none;}'
);
//..

$scale = Yii::app()->request->getParam('scale', 1);
?>
<h1>График <small>- проект <?=$model->name?></small></h1>

<?php
$this->widget('application.widgets.ProjectNavPanel', array(
	'model'=>$model,
));
?>

<?php 
$data = $model->getScheduleData();
//echo '<pre>'; print_r($data); echo '</pre>';
?>

<div id="canvas_graph"></div>


<?php
echo CHtml::radioButtonList("scale", $scale, array(
	'1'=>'День',
	'2'=>'Неделя',
	'3'=>'Месяц',
	'4'=>'Год',
), array(
	'separator'=>' ',
	'container'=>'div',
));
?>

<script type="text/javascript">
$(function(){
	$('#canvas_graph').css({
		width:'100%',
		height:($(document).height()-$('body').height())+'px'
	});
 
	var tasks = <?=json_encode($data);?>;
	gantt.init("canvas_graph");
	
	gantt.config.scale_unit = "day";
        gantt.config.step = 1;
        gantt.config.date_scale = "%d %M, %D";
	gantt.config.readonly = true; 
        gantt.config.columns = [
            {name:"text",       label:"Task name",  width:"*", tree:true, template:function(value){ return "<a href='#'>"+value.text+"</a>";} },
            {name:"start_date", label:"Дата", align: "center" , width:70, template:function(value){ return value.start_date.getDate()+'.'+(value.start_date.getMonth()+1)+'.'+(value.start_date.getFullYear()+'').substr(2);}},
            {name:"durationX",   label:"Дни",   align: "center" , width:35, template:function(value){ return value.duration;}}  
        ];
	 
	gantt.config.buttons_right = []; 
        gantt.config.lightbox.sections=[ 
            {name:"time",        height:72, map_to:"auto", type:"duration"}
        ];
	 
	//gantt.config.buttons_left = ['dhx_cancel_btn'];
      
	
	var scaleHandler = function(e) { 
            var el = this,
                value = el.value;

            switch (value) {
                case "1":
                    gantt.config.scale_unit = "day";
                    gantt.config.step = 1;
                    gantt.config.date_scale = "%d %M, %D";
                    gantt.config.subscales = [];
                    gantt.config.scale_height = 27;
                    gantt.templates.date_scale = null;
                    break;
                case "2":
                    var weekScaleTemplate = function(date){
                        var dateToStr = gantt.date.date_to_str("%d %M");
                        var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
                        return dateToStr(date) + " - " + dateToStr(endDate);
                    };

                    gantt.config.scale_unit = "week";
                    gantt.config.step = 1;
                    gantt.templates.date_scale = weekScaleTemplate;
                    gantt.config.subscales = [
                        {unit:"day", step:1, date:"%D" }
                    ];
                    gantt.config.scale_height = 50;
                    break;
                case "3":
                    gantt.config.scale_unit = "month";
                    gantt.config.step = 1;
                    gantt.config.date_scale = "%F, %Y";
                    gantt.config.subscales = [
                        {unit:"day", step:7, date:"%j, %D" }
                    ];
                    gantt.config.scale_height = 50;
                    gantt.templates.date_scale = null;
                    break;
                case "4":
                    gantt.config.scale_unit = "year";
                    gantt.config.step = 1;
                    gantt.config.date_scale = "%Y";
                    gantt.config.min_column_width = 50;

                    gantt.config.scale_height = 90;
                    gantt.templates.date_scale = null;

                    var monthScaleTemplate = function(date){
                        var dateToStr = gantt.date.date_to_str("%M");
                        var endDate = gantt.date.add(date, 2, "month");
                        return dateToStr(date) + " - " + dateToStr(endDate);
                    };

                    gantt.config.subscales = [
                        {unit:"month", step:3, template:monthScaleTemplate},
                        {unit:"month", step:1, date:"%M" }
                    ];
                    break;
            }

            gantt.render();
        },
          
        /**
         * Change date item
         */  
	handlerTaskUpdate = function(id, item){ 
                if(item.noAjax) {                    
                    return;
                } 
                gantt.getTask(id).noAjax = true;
		var d = item.start_date;  
                console.log(item, item.end_date);
		$.post('<?php echo $this->createUrl('/project/updateEntity');?>',{
                    value: d.getFullYear() + '-' + (d.getMonth() +1) + '-' + d.getDate(),
                    pk:id.split('_')[0],
                    name:'date_start'
                }, function(){
			//location.href = location.href.split('#')[0];
                     // item.end_date.setTime(d.getTime() + item.duration * 24 * 60 * 60000); 
                     gantt.getTask(id).end_date.setTime(d.getTime() + item.duration * 24 * 60 * 60000);                    
                     gantt.updateTask(id);
                     gantt.getTask(id).noAjax  = false;
		}); 
	},
        handlerBeforeLightbox = function(id){
            var task = gantt.getTask(id);                 
            return (task.editable == 1);
        };
	
	gantt.attachEvent("onAfterTaskUpdate", handlerTaskUpdate);
        gantt.attachEvent("onBeforeLightbox", handlerBeforeLightbox);
	
	
	gantt.parse(tasks);
	$('input[name="scale"]').change( scaleHandler );
	
});	
</script>	