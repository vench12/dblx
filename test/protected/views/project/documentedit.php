﻿<?php
$this->pageTitle = 'Редактировать документ'; 
$this->breadcrumbs = array('Проекты' => array('/project'), $document->project->name => array('/project/projectedit', 'id' => $document->project_id), 'Редактирование документа'); 
?>
<h1>Редактирование документа <small>- проект <?= $document->project->name; ?> </small></h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/document', array(
			'model' => $model,
			'document' => $document,
		));
		?>
	</div>
</div>
