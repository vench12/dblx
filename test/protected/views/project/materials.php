<?php
/*
 * Файл отвечает за отображение материалов в зданиии проекта.
 */
?>

<?php
echo CHtml::link("Добавить материал", 
	Yii::app()->createUrl('/project/materialAdd', array('id'=>$model->getPrimaryKey())),
	array('class'=>'btn btn-primary'));
?>

<?php 
$gridViewId = 'gwmatId-'.$model->build_id; 
$jsSuccess = 'js:function(){ $.fn.yiiGridView.update("'.$gridViewId.'"); }';  
$this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'id'=>$gridViewId,
			'dataProvider' => $model->dataProviderMaterials(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name'=>'material_id',
					'value'=>'$data->material->name',
				),
				array( 
					'class'=>'bootstrap.widgets.TbEditableColumn',
					'name'=>'size', 
					'editable'=>array(
						'url'=>array('/project/updateMaterial'),
						'title'=>'Поле: {label}',
						'success'=>$jsSuccess,
					),
				), 
			    array(
					'header'=>'Коэф. разрыхления',
					'value'=>'$data->material->coeff_of_loss',
				),
				array(
					'header'=>'Размер после разрыхления',
					'value'=>'($data->size * $data->material->coeff_of_loss)',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{materialedit}',
					'buttons' => array
					(
						'materialedit' => array
						(
							'label' => 'Редактировать',	 
							'url' => 'Yii::app()->createUrl("/project/materialEdit", array("id"=>$data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
)); ?>
