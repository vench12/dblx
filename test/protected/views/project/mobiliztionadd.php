<?php
$this->pageTitle = 'Добавление мобилизацию'; 
$this->breadcrumbs = array('Проекты' => array('/project'), $project->name=>array('/project/projectedit', 'id'=>$project->project_id), 'Добавление мобилизации');
?>
<h1>Добавление мобилизацию <small>- проект <?=$project->name?></small></h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/mobilization', array(
	'model'=>$model,
        'project'=>$project,
));
?>
</div>
</div>