<?php
/**
 * Файл отвечает за отображения вывода зданий в проекте. 
 */
?>
<?php if(isset($buildings)) { ?>
	<?php $counter = 0; ?>
	<?php foreach($buildings as $building) {?>
		<div class="building">
		<fieldset>
			<legend>Здание # <?= (++$counter)?></legend>
            <div class="btTabs">
                <?php $this->widget(
				'bootstrap.widgets.TbTabs',
				array(
					'type' => 'tabs',
					'tabs' => array(
						array(
							'label' => 'Основное', 
							'active' => true,
							'content' => $this->renderPartial('forms/building', array(
								'model' => $building,
							), true) 
						),
						array(
							'label' => 'Объекты',
							'content' => $this->renderPartial('entities', array(
								'model' => $building,
							), true)
						),
						array(
							'label' => 'Материалы',
							'content' => $this->renderPartial('materials', array(
								'model' => $building,
							), true) 
						),
					),
                ));
                ?>
            </div>
		</fieldset>
		</div>
	<?php }?>
	
	<script type="text/javascript">
	$(function(){
		var handlerChange = function(){
			var self = this;
			$.get('<?=Yii::app()->createUrl('/project/GetTemplates')?>', $(self.form).serialize(), function(data){ 
				$(self.form).find('.TemplateID').html(data);
			}); 
		};
		$('.bl_bind_change').change(handlerChange);
		$('.bl_bind_keyup').keyup(handlerChange);
	});
	</script>

<?php } ?>


			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		 