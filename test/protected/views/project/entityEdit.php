<?php
$this->pageTitle = 'Редактирование объекта'; 
$this->breadcrumbs = array('Проекты' => array('/project'), $building->project->name=>array('/project/projectedit', 'id'=>$building->project_id), 'Редактирование объекта');
?>
<h1>Редактирование объекта <small>- здание <?=$building->name?></small></h1>

<div class="row">
	<div class="span12">
<?php 
$this->renderPartial('forms/entity', array(
	'model'=>$model,
	'providerName' => $providerName,
));
?>
</div>
</div>
<?php /* изменяем то что не по шаблону */?>
<?php 
if(!$model->isNewRecord && $model->useProcess() && is_null($model->templateItem)) {  
	$this->renderPartial('entityBuildingAddEdit', array(
		'model'=>$model,
	));
} ?>
