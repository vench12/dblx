﻿<?php
$this->pageTitle = 'Редактирование проекта | ' . Yii::app()->name;
$this->breadcrumbs = array('Проекты' => array('/project'), 'Редактирование проекта');
?>
<h1>Проект: <?= $model->name; ?></h1>

<?php
$this->widget('application.widgets.ProjectNavPanel', array(
	'model'=>$model,
));
?>

<div id="project">
	<div class="row">
		<div class="span12">
			<div class="btTabs">
				<?php $this->widget(
				'bootstrap.widgets.TbTabs',
				array(
					'type' => 'tabs',
					'tabs' => array( 
						array(
							'label' => 'Основное', 
							'active' => true,
							'content' => $this->renderPartial('forms/project', array(
								'model' => $model,
								'questions' => $questions,
								'prjQuests' => $prjQuests
							 ), true)
						), array(
							'label' => 'Документация', 
							'content' => $this->renderPartial('documents', array(
								'model' => $documents,
								'project_id' => $model->getPrimaryKey()
							), true)
						), array(
							'label' => 'Мобилизация', 
							'content' => $this->renderPartial('mobilizations', array(
								'model' => $eMobilization,
								'project_id' => $model->getPrimaryKey()
							), true)
						),
					),
				)); ?>
			</div>
		</div>
	</div>
</div>

<div id="buildings">
	<div class="row">
		<div class="span12">
			<?php
			$this->renderPartial('buildings', array(
				'model' => $model,
				'buildings' => $buildings,
			));
			?>
		</div>
	</div>
</div>

 


<?php
$this->widget('application.widgets.ProjectNavPanel', array(
	'model'=>$model,
));
?>


 