<?php

$this->pageTitle = 'Добавить здание ' ;
$this->breadcrumbs = array('Проекты' => array('/project'), $model->name=>array('/project/ProjectEdit', 'id'=>$model->getPrimaryKey()),  'Добавить здание');
?>
<h1>Добавить здание<small> - к проекту <?=$model->name?></small></h1>

<?php
$this->widget('application.widgets.ProjectNavPanel', array(
	'model'=>$model,
));
?>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/building', array(
	'model'=>$building,
));
?>
</div>
</div>
