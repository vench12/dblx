<?php

/*
 * entityBuildingAdd.php
 * Представление выводит для дополнительного объекта материалы над которыми он может работать
 */
?>




<?php
$gridViewId = 'xgwentId-'.$model->build_id; 
$models = BuildingMaterials::model()->with('material')->findAll(
		'build_id	=:build_id 
			AND t.material_id NOT IN (SELECT material_id FROM {{EntityBuildingAdd}} WHERE entity_building_id = :entity_building_id)', array(
	':build_id'=>$model->build_id,
	':entity_building_id'=>$model->entity_building_id			
));
if(sizeof($models) > 0) {
?>
<h5>Выбор материала с которым будет работать объет</h5>
<?php echo CHtml::beginForm('#'); ?>
<div class="row">
	<div class="span3">
<?php
echo CHtml::dropDownList('material', null, CHtml::listData($models, 'material_id', 'material.name'), array(
	'empty'=>'--Выбор материала--', 'class'=>'span3', 
));
?>
</div><div class="span3">		
<?php
echo CHtml::button('Добавить материал', array(
	'class'=>'btn btn btn-primary', 
	'id'=>'btnAddMaterial',
));
?> </div>
</div>	
<?php echo CHtml::endForm(); ?>

<script type="text/javascript">
$(function(){
	$('#btnAddMaterial').click(function(){
		var mVal = this.form.material.value,
			bVal = <?php echo $model->entity_building_id;?>;
		if(mVal > 0) {
			$.get('<?=Yii::app()->createUrl('/project/entityBuildingAdd')?>', {entity_building_id:bVal, material_id:mVal}, function(data){
				/*$.fn.yiiGridView.update("<?php echo $gridViewId;?>");*/
				location.href = location.href;
			});
		}
		return false;
	});
});
</script>
<?php } ?>


 

<div class="row">
	<div class="span12">
		<?php 
		
		$jsSuccess = 'js:function(){ $.fn.yiiGridView.update("'.$gridViewId.'"); }';
		$this->widget('bootstrap.widgets.TbGridView', array(
			'id'=>$gridViewId,
			'type' => 'striped bordered condensed',
			'dataProvider' => $model->buildDataProviderEntityBuildingAdd(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'material_id',
					'value'=>'$data->material->name',
				),
				array( 
					'class'=>'bootstrap.widgets.TbEditableColumn',
					'name'=>'size',  
					'editable'=>array(
						'url'=>array('/project/updateEntityBuildingAdd'),
						'title'=>'Поле: {label}',
						'success'=>$jsSuccess,
					),
				),  
				array( 
					'class'=>'bootstrap.widgets.TbEditableColumn',
					'name'=>'date_start',  
					'editable'=>array(
						'url'=>array('/project/updateEntityBuildingAdd'),
						'title'=>'Поле: {label}',
						'success'=>$jsSuccess,	
						'type'=>'date',
						'viewformat' => 'dd.mm.yyyy',
						'options' => array(
											'clear' => false,
											'datepicker' => array('language' => 'ru'),
											),
					),
				),
				
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{projectedit}',
					'buttons' => array
					(
						'projectedit' => array
						(
							'label' => 'Удалить',							
							'url' => 'Yii::app()->createUrl("/project/entityBuildingRemove", array("id" => $data->getPrimaryKey()))',
							'options' => array( 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись");', ),
						),
					),
					'htmlOptions' => array( 
					),
				),
			),
		)); ?>
    </div>
</div>
<?php   
Utilites::bdatepickerFix(); 
?>
