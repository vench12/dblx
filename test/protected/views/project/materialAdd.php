<?php
$this->pageTitle = 'Добавление материала'; 
$this->breadcrumbs = array('Проекты' => array('/project'), $building->project->name=>array('/project/projectedit', 'id'=>$building->project_id), 'Добавление материала');
?>
<h1>Добавление материала <small>- здание <?=$building->name?></small></h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/material', array(
	'model'=>$model,
));
?>
</div>
</div>