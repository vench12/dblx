<?php

$this->pageTitle = 'Добавить проект | ' . Yii::app()->name;
$this->breadcrumbs = array('Проекты' => array('/project'), 'Добавить проект');
?>
<h1>Добавить проект</h1>

<div class="row">
	<div class="span12">
		<?php
		$this->renderPartial('forms/project', array(
			'model' => $model,
			'questions' => $questions,
		));
		?>
	</div>
</div>
