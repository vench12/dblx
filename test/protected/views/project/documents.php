﻿
<?php
echo CHtml::link("Добавить документ", 
	Yii::app()->createUrl('/project/documentadd', array('id' => $project_id)),
	array('class' => 'btn btn-primary'));
?>

<div class="row">
	<div class="span12">
		<?php
		$gridViewId = 'gwmatId-' . $project_id;
		$jsSuccess = 'js:function(){ $.fn.yiiGridView.update("'.$gridViewId.'"); }';
		$this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'id' => $gridViewId,
			'dataProvider' => $model->search($project_id),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array
				(
					'header' => 'Документ',
					'class' => 'CButtonColumn',
					'template' => '{documentdownload}',
					'buttons' => array
					(
						'documentdownload' => array
						(
							'label' => 'Ссылка',							
							'url' => '$data->getUrl()',
							'options' => array('target' => '_blank'),
							'visible' => '$data->file',
						),
					),
					'htmlOptions' => array(
						'align' => 'center',
						'width' => '100px',
					),
				),
				array( 
					'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'price', 
					'editable' => array(
						'url' => array('/project/updateDocumentPrice'),
						'title' => 'Поле: {label}',
						'success' => $jsSuccess,
					),
				),
				array(
					'name' => 'doc_type_id',
					'value' => '$data->type->name',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{documentedit}',
					'buttons' => array
					(
						'documentedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/project/documentedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>
