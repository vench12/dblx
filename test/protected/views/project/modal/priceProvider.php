<?php

/** @var Project $modelProject */

$providersLink_default = isset($providersLink_default) ? $providersLink_default : 'Выбрать цену у поставщика';
$providersLink_provider = isset($providersLink_provider) ? $providersLink_provider : 'Цена от поставщика';
$fieldPrice = isset($fieldPrice) ? $fieldPrice : 'EntityBuilding_price';
$fieldPriceId = isset($fieldPriceId) ? $fieldPriceId : 'EntityBuilding_prices_id';
?>

<?php $this->beginWidget(
	'bootstrap.widgets.TbModal',
	array('id' => 'providersContent')
); ?>
 
 
	<div class="modal-header">
		<a class="close" data-dismiss="modal">&times;</a>
		<h4>Доступные поставщики</h4>
		<table class="table-ext">
			<tr>
				<td>
				<div> 	
					<?php
					$data = Provider::model()->listRegions();
					echo CHtml::dropDownList('Project_region_id', $modelProject->region_id, $data, array('class' => 'span2', 'empty' => '--Выбор региона--'));
					Yii::app()->clientScript->registerScript('entity_form-location', "
						$('#Project_region_id').change(function() {
							$('#Project_city_id').html('');
							$.get('".Yii::app()->createUrl("/project/citiesListFromProvider")."', {region_id:this.value}, function(data){  $('#Project_city_id').html(data); });
						});
					");
					?>
				</div>
				</td>
				<td>
				<div> 
					<?php
					$data = Provider::model()->listCities($modelProject->region_id);
					echo CHtml::dropDownList('Project_city_id', $modelProject->city_id, $data, array('class' => 'span2', 'empty' => '--Выбор города--'));
					Yii::app()->clientScript->registerScript('entity_form-providers', "
						function pget(c_id) {
							$.get('".Yii::app()->createUrl("/project/accordionProviders")."', {city_id:c_id,prices_id:".$model->prices_id."}, function(data){ $('#accordionProviders').html(data); });	
						}
						$('#Project_city_id').change(function() {
							pget(this.value);
						});
						$(function () {
							pget(".$modelProject->city_id.");
						});
					");
					?>
				</div>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="modal-body">
		<div class="accordion" id="accordionProviders"></div>
	</div>
	
	<div class="modal-footer">
		<?php $this->widget(
			'bootstrap.widgets.TbButton',
			array(
				'type' => 'primary',
				'label' => 'Применить',
				'url' => '#',
				'htmlOptions' => array('data-dismiss' => 'modal', 'id' => 'modalOk'),
		)); ?>
		<?php $this->widget(
			'bootstrap.widgets.TbButton',
			array(
				'label' => 'Отмена',
				'url' => '#',
				'htmlOptions' => array('data-dismiss' => 'modal', 'id' => 'modalReset'),
			)
		); ?>
	</div>
	
<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('modal-window', "
	function pr_link(link) {
		$('#providersLink').html( link );
	}
	
	function pr_id_set(price_id) {
		$('#".$fieldPriceId."').val( parseInt(price_id) );
	}
	
	function pr_val_set(price_val) {
		$('#".$fieldPrice."').val( price_val );
	}
	
	$('#modalOk').click(function() {
		price_id = parseInt($('#providersContent').find('input:radio:checked').val());
		if (!isNaN(price_id)) {
			pr_id_set( price_id );
			pr_val_set( $('#providersContent').find('#price-'+price_id).val() );
			provider_id = parseInt($('#providersContent').find('#for_provider_id-'+price_id).val());
			provider_name = $('#providersContent').find('#provider-'+provider_id).html();
			pr_link('".$providersLink_provider." &laquo;'+provider_name+'&raquo;');
		}
	});
	
	$('#modalReset').click(function() {
		if (confirm('Сбросить выбранную цену от поставщика?')) {
			$('#providersContent').find('input:radio').removeAttr('checked');
			pr_id_set(0);
			pr_val_set('0.00');
			pr_link('".$providersLink_default."');
		}
	});
");
?>


