<?php
$this->pageTitle = 'Добавление объекта'; 
$this->breadcrumbs = array('Проекты' => array('/project'), $building->project->name=>array('/project/projectedit', 'id'=>$building->project_id), 'Добавление объекта');
?>
<h1>Добавление объекта <small>- здание <?=$building->name?></small></h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/entity', array(
	'model'=>$model,
));
?>
</div>
</div>