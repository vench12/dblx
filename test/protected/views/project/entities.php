<?php
/*
 * Файл отвечает за отображение сущностей в зданиии проекта.
 */
?>

<?php
echo CHtml::link("Добавить объект", 
	Yii::app()->createUrl('/project/entityAdd', array('id'=>$model->getPrimaryKey())),
	array('class'=>'btn btn-primary'));
?>

<?php 
$gridViewId = 'gwentId-'.$model->build_id; 
$jsSuccess = 'js:function(){ $.fn.yiiGridView.update("'.$gridViewId.'"); }';
$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->dataProviderEntities(),
	'id'=>$gridViewId,
	'template' => '{summary}{items}<div class="pagination-centered">{pager}</div>', 
	'columns' => array( 
		array (
			'header'=>'Название\материалы',  
			'value'=>'trim(Yii::app()->controller->renderPartial("entityMaterial",array("model"=>$data)))',
			'htmlOptions' => array( 
				'width' => '250px',
			),
        ),
		array(
			'class'=>'bootstrap.widgets.TbEditableColumn',
			'name'=>'count', 
			'header'=>'Кол-во',
			'editable'=>array(
				'url'=>array('/project/updateEntity'),
				'title'=>'Поле: {label}',
				'success'=>$jsSuccess,
			),
			'htmlOptions' => array( 
				'width' => '90px',
			),
		),
		array(
			'class'=>'bootstrap.widgets.TbEditableColumn',
			'name'=>'count_mobile', 
			'header'=>'Кол-во моб.',
			'editable'=>array(
				'url'=>array('/project/updateEntity'),
				'title'=>'Поле: {label}',
				'success'=>$jsSuccess,
			),
			'htmlOptions' => array( 
				'width' => '90px',
			),
		),
      

                array(
			'class'=>'bootstrap.widgets.TbEditableColumn',
                        'header'=>'Дата в граф.',
			'name'=>'date_start', 
			'editable'=>array(
						'url'=>array('/project/updateEntity'),
						'title'=>'Поле: {label}',
						'success'=>$jsSuccess,	
						'type'=>'date',
						'viewformat' => 'dd.mm.yyyy',
						'options' => array(
											'clear' => false,
											'datepicker' => array('language' => 'ru'),
					    ),
			 ),
		), 
		array(
			'header'=>'Из шаблона',
			'value'=>'($data->template_item_id > 0) ? "Да" : "Нет"',  
			'htmlOptions' => array( 
				'width' => '90px',
			),
		),
            
            
        array(
			'header'=>'Обязательное',
			'value'=>'(!is_null($data->templateItem) &&  $data->templateItem->required == 1) ? "Да" : "Нет"', 
			'htmlOptions' => array( 
				'width' => '90px',
			),	
		), 
            
            array(
			'class'=>'bootstrap.widgets.TbEditableColumn',
			'name'=>'working_day', 
			'header'=>'Раб. часы',
			'editable'=>array(
				'url'=>array('/project/updateEntity'),
				'title'=>'Поле: {label}',
				'success'=>$jsSuccess,
			),
			'htmlOptions' => array( 
				'width' => '90px',
			),
		),
            
            	 array(
			'class'=>'bootstrap.widgets.TbEditableColumn',
			'name'=>'price', 
			'value'=>'Utilites::priceFormat($data->price)',
			'editable'=>array(
				'url'=>array('/project/updateEntity'),
				'title'=>'Поле: {label}',
				'success'=>$jsSuccess,
			),
		), /* пока не корректно. т.к. техника либо процесс + моб*/
		array(
			'header'=>'Сумма',
			'value'=>'Utilites::priceFormat($data->price * $data->count)',  
		), 
            
		array
		(
			'header' => 'Действия',
			'class' => 'CButtonColumn',
			'template' => '{entityedit}',
			'buttons' => array
			(
				'entityedit' => array
				(
					'label' => 'Редактировать',	 
					'url' => 'Yii::app()->createUrl("/project/entityEdit", array("id"=>$data->getPrimaryKey()))',
					'options' => array( ),
				),
			),
			'htmlOptions' => array(
				'class' => 'text-center',
				'width' => '150px',
			),
		),
	),
)); 

Utilites::bdatepickerFix();
?>