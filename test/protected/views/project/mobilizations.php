<?php

/* 
 *  Все что связано с мобилизацией
 */

?>
<?php
echo CHtml::link("Добавить мобилизацию", 
	Yii::app()->createUrl('/project/mobiliztionadd', array('id' => $project_id)),
	array('class' => 'btn btn-primary'));
?>

<div class="row">
    <div class="span12">
        <?php
		$gridViewId = 'idMobilez-' . $project_id;
		$jsSuccess = 'js:function(){ $.fn.yiiGridView.update("'.$gridViewId.'"); }';
		$this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'id' => $gridViewId,
			'dataProvider' => $model->search($project_id),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				 
                                array(
					'name' => 'entity_building_id', 
                                        'value'=> '$data->entityBuilding->entity->name',
				),
                                array(
                                        'header'=>'Поставщик',
					'name' => 'prices_id', 
                                        'value'=> '!is_null($data->pricesEn) ? $data->pricesEn->provider->name : ""',
				), 
                                array(
					'name' => 'count', 
				),
				array(
					'name' => 'price', 
				),
                                array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{documentedit}',
					'buttons' => array
					(
						'documentedit' => array
						(
							'label' => 'Редактировать',							
							'url' => 'Yii::app()->createUrl("/project/mobiliztionedit", array("id" => $data->getPrimaryKey()))',
							'options' => array( ),
						),
					),
					'htmlOptions' => array(
						'align' => 'center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
 </div>    