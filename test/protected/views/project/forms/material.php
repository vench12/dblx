<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'material_form', 
			'method'=>'post', 
            'htmlOptions' => array('class' => 'well well-small'),
));
?>
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php echo $form->dropDownListRow($model, 'material_id', CHtml::listData(Materials::model()->findAll(), 'material_id','name'), array('class' => 'span5', 'empty' => '--Выбор материала--'));  ?>
	</div>
	
	<div>
		<?php echo $form->textFieldRow($model, 'size', array('class' => 'span2') ); ?>
	</div>

		<div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('/project/materialRemove', array('pid'=>$model->getPrimaryKey())), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
			}
			?> 
		</div>	

<?php $this->endWidget(); ?>