﻿<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'document_form',
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small', 'enctype' => 'multipart/form-data'),
));
?>
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?= $form->dropDownListRow($document, 'doc_type_id', TypeDocument::model()->listTypes(), array('empty' => '--Выбор типа--', 'class' => 'span5')); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($document, 'price', array('class' => 'span5')); ?>
	</div>
	
	<div>
		<?php
		echo $form->fileFieldRow($document, 'documentField');
		if ($document->file)
			echo CHtml::link('Ссылка', $document->getUrl(), array('target' => '_blank'));
		?>
	</div>
    <br />
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if (!$document->isNewRecord)
		{
			echo CHtml::link("Удалить", 
				 Yii::app()->createUrl('/project/documentRemove', array('id' => $document->getPrimaryKey())), 
				 array('class'=>'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
<?php $this->endWidget(); ?>
