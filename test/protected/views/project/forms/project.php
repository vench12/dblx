<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'project_form',
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>
<div class="row">
	<div class="span6">
		<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
		<div>
			<?php echo $form->textFieldRow($model, 'number', array('class' => 'span5' , 'disabled'=>(!$model->isNewRecord)?'disabled':'')); ?>
		</div>
		<div>
			<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5')); ?>
		</div>
		<table class="table-ext">
			<tr>
				<td>
				<div>
					<?php 
					echo $form->datepickerRow($model, 'date_start', array(
						'options' => array('language' => 'ru', 'format' => Yii::app()->params['jsDateFormat']), 'class' => 'span2',
						'prepend' => '<i class="icon-calendar"></i>'
					)); 
					?>
				</div>
				</td>
				<td>
				<div>
					<?php 
					echo $form->datepickerRow($model, 'date_end', array(
						'options' => array('language' => 'ru', 'format' => Yii::app()->params['jsDateFormat']), 'class' => 'span2',
						'prepend' => '<i class="icon-calendar"></i>'
					)); 
					?>
				</div>
				</td>
			</tr>
		</table>
		<div>
			<?php echo $form->dropDownListRow($model, 'status_id', StatusProject::model()->listStatuses(), array('class' => 'span5', 'empty' => '--Выбор статуса--'));  ?>
		</div>
		<div>
			<?php echo $form->textAreaRow($model, 'address', array('rows' => '2' , 'class' => 'span5')); ?>
		</div>
		<div> 	
			<?php 
			$data = CHtml::listData(Regions::model()->findAll(), 'region_id', 'name');
			echo $form->dropDownListRow($model, 'region_id', $data, array('class' => 'span5', 'empty' => '--Выбор региона--'));
			Yii::app()->clientScript->registerScript('project_form', "   
				$('#Project_region_id').change(function(){
					$('#Project_city_id').html('');
					$.get('".Yii::app()->createUrl("/project/citiesOptions")."', {region_id:this.value}, function(data){ $('#Project_city_id').html(data); });
				}); 
			");
			?>
		</div>
		<div> 
			<?php  
			$data = CHtml::listData(Cities::model()->findAll('region_id = :region_id', array(
				':region_id' => $model->region_id, 
			)), 'city_id', 'name'); 
			echo $form->dropDownListRow($model, 'city_id', $data, array('class' => 'span5', 'empty' => '--Выбор города--'));  
			?>
		</div>
		<fieldset><legend><small>Общая информация</small></legend>
		<div>
			<?php echo $form->dropDownListRow($model, 'cleaning', PaymentType::getInstance()->getPaymentTypes(), array('class' => 'span5', 'empty' => '--Выбор типа--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'payment_climbers', PaymentType::getInstance()->getPaymentTypes(), array('class' => 'span5', 'empty' => '--Выбор типа--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'backfill_id', Materials::model()->listMaterials(true), array('class' => 'span5', 'empty' => '--Выбор материала--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'crusher', Crusher::getInstance()->getCrusherTypes(), array('class' => 'span5', 'empty' => '--Не требуется--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'payment_gas_device', PaymentTypeGasDevice::getInstance()->getPaymentTypes(), array('class' => 'span5', 'empty' => '--Выбор--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'disable_sewage', TreatyParties::getInstance()->getTreatyParties(), array('class' => 'span5', 'empty' => '--Выбор--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'disable_electro', TreatyParties::getInstance()->getTreatyParties(), array('class' => 'span5', 'empty' => '--Выбор--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'disable_gas', TreatyParties::getInstance()->getTreatyParties(), array('class' => 'span5', 'empty' => '--Выбор--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'provide_electro', TreatyParties::getInstance()->getTreatyParties(), array('class' => 'span5', 'empty' => '--Выбор--'));  ?>
		</div>
		<div>
			<?php echo $form->dropDownListRow($model, 'provide_water', TreatyParties::getInstance()->getTreatyParties(), array('class' => 'span5', 'empty' => '--Выбор--'));  ?>
		</div>
		</fieldset>
		<fieldset><legend><small>Расстояния</small></legend>
			<div>
				<?php echo $form->textFieldRow($model, 'distance_recycled', array('class' => 'span5')); ?>
			</div>
			<div>
				<?php echo $form->textFieldRow($model, 'distance_scrap', array('class' => 'span5')); ?>
			</div>
			<div>
				<?php echo $form->textFieldRow($model, 'distance_landfill', array('class' => 'span5')); ?>
			</div>
		</fieldset>
	</div>
	
	<div class="span5">
		<div id="questions">
			<?php
			$group_id = 0;
			if (isset($questions))
			{
				if (isset($prjQuests))
					$prjQuests_ = Utilites::convertObjectARToSimpleArray($prjQuests, array('question_id', 'answer'));
				
				foreach ($questions as $question)
				{
					if ($group_id != $question->group_question_id)
					{ ?>
						<fieldset><legend><small><?= $question->group->name; ?></small></legend>
					<?php } ?>
					<div class="question" data-parent_id="<?= $question->parent_question_id; ?>" data-question_id=<?= $question->question_id; ?>>
						<label class="checkbox">
						<?php
						$checked = false;
						if (isset($prjQuests))
						{
							if (in_array(array('question_id' => $question->question_id, 'answer' => '1'), $prjQuests_))
							{
								$checked = true;
								Yii::app()->clientScript->registerScript('question_'.$question->question_id, "
									questions.each(function() {
										if ($(this).data('parent_id') == ".$question->question_id.")
											$(this).show();
									});
								", CClientScript::POS_READY);
							}
						}
						
						echo CHtml::checkbox('questions[]', $checked, array('id' => 'question-'.$question->question_id, 'value' => $question->question_id));
						echo CHtml::label($question->question, 'question-'.$question->question_id);
						?>
						</label>
					</div>
					<?php
					if ($group_id != $question->group_question_id)
					{ ?>
						</fieldset>
					<?php }
					$group_id = $question->group_question_id;
				}
			}
			?>
		</div>
		<div>
			<?php
			if($model->isNewRecord)
			{ ?>
				<p><i>Подсказка: на основании отмеченных параметров автоматически будет сформирован список необходимой документации.</i></p>
			<?php }
			else
			{ ?>
				<p><i>Предупреждение: при редактировании параметров здания список необходимой документации повторно не формируется.</i></p>
			<?php } ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="span12">
		<br />
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php 
		if(!$model->isNewRecord) {
			echo CHtml::link("Удалить", 
				Yii::app()->createUrl('project/projectRemove', array('id' => $model->getPrimaryKey())), 
				array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удaлить запись?");'));
		}
		?> 
	</div>
</div>
<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('questions', "
	var questions_container = $('#questions');
	var questions = questions_container.find('.question');
	
	questions.each(function() {
		if ($(this).data('parent_id') > 0)
			$(this).hide();
	});
	
	questions.find('input[type=checkbox]').change(function() {
		var self = $(this);
		var is_checked  = ( self.attr('checked') ) ? true : false;
		var question_id = self.parent().parent().data('question_id');
		
		if (is_checked) {
			questions.each(function() {
				if ($(this).data('parent_id') == question_id)
					$(this).slideDown();
			});
		} else {
			questions.each(function() {
				if ($(this).data('parent_id') == question_id) {
					$(this).find('input[type=checkbox]').removeAttr('checked').trigger('change');
					$(this).slideUp();
				}
			});
		}
	});
", CClientScript::POS_END);
?>
