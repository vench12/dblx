<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'entity_form', 
	'method' => 'post', 
	'htmlOptions' => array('class' => 'well well-small'),
));
?>	
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
	<div>
		<?php  
		if(!is_null($model->templateItem) && $model->templateItem->required == 1) { 
			echo CHtml::telField('x', $model->entity->name, array('class'=>'span5 disabled', 'disabled'=>'disabled'));
		} else {
			$data = Entity::getDataList();
			echo $form->dropDownListRow($model, 'entity_id', $data, array('class'=>'span5', 'empty'=>'--Выбор объекта--',)); 
		}
		?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model, 'count', array('class' => 'span2') ); ?>
	</div>
        <div>
		<?php echo $form->textFieldRow($model, 'count_mobile', array('class' => 'span2') ); ?>
	</div>
        <div>
		<?php echo $form->textFieldRow($model, 'working_day', array('class' => 'span2') ); ?>
	</div>
        

	<div>
    	<?php echo $form->hiddenField($model, 'prices_id', array()) ?>
    	
		<?php echo $form->textFieldRow($model, 'price', array('class' => 'span2') ); ?>
		<?php
		$providersLink_default = 'Выбрать цену у поставщика';
		$providersLink_provider = 'Цена от поставщика';
		$providersLink = (isset($providerName) && !empty($providerName)) ? 'Цена от поставщика &laquo;' . $providerName . '&raquo;' : $providersLink_default;
        echo CHtml::link($providersLink, '#', array(
			'id' => 'providersLink',
			'data-toggle' => 'modal',
			'data-target' => '#providersContent',
		));
		?>
  
	</div>
	
	<div>
		<?php 
		//дата иммет смысл только у тех элементов что учавствуют в процессе
		if($model->useProcess()) {
			echo $form->datepickerRow($model, 'date_start', array(
				'options' => array('language' => 'ru', 'format' => Yii::app()->params['jsDateFormat']), 'class' => 'span2',
				'prepend' => '<i class="icon-calendar"></i>'
			)); 
		}
		?>
	</div>
	
	<div>
		<button type="submit" class="btn btn-primary">Сохранить</button>
		<?php
		if(!$model->isNewRecord) {
			if(!is_null($model->templateItem) && $model->templateItem->required == 1) { ?>
				<p>Этот объект нельзя удалить, так как он привязан к шаблону.</p>
			<?php } else {
			echo CHtml::link("Удалить", 
				 Yii::app()->createUrl('/project/entityRemove', array('id'=>$model->getPrimaryKey())),
				 array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));

			}
		}
		?>
	</div>
<?php $this->endWidget(); ?>

<?php $this->renderPartial('modal/priceProvider', array(
    'modelProject'=>$model->building->project,
    'model'=>$model,
    'fieldPrice'=>'EntityBuilding_price',
    'fieldPriceId'=>'EntityBuilding_prices_id',
)); ?>
