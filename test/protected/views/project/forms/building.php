<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'building_form', 
	'method'=>'post', 
	'action'=>(!$model->isNewRecord) ? Yii::app()->createUrl('/project/buildingUpdate', array('id'=>$model->getPrimaryKey())) : "",
	'htmlOptions' => array('class' => 'well well-small'),
));
?>	
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
    
    <div>
        <?php echo $form->textFieldRow($model, 'name', array('class'=>'span5')); ?> 
    </div>
    
    <div>
        <?php  
		$data = CHtml::listData(TypeBuilding::model()->findAll(), 'type_build_id', 'name');
		echo $form->dropDownListRow($model, 'type_build_id', $data, array('class'=>'span5 bl_bind_change', 'empty'=>'--Выбор типа здания--',)); 
        ?>
    </div>
    <div>
        <?php 
		$data = CHtml::listData(TypeBuildingPlace::model()->findAll(), 'type_build_place_id', 'name');
		echo $form->dropDownListRow($model, 'type_build_place_id', $data, array('class'=>'span5 bl_bind_change', 'empty'=>'--Выбор типа площадки--',));
        ?>
    </div>

	<fieldset>
        <legend><small>Размеры</small></legend>
		<table class="table-ext">
			<tr>
                <td>
                <div>
                	<?php echo $form->textFieldRow($model, 'height', array('class'=>'span1 bl_bind_change bl_bind_keyup',)); ?> 
                </div>
                </td>
                <td>
                <div>
                	<?php echo $form->textFieldRow($model, 'width', array('class'=>'span1',)); ?> 
                </div>
                </td>
                <td>
                <div>
                	<?php echo $form->textFieldRow($model, 'depth', array('class'=>'span1',)); ?> 
                </div>
                </td>
			</tr>
		</table>
    </fieldset>
    
    <?php if(!$model->isNewRecord) { ?>
    <fieldset>
        <legend><small>Метод сноса</small></legend>
        <div>
            <?php  
			$data = CHtml::listData(Template::getTemplates( $model->type_build_id,  $model->type_build_place_id, $model->height), 'template_id', 'name');
			echo $form->dropDownListRow($model, 'template_id', $data, array('class'=>'span5 TemplateID', 'empty'=>'--Выбор метода сноса--',)); 
            ?>
            <p><i>Если список методов пуст, то это значит, что ваше здание не подходит не под один шаблон.</i></p>	
            <p><i>После выбора шаблона нажмите Обновить. Система привяжет нужные объекты к зданию.</i></p>
        </div>
    </fieldset>
    <?php }?>
    
    <div>
        <button type="submit" class="btn btn-primary"><?=((!$model->isNewRecord) ? 'Обновить' : 'Сохранить')?></button>
        <?php 
        if(!$model->isNewRecord) {
            echo CHtml::link("Удалить", 
				Yii::app()->createUrl('/project/buildingRemove', array('pid'=>$model->getPrimaryKey())), 
				array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
        }
        ?> 
    </div>	

<?php $this->endWidget(); ?>