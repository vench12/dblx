<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'material_form', 
			'method'=>'post', 
            'htmlOptions' => array('class' => 'well well-small'),
));
?>
	<p>Поля <span class="required">*</span> - обязательные для заполнения.</p>
	
        
        <div>
		<?php 
                
                  if($model->isNewRecord) {
                        echo $form->dropDownListRow(
                            $model, 
                            'entity_building_id', 
                            EntityBMobilization::dataListByProject($project->getPrimaryKey()), 
                            array('class'=>'span4', 'empty'=>'--Выбор--'));  
                  } else {
                      echo CHTml::textField(
                              'x',
                              $model->entityBuilding->entity->name.' (м. '.$model->entityBuilding->count_mobile.' ост. '.($model->entityBuilding->count_mobile - $model->entityBuilding->getSumMobile(   )).' ) ' , 
                              array('class'=>'span4 disabled', 'disabled'=>'disabled'));
                  }
                ?>
	</div>
        
        
	<div>
		<?php echo $form->textFieldRow($model, 'count', array('class'=>'span2'));  ?>
	</div>
        
        <div>
	    <?php echo $form->textFieldRow($model, 'price', array('class'=>'span2'));  ?>
            
            <?php
	     $providersLink_default = 'Выбрать цену у поставщика';	 
             $providersLink = (isset($providerName) && !empty($providerName)) ? 'Цена от поставщика &laquo;' . $providerName . '&raquo;' : $providersLink_default;
             echo CHtml::link($providersLink, '#', array(
			'id' => 'providersLink',
			'data-toggle' => 'modal',
			'data-target' => '#providersContent',
		));
		?>
	</div>
        
       	<div>
            
            
            <?php echo $form->hiddenField($model, 'prices_id'); ?>
            
		<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('/project/mobilizationRemove', array('id'=>$model->getPrimaryKey())), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверены, что хотите удaлить запись?");'));
			}
			?> 
	</div>	

<?php $this->endWidget(); ?> 
        
        
<?php $this->renderPartial( 'modal/priceProvider', array(
    'modelProject'=>$project,
    'model'=>$model,
    'fieldPrice'=>'EntityBMobilization_price',
    'fieldPriceId'=>'EntityBMobilization_prices_id',
)); ?>