<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

return array(
    'sourceLanguage' => 'ru',
    'language' => 'ru',
	'charset' => 'utf-8',
	
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'homeUrl' => 'http://' . $_SERVER['HTTP_HOST'] . '/',
	
    'name' => 'ЕРЦ (Единый Расчетный Центр)',
	
	'preload' => array('log', 'bootstrap'),

    'import' => array(
		'application.models.*',
		'application.models.forms.*',
		'application.components.*',
    ),
	
	'modules' => array(
        'gii' => array(
            'generatorPaths' => array(
                'bootstrap.gii',
            ),
        ),
		'adminUsers'=>array(
                    'defaultController' => 'adminUsers/users',
                    'name'=>'Управление пользователями'
                 ),
    ),
    
    'components' => array(
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		
		'session' => array (
			'sessionName' => 'snos_zdaniy',
            'autoStart' => true
		),
	
		'user' => array(
			'class' => 'WebUser',
			'allowAutoLogin' => true,
		),
		
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
				array(
					'class' => 'CWebLogRoute',
					'levels' => 'warning, error', // warning, trace, info, profile, warning, error
				),
				
			),
		),
		
		'authManager' => array(
			'class' => 'RDbAuthManager',
			'defaultRoles' => array('guest'), // по-умолчанию все с ролью гость
		),
				
		'bootstrap' => array(
			'class' => 'bootstrap.components.Bootstrap',
		),
		'db' => array(
			'connectionString' => 'mysql:host=c3.spetsakov.ru;port=3306;dbname=demolition',
			'emulatePrepare' => true,
			'username' => 'demolition',
			'password' => 'ttngyrd44',
			'charset' => 'utf8',
			'tablePrefix' => '',/*
			'connectionString' => 'mysql:host=localhost;dbname=dbl',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => '', */
		),
    ),
	
	'params' => array(
		'postsPerPage' => 10,
		'phpDateFormat' => 'd.m.Y',
		'jsDateFormat' =>' dd.mm.yyyy',
		'dirUploads' => 'uploads',
		'priceFormat'=>'#,##0.00',
		'extensions' => array(
			'images' => 'jpg, jpeg, gif, bmp, png, tiff',
			'documents' => 'pdf, xls, xlsx, doc, docx, txt, rtf, html, htm, ppt, pptx',
		),
    ),
); 

?>