<?php

class ProjectQuestion extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{ProjectQuestions}}';
    }
	
	public function rules()
	{
		return array(
			array('project_id, question_id, answer', 'required'),
			
			array('project_id', 'numerical', 'min' => 0),
			array('question_id', 'numerical', 'min' => 0),
			array('answer', 'boolean', 'falseValue' => 0, 'trueValue' => 1),
			
			array('project_id, question_id', 'safe', 'on' => 'search'),
		);
	}
	
	public function relations()
	{
		return array(
			'project' => array(self::HAS_MANY, 'Project', 'project_id'),
			'question' => array(self::HAS_MANY, 'Question', 'question_id'),
		);
	}
	
	public function attributeLabels()
	{ 
		return array(
			'project_id' => 'Проект',
			'question_id' => 'Параметр проекта / вопрос',
			'answer' => 'Ответ'
		);
	}
	
	public function getQuestions()
	{
		$data = Question::model()->findAll(array(
			'with' => array('group'),
			'order' => 'group.name, t.question',
		));
		return $data;
	}
	
	public function getPrjQuests($project_id)
	{
		$data = $this->findAll('project_id=:project_id', array(':project_id' => intval($project_id)));
		return $data;
	}
	
	public function add($project_id, $question_id, $answer)
	{
		$command = Yii::app()->db->createCommand();
		$command->insert('ProjectQuestions', array(
			'project_id' => intval($project_id),
			'question_id' => intval($question_id),
			'answer' => intval($answer)
		));
	}
	
	public function remove($project_id)
	{
		$command = Yii::app()->db->createCommand();
		$command->delete('ProjectQuestions', 'project_id=:project_id', array(':project_id' => intval($project_id)));
	}
}

?>