<?php



/**
 * Класс пользователя в системе.
 *
 * @author v.raskin
 */
class Users extends CActiveRecord {
   
 
 	public $password_confirm;

	public $new_password;   


	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;

    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
	return parent::model($className);
    }

     public function attributeLabels() {
		return array(
             'user_id'=>'ID', 
             'login'=>'Логин',
			 'password'=>'Пароль',
			 'email'=>'Адрес эл.почты',
			 'name'=>'Имя',
			 'surname'=>'Фамилия',
			 'patronymic'=>'Отчество',
			 'date_create'=>'Дата создания',
			 'active'=>'Актуальность',
			 'isSuperAdmin'=>'Является суперадминистратором',
			 'new_password'=>'Пароль',
			 'password_confirm'=>'Подтверждение пароля',
        );
    }
    
    /**
     * Шифрует переданный пароль.
     * Пока простая реализация.
     * @param string $password
     * @return string 
     */
    public static function cryptPass($password) {
        return MD5($password);
    }
    
    /**
     * @return string 
    */
    public function tableName()
    {  
	return '{{Users}}';
    }

	public function rules()
	{
		return array(
			array('active, isSuperAdmin', 'boolean','on'=>array('edit','add',)),
			array('login, surname, name', 'required', 'on'=>array('edit','add')),
			array('new_password, password_confirm', 'required', 'on'=>array('add')),
			array('email','email', 'on'=>array('edit','add'),'message' => 'Адрес электронной почты указан неверно.'),
			array('password_confirm', 'compare', 'compareAttribute'=>'new_password','on'=>array('edit','add'), 'message'=>'Пароли не совпадают.'),
			array('login', 'length', 'min'=>4,'max'=>64,'on'=>array('edit','add'),'tooShort'=>'Поле &laquo;{attribute}&raquo; - минимальное количество символов: {min}', 'tooLong'=>'Поле &laquo;{attribute}&raquo; - максимальное количество символов: {max}'),
			array('new_password, password_confirm', 'length', 'min'=>4,'max'=>64,'on'=>array('edit','add'),'tooShort'=>'Поле &laquo;{attribute}&raquo; - минимальное количество символов: {min}', 'tooLong'=>'Поле &laquo;{attribute}&raquo; - максимальное количество символов: {max}'),
			array('surname, name,patronymic', 'length', 'min'=>4,'max'=>64,'on'=>array('edit','add'),'tooShort'=>'Поле &laquo;{attribute}&raquo; - минимальное количество символов: {min}', 'tooLong'=>'Поле &laquo;{attribute}&raquo; - максимальное количество символов: {max}'),
			
			array('login, email, name, surname, patronymic', 'safe', 'on' => 'search'),

		);
	}
    

	public function search()
	{
		
		$criteria = new CDbCriteria();
		$criteria->compare('login', $this->name, true);
		
		$criteria_1 = new CDbCriteria();
		$criteria_1->compare('email', $this->name, true);

		$criteria_2 = new CDbCriteria();
		$criteria_2->compare('name', $this->name, true);

		$criteria_3 = new CDbCriteria();
		$criteria_3->compare('surname', $this->name, true);
		
		$criteria_4 = new CDbCriteria();
		$criteria_4->compare('patronymic', $this->name, true);		
		
		$criteria->mergeWith($criteria_1, 'OR');
		$criteria->mergeWith($criteria_2, 'OR');
		$criteria->mergeWith($criteria_3, 'OR');
		$criteria->mergeWith($criteria_4, 'OR');

		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		 
		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'sort'=>array(
                'defaultOrder'=>'name DESC',
                'attributes'=>array(
                    '*',
                    ),
             ),
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
	}

	protected function beforeSave()
    {
        if(parent::beforeSave())
        {            
				if($this->isNewRecord)
					$this->date_create = date('Ymd');
				
				if(!empty($this->new_password))
					$this->password =self::cryptPass($this->new_password);           
                
            return true;
        }
        else
            return false;
    }    
    
}

?>
