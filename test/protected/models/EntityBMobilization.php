<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Информация о мобилизованной технике
 *
 * @author vench
 */
class EntityBMobilization extends CActiveRecord {
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
	return parent::model($className);
    }
    
    /**
     * Получить список мобилизуемой техники
     * @param type $projectId
     * @return array
     */
    public static function dataListByProject($projectId) {
        $models = EntityBuilding::model()->findAll(array(
            'condition'=>'building.project_id = :project_id AND entity.use_mobile = 1 AND t.count_mobile > 0',
            'params'=>array(  ':project_id'=>$projectId,  ),
            'having'=>'IFNULL(SUM(mobilizations.count),0) < t.count_mobile',
            'group'=>'t.entity_building_id',
            'with'=>array(
                'entity'=>array('select'=>'name',), 
                'building'=>array('select'=>false,),
                'mobilizations'=>array('select'=>'count')
             ),
            'select'=>'t.entity_building_id, t.count_mobile, SUM(mobilizations.count) as asValue',
        ));        
        $data = array();
        foreach($models as $model) {  
           $data[$model->entity_building_id] =  $model->entity->name.' (м. '.$model->count_mobile.' ост. '.($model->count_mobile-(int)$model->asValue).') ';
        }
        return $data;
    }
     
    
    /**
     * @return string 
    */
    public function tableName()
    {  
	return '{{EntityBMobilization}}';
    }
    
    /**
      *
     * @return array 
    */
    public function relations() {
        return array(
            'entityBuilding'=>array(self::BELONGS_TO, 'EntityBuilding', 'entity_building_id'),
            'pricesEn'=>array(self::BELONGS_TO, 'PricesEntity', 'prices_id'),
        );    
    }
    
    /**
     * 
     * @return array
     */
    public function rules() {
        return array(
            array('count,price,entity_building_id, project_id', 'required'),
            array('prices_id', 'safe'),
            array('count', 'testCount'),
        );
    }
    
    /**
     * Проверяем наличие нужного количества - мобилизуемое не должно привышать фактическое кол-во техгники одного типа в записи entity_building_id
     * @param type $attribute
     * @param type $params
     */
    public function testCount($attribute,$params) {
        if(!$this->entity_building_id || is_null($this->entityBuilding)) {
            $this->addError('count','Что бы указать кол-во вы долдны сначала выбрать ед. техники');
            return;
        }
        //отнимает от заявленой на моб то что уже моб.
        $mob = $this->entityBuilding->count_mobile - $this->entityBuilding->getSumMobile($this->mobil_id);
        if($mob < $this->count) {
            $this->addError('count','Выбраное вами кол-во мобилизуемой техники привышает фактическое кол-во мобилизуемой техники (доступно '.$mob.').');
        }
    }
    
    /**
     * 
     * @return array
     */
    public function attributeLabels() {
        return array(
            'count'=>'Размер',
            'price'=>'Цена',
            'entity_building_id'=>'Техника',
        );
    }
    
    /**
     * 
     * 
     * @return \CActiveDataProvider
     */
    public function search() {
	$criteria = new CDbCriteria();
	$criteria->condition = 't.project_id = ' . $this->project_id;
	$criteria->with = array('entityBuilding'=>array('with'=>array('entity')),); 
        $criteria->with = array('pricesEn'=>array('with'=>array('provider')),); 
        
		
	return new CActiveDataProvider(get_class($this), array(
	    'criteria' => $criteria, 
	    'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
	));
    }
}
