<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Класс AR города.
 *
 * @author v.raskin
 */
class Cities extends CActiveRecord {
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
     
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{Cities}}';
    }
	
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(
			'region' => array(self::BELONGS_TO, 'Regions', 'region_id'),
			'provider' => array(self::HAS_MANY, 'Provider', 'city_id'),
		); 
	}
	
	public function rules()
	{
		return array(
			array('name, region_id', 'required'),
			
			array('name', 'length', 'max' => 50),
			array('region_id', 'safe'),
			
			array('name, region_id', 'safe', 'on' => 'search'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'region_id' => 'Регион',
			'name' => 'Название',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->with = array('region');
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.region_id', $this->region_id);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 't.name, region.name',
				'attributes' => array(
					'region_id' => array(
						'asc' => 'region.name',
						'desc' => 'region.name DESC',
					),
                    '*',
                ),
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ), 
		));
	}
}

?>
