<?php

class Entity extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
	
	/**
	 * Получить хэш с группами для выпадающего списка.
	 * @return array 
	 */
	public static function getDataList() {
		$models = self::model()->findAll(array(
			'select'=>'t.name,t.entity_type_id,t.entity_id,entityType.name',
			'with'=>'entityType',
		)); 
		return CHtml::listData($models, 'entity_id', 'name', 'entityType.name');
	}
	
	public function tableName()
	{  
		return '{{Entity}}';
	}
	
	public function relations()
	{
		return array(
			'entityType' => array(self::BELONGS_TO, 'EntityType', 'entity_type_id'),
			'units' => array(self::BELONGS_TO, 'Units', 'unit_id'),
			'prices' => array(self::HAS_MANY, 'PricesEntity', 'entity_id'),
                        
		);
	}
	
	public function rules()
	{
		return array(
			array('name', 'required'),
			
			array('name', 'length', 'max' => 100),
			array('unit_id, use_process, entity_type_id', 'safe'),
			array('use_process,use_mobile', 'boolean'),
			array('unit_id, entity_type_id, name', 'safe', 'on' => 'search'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'unit_id' => 'Единица измерения',
			'entity_type_id' => 'Тип',
			'name' => 'Название',
			'use_process'=>'Объект использует процесс',
                        'use_mobile'=>'Использует мобилизацию',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->with = array('units', 'entityType');
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.unit_id', $this->unit_id);
		$criteria->compare('t.entity_type_id', $this->entity_type_id);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'entity_id DESC',
                'attributes' => array(
					'unit_id' => array(
						'asc' => 'units.name',
						'desc' => 'units.name DESC',
					),
					'entity_type_id' => array(
						'asc' => 'entityType.name',
						'desc' => 'entityType.name DESC',
					),
                    '*',
                ),
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ), 
		));
	}
}

?>
