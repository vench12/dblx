<?php
 

/**
 * Класс AR цен.
 *
 * @author v.raskin
 */
class PricesEntity extends CActiveRecord  {
	
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
     
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{PricesEntity}}';
    }
	
 
	
	/**
	 *
	 * @return type 
	 */
	public function attributeLabels() {
		return array(
			'entity_id'=>'Предложение',
			'price'=>'Цена',
			'comment'=>'Комментарий',
			'date_create'=>'Дата',
			'main'=>'По умолчанию',
			'actual'=>'Актуальное предложение',
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			//по умолчанию
			array('price,provider_id,entity_id', 'required'),
			array('price', 'numerical'),
			array('comment,date_create,main,actual', 'safe'),
			array('main, actual', 'boolean'),
			//сценарий для поиска
			array('price', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(
			'entity'=>array(self::BELONGS_TO, 'Entity', 'entity_id'),
			'provider'=>array(self::BELONGS_TO, 'Provider', 'provider_id'),
		);
	}
	
	/**
	 * Создает и возвращает экземпляр \CActiveDataProvider.
	 * @return \CActiveDataProvider 
	 */
	public function buildDataProvider() { 
		$criteria = new CDbCriteria();
	    $criteria->compare('provider_id', $this->provider_id);
		$criteria->with = array(
			'entity', 'entity.entityType', 'entity.units',
		);
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		 
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                'defaultOrder'=>'prices_id DESC',
                'attributes'=>array( 
                    '*',
                 ),
             ),
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
	
	/**
	 *
	 * @return type 
	 */
	protected function afterFind() {
		$date = strtotime($this->date_create);  
		$this->date_create = date(Yii::app()->params['phpDateFormat'], $date);
		return parent::afterFind();
	}
	
	/**
	 *
	 * @return type 
	 */
	protected function beforeSave() {
		$date = strtotime($this->date_create);
		$this->date_create = date('Y-m-d', $date);
		return parent::beforeSave();
	}
	
	/**
	 *
	 * @return type 
	 */
	protected function afterSave() {
		if($this->main == 1) {
			//если главная цена, то снимаем этот признак у остальных
			self::model()->updateAll(
					array('main'=>0), 
				'main=1 AND provider_id=:provider_id AND entity_id=:entity_id AND prices_id <> :prices_id', array(
				':provider_id'=>$this->provider_id,
				':entity_id'=>$this->entity_id,
				':prices_id'=>$this->prices_id,
			));		 
		}
		return parent::afterSave();
	}
	
	public function prices($provider_id = 0)
	{
		if (intval($provider_id) > 0)
			return $this->findAll('provider_id=:provider_id AND actual=1 ORDER BY date_create DESC', array(':provider_id' => intval($provider_id)));
		else
			return $this->findAll('actual=1');
	}
	
	public function isActual()
	{
		echo ($this->actual) ? '<i class="icon-ok"></i> Да' : 'Нет';
	}
	
	public function isMain()
	{
		echo ($this->main) ? '<i class="icon-ok"></i> Да' : 'Нет';
	}
}

?>
