<?php

class Question extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{Questions}}';
    }
	
	public function rules()
	{
		return array(
			array('question, group_question_id', 'required'),
			
			array('question', 'length', 'max' => 150),
			array('parent_question_id', 'numerical', 'min' => 0),
			array('group_question_id', 'numerical', 'min' => 0),
			
			array('question', 'safe', 'on' => 'search'),
		);
	}
	
	public function relations()
	{
		return array(
			'group' => array(self::BELONGS_TO, 'GroupQuestion', 'group_question_id'),
		);
	}
	
	public function attributeLabels()
	{ 
		return array(
			'question' => 'Параметр проекта / вопрос',
			'parent_question_id' => 'Зависимость от вопроса',
			'group_question_id' => 'Группа параметров проекта / вопроса'
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->with = array('group');
		$criteria->compare('t.question', $this->question, true);
		$criteria->compare('group.group_question_id', $this->group_question_id);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 't.question_id DESC',
				'attributes' => array(
					'group_question_id' => array(
						'asc' => 'group.name',
						'desc' => 'group.name DESC',
					),
					'*',
				),
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
		));
	}
	
	public function listQuestions()
	{
		return CHtml::listData($this->findAll(array('order' => 'question')), 'question_id', 'question');
	}
}

?>