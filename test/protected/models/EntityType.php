<?php

/**
 * Класс AR типы сущностей.
 *
 * @author v.raskin
 */
class EntityType extends CActiveRecord {
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{EntityType}}';
    }
	
	public function listTypes()
	{
		return CHtml::listData($this->findAll(), 'entity_type_id', 'name');
	}
	
	public function rules()
	{
		return array(
			array('name', 'required'),
			
			array('name', 'length', 'max' => 50),
			
			array('name', 'safe', 'on' => 'search'),
		);
	}
	
	public function attributeLabels()
	{ 
		return array(
			'name' => 'Название',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'entity_type_id DESC',
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
		));
	}
}

?>
