<?php

class StatusProject extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return 'StatusProject';
    }
	
	public function listStatuses()
	{
		return CHtml::listData($this->findAll(), 'status_id', 'name');
	}
}

?>