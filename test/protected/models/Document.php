<?php

class Document extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{ProjectsDocs}}';
    }
	
	public $documentField;
	
	public function relations()
	{
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'type' => array(self::BELONGS_TO, 'TypeDocument', 'doc_type_id'),
		); 
	}
	
	public function attributeLabels()
	{ 
		return array(
			'doc_type_id' => 'Тип',
			'project_id' => 'Проект',
			'price' => 'Цена',
			'file' => 'Файл документа',
			'documentField' => 'Файл документа',
		);
	}
	
	public function rules()
	{
		return array(
			array('doc_type_id, price', 'required'),
			
			array('price', 'numerical'),
			
			array('documentField', 'file', 'safe' => true, 'types' => Yii::app()->params['extensions']['documents'], 'allowEmpty' => true,),
			
			array('doc_type_id, price', 'safe', 'on' => 'search'),
			
			array('doc_type_id', 'testUniqueDocument'),
		);
	}
	
	public function testUniqueDocument()
	{
		if ($this->isNewRecord)
		{
			$model = $this->find('project_id=:project_id AND doc_type_id=:doc_type_id', array(
				':project_id' => $this->project_id,
				':doc_type_id' => $this->doc_type_id,
			));
			if(!is_null($model))
				$this->addError('doc_type_id', 'Документ данного типа уже загружен');
		}
	}
	
	public function search($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 't.project_id = ' . intval($id);
		$criteria->with = array('type');
		$criteria->compare('t.price', $this->price, true);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'type.name',
				'attributes' => array(
					'doc_type_id' => array(
						'asc' => 'type.name',
						'desc' => 'type.name DESC',
					),
                    '*',
                ),
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
		));
	}
	
	protected function beforeSave()
	{
		$file = CUploadedFile::getInstance($this, 'documentField'); 
		if(!is_null($file))
		{ 
			$fileName = md5($file->name . time()).'.'.$file->extensionName;
			$uploadsPath = Yii::app()->params['dirUploads'];
			$path = Yii::app()->basePath . '/../' . $uploadsPath . '/' . strtolower(get_class($this)) . '/';
			if (!is_dir($path))
				mkdir($path, 0777);
			
			if ($file->saveAs($path.$fileName))
			{
				if (is_file($this->file))
					unlink($this->file);
				$this->file = $fileName;
			}
		}
		return parent::beforeSave();
	}
	
	public function getUrl()
	{
		return Yii::app()->baseUrl . '/' . Yii::app()->params['dirUploads'] . '/' . strtolower(get_class($this)) . '/' . $this->file;
	}
	
	public function add($project_id, $doc_type_id)
	{
		$command = Yii::app()->db->createCommand();
		$models = $this->find('project_id=:project_id AND doc_type_id=:doc_type_id', array(
			':project_id' => intval($project_id),
			':doc_type_id' => intval($doc_type_id),
		));
		if (is_null($models))
		{
			$command->insert('ProjectsDocs', array(
				'project_id' => intval($project_id),
				'doc_type_id' => intval($doc_type_id)
			));
		}
	}
}

?>