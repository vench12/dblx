<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Класс AR выроботки объекта к материалу.
 *
 * @author v.raskin
 */
class EntityBuildingAdd extends CActiveRecord {
	
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{EntityBuildingAdd}}';
    }
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() {
		return array(
			'size'=>'Размер от объема',
			'date_start'=>'Дата',
			'material_id'=>'Материал',
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			array('size', 'numerical', 'min'=>0),
			array('size', 'testSizeMax'),
			array('date_start', 'safe'), 
		);
	}
	
	/**
	 * Проверяем максимальный придел используемого материала.
	 * @param type $attribute
	 * @param type $params 
	 */
	public function testSizeMax($attribute,$params) { 
		$query = 'SELECT (bm.size - 
				(SELECT IFNULL(SUM(ba.size), 0)
FROM {{EntityBuildingAdd}} ba 
INNER JOIN {{EntityBuilding}} eb2 ON (eb2.entity_building_id = ba.entity_building_id)
WHERE ba.material_id = bm.material_id  AND eb2.build_id = bm.build_id AND ba.entity_building_add_id <> :entity_building_add_id)
					) AS s  
					FROM {{EntityBuilding}} AS eb 					
					INNER JOIN {{BuildingMaterials}} bm ON (bm.`material_id` = :material_id ) AND (eb.build_id = bm.build_id )
				 
					WHERE eb.`entity_building_id` = :entity_building_id ';
		$db = $this->getDbConnection();
		$command = $db->createCommand($query);
		$command->params = array(
			':entity_building_id'=>$this->entity_building_id,
			':entity_building_add_id'=>$this->entity_building_add_id,
			':material_id'=>$this->material_id
		);		
		$row = $command->queryRow();
	 
		if(isset($row['s']) && $row['s'] < $this->size) {
			$this->addError('size','Объем вышел за пределы');
		}
	}
		
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array( 
			'material'=>array(self::BELONGS_TO, 'Materials', 'material_id'), 
		);
	}
	
	/**
	 *
	 * @return boolean 
	 */
	protected function afterFind() { 
		
		if (isset($this->date_start)) {
			$date = strtotime($this->date_start);
			$this->date_start = date('Y-m-d', $date); 
		}
		
		return parent::afterFind();
	}
	
	/**
	 *
	 * @return boolean 
	 */
	protected function beforeSave() { 
		if (isset($this->date_start)) {
			$date = strtotime($this->date_start);
			$this->date_start = date('Y-m-d', $date);
		}  else {
			$this->date_start = date('Y-m-d');
		}
		return parent::beforeSave();
	}
}

?>
