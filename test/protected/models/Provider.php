<?php
 

/**
 * Класс AR поставщиков.
 *
 * @author v.raskin
 */
class Provider extends CActiveRecord {
	
	/**
	 *
	 * @var integer 
	 */
	public $region_id;
	
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
	
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
 
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{Provider}}';
    }
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() { 
		return array(
			'name'=>'Название',
			'city_id'=>'Город',
			'address'=>'Адрес',
			'phone'=>'Телефон',
			'region_id'=>'Регион',
		);
	}
	
	/**
	 * Создает и возвращает экземпляр \CActiveDataProvider.
	 * @return \CActiveDataProvider 
	 */
	public function buildDataProvider() { 
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.city_id', $this->city_id, true);
		$criteria->with = array(
			'city',
		); 
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		 
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                'defaultOrder'=>'provider_id DESC',
                'attributes'=>array(
                    'city_id'=>array(
                        'asc'=>'city.name',
                        'desc'=>'city.name DESC',
                        ),
                    '*',
                    ),
             ),
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
	
	/**
	 *
	 * @return type 
	 */
	public function rules() {
		return array(
			//по умолчанию
			array('name,region_id,city_id,address,phone', 'required'),
			//сценарий для поиска
			array('name,city_id', 'safe', 'on'=>'search'),
			
			array('name, phone', 'length', 'max' => 100),
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(
			'city'=>array(self::BELONGS_TO, 'Cities', 'city_id'),
		); 
	}
	
	/**
	 *
	 * @return type 
	 */
	protected function afterFind() {
		$this->region_id = !is_null($this->city) ? $this->city->region_id  : 0;
		return parent::afterFind();
	}
	
	public function listProviders($city_id = 0)
	{
		if (intval($city_id) > 0)
			return CHtml::listData($this->findAll(
				'city_id=:city_id', array(':city_id' => intval($city_id))
			), 'provider_id', 'name');
		else
			return CHtml::listData($this->findAll(), 'provider_id', 'name');
		
	}
	
	// список только тех регионов, где есть поставщики
	public static function listRegions()
	{
		$data = Regions::model()->findAll(array(
			'select' => 't.name, t.region_id',
			'with' => array('city.provider' => array('joinType' => 'INNER JOIN')),
		));
		return CHtml::listData($data, 'region_id', 'name');
	}
	
	// список только тех городов, где есть поставщики
	public static function listCities($region_id = 0)
	{
		$data = Cities::model()->findAll(array(
			'select' => 't.name, t.city_id',
			'with' => array('provider' => array('joinType'=>'inner JOIN')),
			'condition' =>  (intval($region_id) > 0) ? 'region_id = ' . intval($region_id) : '',
		));
		return CHtml::listData($data, 'city_id', 'name');
	}
}

?>
