<?php

/**
 * Класс AR типы единиц измерений.
 *
 * @author v.raskin
 */
class Units extends CActiveRecord {
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{Units}}';
    }
	
	public function listUnits()
	{
		return CHtml::listData($this->findAll(), 'unit_id', 'name');
	}
	
	public function rules()
	{
		return array(
			array('name, name_short', 'required'),
			
			array('name', 'length', 'max' => 20),
			array('name_short', 'length', 'max' => 8),
			
			array('name, name_short', 'safe', 'on' => 'search'),
		);
	}
	
	public function attributeLabels()
	{ 
		return array(
			'name' => 'Название',
			'name_short' => 'Сокращенное название',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.name_short', $this->name_short, true);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'unit_id DESC',
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
		));
	}
}

?>
