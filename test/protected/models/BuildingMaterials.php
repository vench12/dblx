<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Класс AR материалы здания.
 *
 * @author v.raskin
 */
class BuildingMaterials extends CActiveRecord {
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
     
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{BuildingMaterials}}';
    }
	
	public function attributeLabels() {
		return array(
			'size'=>'Размер',
			'material_id'=>'Материал',
		);	
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			array('size,material_id,build_id', 'required'),
			array('size', 'numerical', 'min'=>1),
			array('material_id', 'testUnique'),
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(
			'material'=>array(self::BELONGS_TO, 'Materials', 'material_id'),
			'building'=>array(self::HAS_ONE, 'Building', 'build_id'),
		);
	}
	
	/**
	 * Проверка уникальности.
	 * @param type $attribute
	 * @param type $params 
	 */
	public function testUnique($attribute,$params) {
		if(!$this->isNewRecord) {
			return;
		}
		$models = self::model()->find('build_id=:build_id AND material_id=:material_id',array(
			':material_id'=>$this->material_id,
			':build_id'=>$this->build_id,
		));
		if(!is_null($models)) {
			$this->addError('material_id','У здания уже есть такой материал');
		}
	}
}

?>
