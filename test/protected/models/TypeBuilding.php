<?php

 

/**
 * Класс AR тип зданий.
 *
 * @author v.raskin
 */
class TypeBuilding extends CActiveRecord {
	
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
 
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{TypeBuilding}}';
    }
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() {
		return array(
			'name'=>'Название',
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			//по умолчанию
			array('name', 'required'), 
			array('name', 'length', 'min'=>1,'max'=>'120'),  
			//сценарий для поиска
			array('name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 *
	 * @return \CActiveDataProvider 
	 */
	public function buildDataProvider() {
		$criteria = new CDbCriteria(); 	
		$criteria->compare('t.name', $this->name, true);
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
}

?>
