<?php

class TypeDocument extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{DocumentType}}';
    }
	
	public function rules()
	{
		return array(
			array('name', 'required'),
			
			array('name', 'length', 'max' => 100),
			
			array('name', 'safe', 'on' => 'search'),
		);
	}
	
	public function attributeLabels()
	{ 
		return array(
			'name' => 'Тип',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'doc_type_id DESC',
            ),
			'pagination' => array(
				'pageSize' => Yii::app()->params['postsPerPage']
			),
		));
	}
	
	public function listTypes()
	{
		return CHtml::listData($this->findAll(array('order' => 'name')), 'doc_type_id', 'name');
	}
}

?>