<?php

 

/**
 * Класс AR Шаблонов - методов сноса зданий.
 *
 * @author v.raskin
 */
class Template extends CActiveRecord {
	
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
	
	/**
	 * Получить список шаблонов по заданным критериям поиска. 
	 * @param integer $type_build_id
	 * @param integer $type_build_place_id
	 * @param integer $height
	 * @return /CActiveRecord 
	 */
	public static function getTemplates($type_build_id, $type_build_place_id, $height) {
		 
		$models = Template::model()->findAll(array(
			'select'=>'template_id,name',
			'condition'=>'type_build_id=:type_build_id AND type_build_place_id=:type_build_place_id AND ((min_height <= :height_1 AND max_height >= :height_2) OR (max_height = 0 AND min_height = 0))',
			'params'=>array(
				':type_build_id'=>$type_build_id,
				':type_build_place_id'=>$type_build_place_id,
				':height_1'=>$height,
				':height_2'=>$height,
			),
		));
		return $models;
	}
 
	/**
	 * Получить текст с описание требуемой высоты применения шаблона.
	 * @return string 
	 */
	public function getHeightText() {
		if($this->min_height > 0 && $this->max_height > 0) {
			return 'от '.$this->min_height.' до '.$this->max_height.'';
		} else if($this->min_height > 0) {
			return 'от '.$this->min_height.'';
		} else if($this->max_height > 0) {
			return 'до '.$this->max_height.'';
		}
		return 'любая';
	}
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{Template}}';
    }
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() {
		return array(
			'name'=>'Название',
			'type_build_id'=>'Тип здания',
			'type_build_place_id'=>'Тип площадки',
			'min_height'=>'Минимальная высота сносимого здания',
			'max_height'=>'Максимальная высота сносимого здания',
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			//по умолчанию
			array('name,type_build_id,type_build_place_id', 'required'),
			array('min_height, max_height', 'safe'),
			array('min_height, max_height', 'numerical'),
			array('name', 'length', 'max'=>120),
			array('min_height', 'testMinMaxHeight'), 
			//сценарий для поиска
			array('name,type_build_id,type_build_place_id', 'safe', 'on'=>'search'),
		);
	}
	
	 
	/**
	 * Проверка высоты.
	 */		
	public function testMinMaxHeight($attribute,$params) { 
		if($this->min_height > $this->max_height) {
			$this->addError('min_height','Минимальная высота больше максимальной');
		}
	}


	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(
			'typeBuilding'=>array(self::BELONGS_TO, 'TypeBuilding', 'type_build_id'),
			'typeBuildingPlace'=>array(self::BELONGS_TO, 'TypeBuildingPlace', 'type_build_place_id'),
		);
	}
	
	/**
	 * Создает и возвращает экземпляр \CActiveDataProvider.
	 * @return \CActiveDataProvider 
	 */
	public function buildDataProvider() { 
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.type_build_id', $this->type_build_id);
		$criteria->compare('t.type_build_place_id', $this->type_build_place_id); 
		$criteria->with = array('typeBuilding', 'typeBuildingPlace');
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                'defaultOrder'=>'template_id DESC',
                'attributes'=>array(
                    'type_build_id'=>array(
                        'asc'=>'typeBuilding.name',
                        'desc'=>'typeBuilding.name DESC',
                        ),
					'type_build_place_id'=>array(
                        'asc'=>'typeBuildingPlace.name',
                        'desc'=>'typeBuildingPlace.name DESC',
                        ),
                    '*',
                    ),
             ),/**/
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
}

?>
