<?php

class QuestionsDocs extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{QuestionsDocs}}';
    }
	
	public function relations()
	{
		return array(
			'typedocument' => array(self::BELONGS_TO, 'TypeDocument', 'doc_type_id'),
			'question' => array(self::BELONGS_TO, 'Question', 'question_id'),
		);
	}
	
	public function rules()
	{
		return array(
			array('doc_type_id, question_id', 'required'),
			
			array('doc_type_id', 'numerical', 'min' => 0),
			array('question_id', 'numerical', 'min' => 0),
			
			array('doc_type_id, question_id', 'safe', 'on' => 'search'),
			
			array('doc_type_id, question_id', 'testUnique'),
		);
	}
	
	public function testUnique()
	{
		if (!$this->isNewRecord)
			return;
		
		$models = $this->find('doc_type_id=:doc_type_id AND question_id=:question_id',array(
			':doc_type_id'=>$this->doc_type_id,
			':question_id'=>$this->question_id,
		));
		if (!is_null($models))
		{
			$this->addError('doc_type_id', 'Такое отношение уже существует');
			$this->addError('question_id', 'Такое отношение уже существует');
		}
	}
	
	public function attributeLabels()
	{ 
		return array(
			'doc_type_id' => 'Тип документа',
			'question_id' => 'Параметр проекта / вопрос',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->with = array('typedocument', 'question');
		$criteria->compare('typedocument.doc_type_id', $this->doc_type_id);
		$criteria->compare('question.question_id', $this->question_id);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'question.question, typedocument.name',
				'attributes' => array(
					'doc_type_id' => array(
						'asc' => 'typedocument.name',
						'desc' => 'typedocument.name DESC',
					),
					'question_id' => array(
						'asc' => 'question.question',
						'desc' => 'question.question DESC',
					),
					'*',
				),
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
		));
	}
	
	public function getQuestsDocs($question_id)
	{
		$data = $this->findAll('question_id=:question_id', array(':question_id' => intval($question_id)));
		return $data;
	}
}

?>