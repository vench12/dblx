<?php

class GroupQuestion extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{GroupsQuestions}}';
    }
	
	public function rules()
	{
		return array(
			array('name', 'required'),
			
			array('name', 'length', 'max' => 50),
			
			array('name', 'safe', 'on' => 'search'),
		);
	}
	
	public function attributeLabels()
	{ 
		return array(
			'name' => 'Группа параметров проекта / вопроса'
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'group_question_id DESC',
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
		));
	}
	
	public function listGroups()
	{
		return CHtml::listData($this->findAll(array('order' => 'name')), 'group_question_id', 'name');
	}
}

?>