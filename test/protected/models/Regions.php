<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Класс AR регионов.
 *
 * @author v.raskin
 */
class Regions extends CActiveRecord {
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
     
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{Regions}}';
    }
	
	public function rules()
	{
		return array(
			array('name', 'required'),
			
			array('name', 'length', 'max' => 50),
			
			array('name', 'safe', 'on' => 'search'),
		);
	}
	
	public function relations() {
		return array(
			'city' => array(self::HAS_MANY, 'Cities', 'region_id'),
		); 
	}
	
	public function attributeLabels()
	{
		return array(
			'name' => 'Название',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'name',
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ), 
		));
	}
}

?>
