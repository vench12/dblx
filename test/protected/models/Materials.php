<?php

class Materials extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{Materials}}';
    }
	
	public function listMaterials($is_backfill = false)
	{
		$cond = ($is_backfill) ? 'is_backfill = 1' : '';
		return CHtml::listData($this->findAll($cond), 'material_id', 'name');
	}
	
	public function rules()
	{
		return array(
			array('name, coeff_of_loss', 'required'),
			
			array('name', 'length', 'max' => 100),
			array('coeff_of_loss', 'numerical'),
			array('is_backfill', 'boolean'),
			
			array('name, coeff_of_loss', 'safe', 'on' => 'search'),
		);
	}
	
	public function attributeLabels()
	{ 
		return array(
			'name' => 'Название',
			'coeff_of_loss' => 'Коэффициент разрыхления',
			'is_backfill' => 'Использ. при засыпке',
		);
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.coeff_of_loss', $this->coeff_of_loss, true);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'material_id DESC',
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ),
		));
	}
	
	public function isBackfill()
	{
		echo ($this->is_backfill) ? '<i class="icon-ok"></i> Да' : 'Нет';
	}
}

?>