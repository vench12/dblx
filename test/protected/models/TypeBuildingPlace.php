<?php
 

/**
 * Класс AR тип строительной площадки.
 *
 * @author v.raskin
 */
class TypeBuildingPlace extends CActiveRecord {
		
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
	/**
	 *
	 * @var mixed 
	 */
	public $imageField;
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
 
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{TypeBuildingPlace}}';
    }
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() {
		return array(
			'name'=>'Название',
			'image'=>'Изображение',
			'imageField'=>'Изображение',
		);
	}
	
	/**
	 * Получить URL адрес фото
	 * @return string 
	 */
	public function getImageUrl() {
		$image = $this->image;
		
		return  Yii::app()->baseUrl. str_replace($this->getBasePath(), '', $image);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			//по умолчанию
			array('name', 'required'), 
			array('name', 'length', 'min'=>1,'max'=>'120'),  
			array('imageField', 'file', 'safe'=>true, 'types'=>Yii::app()->params['extensions']['images'], 'allowEmpty'=>true,),
			array('image', 'safe'),
			//сценарий для поиска
			array('name', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * Базовый путь загрузки фото
	 */
	public function getBasePath() {
		return Yii::app()->basePath.'/..';
	}
	
	/**
	 *
	 * @return \CActiveDataProvider 
	 */
	public function buildDataProvider() {
		$criteria = new CDbCriteria(); 	
		$criteria->compare('t.name', $this->name, true);
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
	
	/**
	 *
	 * @return boolean 
	 */
	protected function beforeSave() {
		$file =  CUploadedFile::getInstance($this, 'imageField'); 
		if(!is_null($file)) { 
			$fileName = MD5($file->name . time()).'.'.$file->extensionName;
			$uploadsPath = isset(Yii::app()->params['dirUploads']) ? Yii::app()->params['dirUploads'] : 'uploads';
			$path = '/'.$uploadsPath.'/'.strtolower(get_class($this)).'/';
			$root = $this->getBasePath();
			if(!is_dir($root.$path)){
				mkdir($path, 0777);
			}
			if($file->saveAs($root.$path.$fileName)) {
				if(is_file($this->image)) {
					unlink($this->image);
				}
				$this->image = $path.$fileName;
			}
		} 
		//var_dump($file, $this->image);  exit();
		return parent::beforeSave();
	}
}

?>
