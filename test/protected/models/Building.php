<?php

/**
 * Класс AR здание в проекте.
 *
 * @author v.raskin
 */
class Building  extends CActiveRecord  {
	
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
     
	/**
	 * Попытка установить зданию новый шаблон после обновления AC.
	 * @param integer $oldTemplateID ИД шаблона
	 * @return void 
	 */
	public function tryChangeTemplate($oldTemplateID) {
		if($this->template_id == $oldTemplateID) {
			return;
		}
		
		//удаляем все что осталось от старого шаблона
		if($oldTemplateID > 0) {
			EntityBuilding::model()->deleteAll('build_id =:build_id 
				AND template_item_id IN (SELECT template_item_id FROM {{TemplateItem}} WHERE template_id = :template_id)', array(
				':build_id'=>$this->build_id,
				':template_id'=>$oldTemplateID,
			));
		}	
		
	    //добаляем элементы новго шаблона
		$items = TemplateItem::model()->findAll(array(  
				'condition'=>'template_id=:template_id',
				'params'=>array(':template_id'=>$this->template_id),
				'order'=>'order_field',
		)); 
		if(!is_null($items)) {
			$add = null;
		 
			foreach($items as $item) {  
				$entity_building_id = is_null($add) ? 0 : $add->entity_building_id;
				$add = new EntityBuilding();
				$add->build_id = $this->build_id;
				$add->entity_id = $item->entity_id;
				$add->parent_entity_building_id = $entity_building_id;
				$add->count = $item->count;
				$add->price = 0;
				$add->prices_id = 0;
				$add->template_item_id = $item->template_item_id;
				$add->save(false); 
			}
		}	
	}
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{Building}}';
    }
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() {
		return array(
			'name'=>'Название',
			'type_build_id'=>'Тип здание',
			'type_build_place_id'=>'Тип строительной площадки',
			'height'=>'Высота',
			'width'=>'Ширина',
			'depth'=>'Длинна',
			'template_id'=>'Метод сноса',
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			array('name,type_build_id,type_build_place_id,height,width,depth,project_id', 'required'),	
			array('height,width,depth','numerical','min'=>1),
			array('template_id', 'safe'),
		);	
	}
	
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(
			'project'=>array(self::BELONGS_TO, 'Project', 'project_id'),
                        'entityBuildings'=>array(self::HAS_MANY, 'EntityBuilding', 'build_id'),
		);
	}
	
	/**
	 *
	 * @return \CActiveDataProvider 
	 */
	public function dataProviderMaterials() {
		$criteria = new CDbCriteria(); 	
		$criteria->with = array('material');
		$criteria->compare('t.build_id', $this->build_id);
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		
		$dataProvider = new CActiveDataProvider(new BuildingMaterials(), array(
			'criteria'=>$criteria, 
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
	
	/**
	 *
	 * @return \CActiveDataProvider 
	 */
	public function dataProviderEntities() {
		$criteria = new CDbCriteria(); 	
		$criteria->with = array('entity', 'templateItem');
		$criteria->compare('t.build_id', $this->build_id); 
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		
		$dataProvider = new CActiveDataProvider(new EntityBuilding(), array(
			'criteria'=>$criteria, 
			'sort'=>array(
				'defaultOrder'=>'templateItem.required DESC, t.template_item_id DESC',
			),
			'pagination'=>array(
                             'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
 
}

?>
