<?php

class EntityProductivity extends CActiveRecord
{	
    public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return '{{EntityProductivity}}';
    }
	
	public function relations()
	{
		return array(
			'materials' => array(self::BELONGS_TO, 'Materials', 'material_id'),
			'units' => array(self::BELONGS_TO, 'Units', 'unit_id'),
		);
	}
	
	public function rules()
	{
		return array(
			array('material_id, productivity, unit_id', 'required'),
			
			array('productivity', 'numerical'),
			array('material_id, unit_id', 'safe'),
			
			array('material_id, productivity, unit_id', 'safe', 'on' => 'search'),
			
			array('material_id', 'testUniqueMaterial'),
		);
	}
	
	public function testUniqueMaterial()
	{
		if ($this->isNewRecord)
		{
			$model = $this->find('entity_id=:entity_id AND material_id=:material_id', array(
				':entity_id' => $this->entity_id,
				':material_id' => $this->material_id,
			));
			if(!is_null($model))
				$this->addError('material_id', 'Данный материал уже используется');
		}
	}
	
	public function attributeLabels()
	{
		return array(
			'material_id' => 'Материал',
			'productivity' => 'Производительность',
			'unit_id' => 'Единица измерения',
		);
	}
	
	public function search($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 't.entity_id = ' . intval($id);
		$criteria->with = array('materials', 'units');
		$criteria->compare('t.unit_id', $this->unit_id);
		$criteria->compare('t.productivity', $this->productivity, true);
		$criteria->compare('t.material_id', $this->material_id);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'entity_id DESC',
                'attributes' => array(
					'material_id' => array(
						'asc' => 'materials.name',
						'desc' => 'materials.name DESC',
					),
					'unit_id' => array(
						'asc' => 'units.name',
						'desc' => 'units.name DESC',
					),
                    '*',
                ),
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ), 
		));
	}
}

?>