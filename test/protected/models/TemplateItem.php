<?php



/**
 * Класс AR Элементов шаблона.
 *
 * @author v.raskin
 */
class TemplateItem extends CActiveRecord {
	
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
	
	    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{TemplateItem}}';
    }
	
	
	/**
	 * Создает и возвращает экземпляр \CActiveDataProvider.
	 * @return \CActiveDataProvider 
	 */
	public function buildDataProvider() {
		$criteria = new CDbCriteria(); 		
		$criteria->compare('t.template_id', $this->template_id);
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			/*'sort'=>array(
                'defaultOrder'=>'provider_id DESC',
                'attributes'=>array(
                    'city_id'=>array(
                        'asc'=>'city.name',
                        'desc'=>'city.name DESC',
                        ),
                    '*',
                    ),
             ),*/
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() {
		return array(
			'count'=>'Количество',
			'required'=>'Обязательный элемент для сноса',
			'order_field'=>'Порядковый вывод',
			'entity_id'=>'Объект',
		);
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			//по умолчанию
			array('count,entity_id,template_id,order_field', 'required'),
			array('required', 'safe'),
			array('required', 'boolean'),
			array('order_field,count', 'numerical', 'min'=>1),   		 
			array('entity_id', 'testUniqueEntity'),
			//сценарий для поиска
			//array('name', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * Проверка на уникальность объекта в шаблоне.
	 * @param type $attribute
	 * @param type $params 
	 */
	public function testUniqueEntity($attribute,$params) {
		if(!$this->isNewRecord) {
			return;
		}
		$models = self::model()->find('entity_id = :entity_id AND template_id=:template_id', array(
			':template_id'=>$this->template_id,
			':entity_id'=>$this->entity_id,
		));
		if(!is_null($models)) {
			$this->addError('entity_id','В шаблоне уже есть такой объект');
		}
	}
	
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(
			'entity'=>array(self::BELONGS_TO, 'Entity', 'entity_id'),
			'template'=>array(self::BELONGS_TO, 'Template', 'template_id'),
		);
	}
}

?>
