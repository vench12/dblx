<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Класс AR объекты к зданию.
 *
 * @author v.raskin
 */
class EntityBuilding extends CActiveRecord {
	
	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
    public $pageSize = NULL;
    
    /**
     * Псевдо-поле для извлечения  данных агригаций в сложных запросах  CActiveRecord 
     * Типо (SUM(mobilizations.count) AS asValue)
     * @var string 
     */
    public $asValue;
	
    /**
     *
     * @param string $className
     * @return CActiveRecord 
     */
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
     
    
    /**
     * @return string 
    */
    public function tableName()
    {  
		return '{{EntityBuilding}}';
    }
	
	/**
	 * Указатель того, что объект используется в процессе - тогда он обрабатывает материалы и учавствует в графике.
	 * @return boolean 
	 */
	public function useProcess() {
		return !is_null($this->entity) && ($this->entity->use_process == 1);
	}
        
        /**
         * Получить количество мобилизуемо техники (отмеченной)
         * @param int $name ИД мобилизации которое следует игнорировать.
         * @return int
         */
        public function getSumMobile($ignoreId =- 1) {
            $count = 0;
            foreach($this->mobilizations as $mobilization) {
                if($ignoreId == $mobilization->mobil_id) {
                    continue;
                }
                $count += $mobilization->count;
            }
            return $count;
        }
	
	/**
	 *
	 * @return array 
	 */
	public function attributeLabels() {
		return array(
			'entity_id'=>'Объект-сущность',
			'parent_entity_building_id'=>'родитель',
			'count'=>'Количество',
                        'count_mobile'=>'Количество мобилизуемой техники',            
			'price'=>'Цена',
			'prices_id'=>'Ид цены',
			'template_item_id'=>'Шаблон',
			'date_start'=>'Дата начала в графике',//дата начала заступления на работу
                        'working_day'=>'Количество часов в смене',
		);
	}
	
	/**
	* Получить материалы с которыми работатет объект
	* @return /array|NULL
	*/
	public function getMaterialsList() {
		if(!$this->useProcess()) {
			return NULL;
		}
		$db = $this->getDbConnection();
		$command = NULL;
		if($this->template_item_id > 0) {
			//по шаблону
			$query = 'SELECT (bm.size - IFNULL(
						(
							SELECT SUM(ma.size) FROM `EntityBuildingAdd` ma 
							WHERE (bm.`material_id` = ma.material_id)  AND
							(ma.`entity_building_id` IN ( 
								SELECT eb.entity_building_id FROM EntityBuilding eb WHERE eb.build_id = bm.`build_id`))
						)
					 , 0)) AS size, m.name, bm.`material_id`, ep.productivity , "" AS date_start 
					 FROM `BuildingMaterials` bm 
					 INNER JOIN Materials m ON (bm.`material_id` = m.material_id)
                                         INNER JOIN EntityProductivity ep ON (ep.entity_id = :entity_id) AND (ep.material_id = bm.material_id)
					 WHERE bm.`build_id` = :build_id';
			
			$command = $db->createCommand($query);
			$command->params = array(
				':build_id'=>$this->build_id, 
                                ':entity_id'=>$this->entity_id,
			);
		  
		} else {
			//индивидуально установлено
			$query = 'SELECT ma.size, m.name, m.`material_id`, ep.productivity , ma.date_start
                                  FROM 
				  `EntityBuildingAdd` ma 
				  INNER JOIN Materials  m ON (m.`material_id` = ma.material_id)
                                  INNER JOIN EntityProductivity ep ON (ep.entity_id = :entity_id) AND (ep.material_id = ma.material_id)
				  WHERE ma.`entity_building_id` = :entity_building_id';
			$command = $db->createCommand($query);
			$command->params = array(
					':entity_building_id'=>$this->entity_building_id, 
                                        ':entity_id'=>$this->entity_id, 
			);
		}

		$rows = !is_null($command) ? $command->queryAll() : array(); 		
		if(sizeof($rows) > 0) {
			$data = array();
			foreach($rows as $item) {
				$data[] = array(
					'name'=>$item['name'],
					'size'=>$item['size'],
                                        'material_id'=>$item['material_id'],
                                        'productivity'=>$item['productivity'],
                                        'date_start'=>$item['date_start'],
				);
			}
			
			return $data;
		}
		
		
		return NULL;
	}
	
	/**
	 *
	 * @return array 
	 */
	public function rules() {
		return array(
			array('entity_id,count,price', 'required'),
			array('count', 'numerical', 'min'=>1),
                        array('working_day', 'numerical', 'min'=>1, 'max'=>24),
                        array('count_mobile', 'numerical', 'min'=>0, ),
                        array('count_mobile', 'testCountMobile'),
			array('parent_entity_building_id,template_item_id,date_start,prices_id', 'safe'),
		);
	}
	
        /**
         * Проверка размера мобилизации.
         * @param type $attribute
         * @param type $params
         */
        public function testCountMobile($attribute,$params) {
            if($this->count_mobile > $this->count) {
                $this->addError('count_mobile','Кол-во мобилизуемой техники больше чем фактической');
            }
        }
        
	/**
	 *
	 * @return array 
	 */
	public function relations() {
		return array(  
			'entity'=>array(self::BELONGS_TO, 'Entity', 'entity_id'),
			'building'=>array(self::BELONGS_TO, 'Building', 'build_id'),
			'templateItem'=>array(self::BELONGS_TO, 'TemplateItem', 'template_item_id'),
                        'mobilizations'=>array(self::HAS_MANY, 'EntityBMobilization', 'entity_building_id'),
		);
	}
	
	/**
	 *
	 * @return \CActiveDataProvider 
	 */
	public function buildDataProviderEntityBuildingAdd() {
		$criteria = new CDbCriteria();
		$criteria->compare('t.entity_building_id', $this->entity_building_id); 
		$criteria->with = array('material'); 
		
		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		 
		$dataProvider = new CActiveDataProvider(new EntityBuildingAdd(), array(
			'criteria'=>$criteria,
			/*'sort'=>array(
                'defaultOrder'=>'provider_id DESC',
                'attributes'=>array(
                    'city_id'=>array(
                        'asc'=>'city.name',
                        'desc'=>'city.name DESC',
                        ),
                    '*',
                    ),
             ),*/
			'pagination'=>array(
                'pageSize'=>$this->pageSize,
             ), 
		));
		
		return $dataProvider;
	}
			
	
	/**
	 *
	 * @return boolean 
	 */
	protected function beforeSave() {
                if($this->isNewRecord && empty($this->date_start)) {
                    $this->date_start = $this->building->project->date_start;
                }
		if (isset($this->date_start)) {
			$date = strtotime($this->date_start);
			$this->date_start = date('Y-m-d', $date);
		} 
                if($this->isNewRecord && $this->count_mobile == 0) {
                    //определяем количество мобилизуемой техники по умолчанию.
                    //добавляем мобилизацию равную кол-во в том случаи если техника еще не мобилизовывалась
                    $db = $this->getDbConnection();
                    $command = $db->createCommand('SELECT IFNULL(COUNT(*), 0) AS c '
                            . 'FROM {{EntityBuilding}} WHERE build_id = :build_id AND entity_id = :entity_id');
                    $command->params = array(
			 ':build_id'=>$this->build_id, 
                         ':entity_id'=>$this->entity_id,
		     );
                     $row = $command->queryRow();
                     if($row['c'] == 0) {
                         $this->count_mobile = $this->count;
                     }  
                }
		return parent::beforeSave();
	}
	
	/**
	 *
	 * @return boolean 
	 */
	protected function afterFind() {
		if (isset($this->date_start)) {
			$date = strtotime($this->date_start);
			$this->date_start = date('Y-m-d', $date);
		}
		
		if (!isset($this->prices_id))
			$this->prices_id = 0;
		
		return parent::afterFind();
	}
	
		 
}

?>
