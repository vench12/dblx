<?php

/**
 * 
 */
class Project extends CActiveRecord
{
	
	public $region_id;
	
	
	public $status_id = 1;
	
	private $sumProject = NULL;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function init() {	
		if($this->isNewRecord){  
			$query = 'SELECT MAX(number) AS c FROM   '.$this->tableName().'';
			$row = self::$db->createCommand($query)->queryRow();			
			$this->number = Yii::app()->numberFormatter->format("000000000000", (intval($row['c']) + 1));
		}
		return parent::init();
	}
	
	public function tableName()
	{  
		return '{{Projects}}';
	}
	
	
	/**
         * 
         * @return array
         */
	public function relations()
	{
		return array(
			'city' => array(self::BELONGS_TO, 'Cities', 'city_id'),
			'status' => array(self::BELONGS_TO, 'StatusProject', 'status_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
                        'buildings' => array(self::HAS_MANY, 'Building', 'project_id'),
                        'documents' => array(self::HAS_MANY, 'Document', 'project_id'),
                        'emobilizations' => array(self::HAS_MANY, 'EntityBMobilization', 'project_id'),
			'material' => array(self::BELONGS_TO, 'Materials', 'backfill_id'),
		);
	}
	
        /**
         * 
         * @return array
         */
	public function rules()
	{
		return array(
			array('region_id, city_id, status_id, number, name, address, date_start, date_end', 'required'),
			array('total_sum', 'safe'),
			array('number', 'length', 'max' => 30),
			array('name', 'length', 'max' => 100),
			array('number', 'unique',),
			
			array('cleaning, payment_climbers, backfill_id, crusher, payment_gas_device, disable_sewage, disable_electro, disable_gas, provide_electro, provide_water, distance_recycled, distance_scrap, distance_landfill', 'numerical', 'min' => 0),
			
			array('name, city_id', 'safe', 'on' => 'search'),
		);
	}
	
        /**
         * 
         * @return array
         */
	public function attributeLabels()
	{ 
		return array(
			'region_id' => 'Регион',
			'city_id' => 'Город',
			'user_id' => 'Автор',
			'status_id' => 'Статус проекта',
			'number' => 'Номер проекта',
			'name' => 'Название',
			'date_start' => 'Дата начала',
			'date_end' => 'Дата окончания',
			'date_create' => 'Дата создания',
			'address' => 'Адрес',
			'time_mobilization' => 'Начало мобилизации',
			'time_demobilization' => 'Окончание мобилизации',
			'cleaning' => 'Зачистка почасовая или за объем?',
			'payment_climbers' => 'Оплата пром. альпинистов за объем или смену?',
			'backfill_id' => 'Какой материал используем при засыпке?',
			'crusher' => 'Требуется измельчитель или дробилка?',
			'payment_gas_device' => 'Оплата газорезки за тонну или за смену?',
			'disable_sewage' => 'Отключение канализационных сетей',
			'disable_electro' => 'Отключение электросетей',
			'disable_gas' => 'Отключение газопровода',
			'provide_electro' => 'Предоставление временной электросети',
			'provide_water' => 'Предоставление временной подачи воды',
			'distance_recycled' => 'Расстояние до точки реализации вторичных материалов (км)',
			'distance_scrap' => 'Расстояние до точки размещения отходов (км)',
			'distance_landfill' => 'Расстояние до точки захоронения ТБО (км)',
		);
	}
	
        /**
         * 
         * @return \CActiveDataProvider
         */
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->with = array('city', 'status');
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.city_id', $this->city_id);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 'project_id DESC',
                'attributes' => array(
                    'city_id' => array(
						'asc' => 'city.name',
						'desc' => 'city.name DESC',
					),
					'status_id' => array(
						'asc' => 'status.name',
						'desc' => 'status.name DESC',
					),
                    '*',
                ),
            ),
			'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
             ), 
		));
	}
	
	/**
	 * Получить сумму проекта.
	 * @return integer 
	 */
	public function calkSum($load = false) { 
		if($load || is_null($this->sumProject)) {
                    $sum = 0;
		    foreach($this->buildings as $building) {
                           foreach($building->entityBuildings as $entityBuilding) {                                
                               $materials =  $entityBuilding->getMaterialsList();
                               
                               if(is_array($materials)) {
                                $cost = $entityBuilding->count * $entityBuilding->price;   
                                foreach($materials as $material) {
                                   if($material['size'] == 0) {
                                     continue;
                                   }

                                   $productivity = $material['productivity'] * $entityBuilding->working_day;
								   
									$ent_prod = $entityBuilding->count * $productivity;
									if ($ent_prod != 0)
										$sum += ceil( $material['size'] / $ent_prod ) * $cost;
                                   
                                }
                               }
                           } 
                    } 
                    
                    foreach($this->documents as $document) {
                        $sum += $document->price;
                    }
                    
                    foreach($this->emobilizations as $emobilization) {
                        $sum += $emobilization->price * $emobilization->count; 
                    }
                    
                    $this->sumProject = $sum;
		}
		return $this->sumProject;
	}
	
	/**
	 * Данные для графика.
	 * @return array 
	 */
	public function getScheduleData() {
		$links = array();
		$data = array();
                
                foreach($this->buildings as $building) {
                    
                    $countAdd = 0;
                    
                    foreach($building->entityBuildings as $entityBuilding) {    
                        
                       
                        $materials =  $entityBuilding->getMaterialsList();
                        if(is_array($materials)) {
                            $lastDuration = 0;
                            $date_start = null;
                            $date_start_last = $this->date_start;
                            foreach($materials as $material) {
                                if($material['size'] == 0) {
                                    continue;
                                }
                                
                                $id = $entityBuilding->entity_building_id.'_'.$material['material_id'];
                                $text = $entityBuilding->entity->name.' ('.$entityBuilding->count.')=>'.
                                        $material['name'].' ('.$material['size'].')';
                                
                                $productivity = $material['productivity'] * $entityBuilding->working_day;
                                
                                if(is_null($date_start)) {
                                    $date_start = $this->date_start;
                                }
                                
                               /* if(!empty($material['date_start'])) {
                                    $date_start = $material['date_start'];
                                } else */
                                if(!empty($entityBuilding->date_start)){
                                    $date_start = $entityBuilding->date_start;
                                } else {
                                    $date_start_last = date('d-m-Y', (strtotime($date_start_last) + 3600 * 24 * $lastDuration)); 
                                    $date_start = $date_start_last;
                                }
                                                                
                                $lastDuration = ceil( $material['size'] / ($entityBuilding->count * $productivity)  );
                                
                                array_push($data, array(
					'id'=>$id,
					'text'=>$text, 
					'start_date'=>date('d-m-Y', strtotime($date_start)), 
					'duration'=>$lastDuration,
					'order'=>10,
					'progress'=>1,
					'open'=>true, 
                                        'editable'=>1,
					'productivity'=>$productivity,
                                        'parent'=>$building->build_id,
				));
                                $countAdd ++;
                                //add link
                                if($entityBuilding->parent_entity_building_id > 0) {
                                            array_push($links, array( 
                                                            'id'=>sizeof($links) +1,
                                                            'source'=>$entityBuilding->parent_entity_building_id.'_'.$material['material_id'],
                                                            'target'=>$id,
                                                            'type'=>'1',
                                            ));
                                 }
                            }
                        }
                    }
                    
                    
                    
                    if($countAdd > 0) {
                        array_push($data, array(
                                            'id'=>$building->build_id,
                                            'text'=>'Здание: '.$building->name,  
                                            'order'=>10, 
                                            'open'=>true, 
                                            'editable'=>0,
                                            'parent'=>'0',
                        ));
                    }
                }
		
		return array(
			'links'=>$links,
			'data'=>$data,
		);
	}
	
        /**
         * 
         * @return type
         */
	protected function afterFind()
	{
		$this->region_id = !is_null($this->city) ? $this->city->region_id  : 0;
		
		if (isset($this->date_start))
		{
			$date = strtotime($this->date_start);
			$this->date_start = date(Yii::app()->params['phpDateFormat'], $date);
		}
		
		if (isset($this->date_end))
		{
			$date = strtotime($this->date_end);
			$this->date_end = date(Yii::app()->params['phpDateFormat'], $date);
		}
		
		return parent::afterFind();
	}
	
        /**
         * 
         * @return type
         */
	protected function beforeSave()
	{
		if($this->isNewRecord) {
			$this->date_create = date('Y-m-d');
                }
		if (isset($this->date_start))
		{
			$date = strtotime($this->date_start);
			$this->date_start = date('Y-m-d', $date);
		}
		
		if (isset($this->date_end))
		{
			$date = strtotime($this->date_end);
			$this->date_end = date('Y-m-d', $date);
		}
                 
                
		
		return parent::beforeSave();
	}
        
        protected function afterSave() {
            
            
            return parent::afterSave();
        }
 
}

?>