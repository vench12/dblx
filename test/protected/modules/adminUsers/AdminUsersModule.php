<?php

class AdminUsersModule extends CWebModule {
    
	public $name;
	
    public function init(){
        parent::init();
		
		$this->setImport(array(
			'adminUsers.models.*',
			'adminUsers.components.*',
		));
    }
    
     
}

?>
