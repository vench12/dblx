<?php

class UsersController extends  BaseController
{

	// index
	public function actionIndex()
	{
		
		$this->actionUsersView();

	}

	/**
	 * просмотр пользователей. 
	 */
	public function actionUsersView() {
		
		$dataValues = new Users('search');

		if (isset($_GET['Users']))
        	$dataValues->attributes = $_GET['Users'];
		
		$this->render('index', array(
			'dataValues' => $dataValues
		));
	}

	/**
	 * Добавить нового пользователя. 
	 */
	public function actionUserAdd() {
		$model = new Users('add');
		
		if(isset($_POST['Users'])) {
			$model->setAttributes($_POST['Users']);
			if($model->validate() && $model->save()) {
				$this->redirect(array('users/userEdit','id'=>$model->getPrimaryKey()));
			}
		}
		
		$this->render('useradd', array(
			'model'=>$model,
		)); 
	}

	/**
	 * Редактирование пользователя. 
	 */
	public function actionUserEdit($id) {
	
		$model = $this->loadModelByPk('Users', $id);
		$model->SetScenario('edit');
	
		if(isset($_POST['Users'])) {
			$model->setAttributes($_POST['Users']);
			if($model->validate() && $model->save()) {
				$this->redirect(array('users/userEdit','id'=>$model->getPrimaryKey()));
			}
		}
		$this->render('useredit', array(
			'model'=>$model,
		)); 
	}

	/**
	 * Удалить пользователя.
	 * @param integer $id 
	 */
	public function actionUserRemove($id) {
		$model = $this->loadModelByPk('Users', $id); 
		$model->delete();
		$this->redirect(array('users/index'));
	}

	/**
	 * Редактирование ролей пользователя. 
	 */
	public function actionUserRoles($id) {
	
		$model = $this->loadModelByPk('Users', $id);

		$assignments = new AssignmentDataProvider;
		
		$exclude = $assignments->getAssignNamesSpecificUser($id);
		
		$authItemDataProvider = new AuthItemDataProvider($id,$exclude);
		
		$assignmentSelectOptions = AuthItemView::generateAuthItemSelectOptions($authItemDataProvider->fetchData());
		
		if( is_array($assignmentSelectOptions) && sizeof($assignmentSelectOptions) > 0 )
		{
			$assignmentFormModel = new AssignmentForm();
		
			if( isset($_POST['AssignmentForm'])===true )
			{
				$assignmentFormModel->attributes = $_POST['AssignmentForm'];
				
				if( $assignmentFormModel->validate()===true )
				{
					$rights = Rights::getInstance();

					$rights->assign($assignmentFormModel->itemname,$id);
					$this->redirect(array('users/userRoles','id'=>$id));
				}
			}
		}else
		{
			$assignmentFormModel = null;
		}

		$this->render('userroles', array(
			'model'=>$model,
			'assignmentFormModel' =>$assignmentFormModel,
			'assignmentSelectOptions'=>$assignmentSelectOptions,
			'assignmentUser'=>$assignments->getDataSpecificUser($id),
		)); 
	}	

	public function actionUserRoleRemove(){
		
		if( Yii::app()->request->isPostRequest===true )
		{
			$itemName = Yii::app()->request->getParam('name');
			$user_id = Yii::app()->request->getParam('user_id');

			if(empty($user_id))
				{
					$this->redirect(array('users/index'));				
				}

			if(empty($itemName))
				{
					$this->redirect(array('users/userRoles','id'=>$user_id));				
				}

			$model = $this->loadModelByPk('Users', $user_id);

			$authItem = new AuthItemForm;
			$rights = Rights::getInstance();
			
			if($rights->revoke($itemName, $model->user_id))
					$this->redirect(array('users/userRoles','id'=>$user_id));

		}
		
	}

}
?>
