<?php

class RolesController extends BaseController
{

	// index
	public function actionIndex()
	{
		
		$this->actionRolesView();

	}

	/**
	 * просмотр ролей. 
	 */
	public function actionRolesView() {
		
		$dataValues = new AuthItemModel('search');

		if (isset($_GET['AuthItemModel']))
        	$dataValues->attributes = $_GET['AuthItemModel'];
		
		$this->render('index', array(
			'dataValues' => $dataValues,
		));
	}

	/**
	 * Добавить новую роль. 
	 */
	public function actionRoleAdd() {
		
		$model = new AuthItemForm('create');

		if(isset($_POST['AuthItemForm']))
		{
			
			$model->attributes = $_POST['AuthItemForm'];
            	if($model->validate())
					{
						$rights = Rights::getInstance();	
						
						if($rights->createAuthItem($model->name, 2, $model->description, $model->bizrule, $model->data)){

							$this->redirect(array('roles/roleEdit', 'name'=>$model->name));
						}
								else{

									$this->redirect(array('adminUsers/roles/roleAdd',));
								}
						
					}

        }
		
		
		$this->render('roleadd', array(
			'model' => $model
		));
	}

	/**
	 * Редактирование роли. 
	 * @param string $name
	 */
	public function actionRoleEdit($name) {
	
		$name = isset($name)===true ? urldecode($name) : NULL;
		
		$rights = Rights::getInstance();
		$authItem = $rights->select($name);		
		
		if(is_null($authItem))
			$this->redirect(array('roles/index',));	
				
		$model = new AuthItemForm('update');
		
		$model->isNewRecord = false;
		
		if(isset($_POST['AuthItemForm']))
		{
			
			$model->attributes = $_POST['AuthItemForm'];
				
            	if($model->validate())
					{
								
						if($rights->update($name, $model->name, $model->description, $model->bizrule, $model->data))
							$this->redirect(array('roles/roleEdit', 'name'=>$model->name));		

					}

	        }else{
			$model->name = $authItem->name;
			$model->description = $authItem->description;
			$model->bizrule = $authItem->bizrule;
			$model->type = $authItem->type;
			}

		$this->render('roleedit', array(
			'model' => $model
		));
			
	}

	/**
	 * Удалить роль.
	 * @param string $name 
	 */
	public function actionRoleRemove($name) {
		
		$name = isset($name)===true ? urldecode($name) : NULL;
		
		if(is_null($name)){	
			$this->redirect(array('roles/index',));
		}

		$rights = Rights::getInstance();
		
		if($rights->remove($name)){
			$this->redirect(array('roles/index',));
		}
	}
	

}
?>
