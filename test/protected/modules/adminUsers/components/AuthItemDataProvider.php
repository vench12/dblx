<?php
class AuthItemDataProvider extends CDataProvider
{

	public $exclude = array();
	public $items;
	public $type;
	
	public function __construct($id, $exclude = array(), $config=array())
	{

		$this->exclude = !isset($exclude) ? array() : $exclude;
		
		$this->setId($id);
		
		foreach($config as $key=>$value)
			$this->$key = $value;
	}

	public function fetchData()
	{

		if( $this->items===null ){			
			
			$where = isset($this->type) ? ' where type <='.(int)$this->type.'' : '';
			
			$command  =	Yii::app()->db->createCommand('SELECT name,type,description,bizrule,data FROM AuthItem'.$where);
			$this->items = $command->queryAll();
		}

		$data = array();
		
		if(is_array($this->items) && sizeof($this->items)>0)
			foreach( $this->items as $name=>$item )
				if(!in_array($item['name'],$this->exclude))
					$data[] = $item;

		return $data;
	}

	/**
	* Fetches the data item keys from the persistent data storage.
	* @return array list of data item keys.
	*/
	public function fetchKeys()
	{
		$keys = array();
		foreach( $this->getData() as $name=>$item )
			$keys[] = $name;

		return $keys;
	}

	/**
	* Calculates the total number of data items.
	* @return integer the total number of data items.
	*/
	protected function calculateTotalItemCount()
	{
		return count($this->getData());
	}

}