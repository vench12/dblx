<?php
class Rights
{
	
	private $_authManager;
	
	public static $inst = NULL;
	
    public static function getInstance() {
        if(is_null(self::$inst)) {
            self::$inst = new Rights();
        }
        return self::$inst;
    }

	private function Rights(){
			$this->_authManager = Yii::app()->getAuthManager();
		}

	public function createAuthItem($name, $type, $description, $bizRule, $data)
	{
		
		if(!is_null($this->select($name)))
			return NULL;

		$data=array('DateAdd'=>date('Y-m-d H:i:s'),'UserAdd'=>Yii::app()->user->id);
		$bizRule = $bizRule!=='' ? $bizRule : null;
		$name = $name;
		$description = $description!=='' ? $description : null;
		
		return $this->_authManager->createAuthItem($name, $type, $description, $bizRule, $data);
	
	}

	public function select($itemName)
	{
		return $this->_authManager->getAuthItem($itemName);
	}

	public function update($old_name, $name, $description, $bizRule, $data)
	{	

	if( strtolower($old_name)!==strtolower($name) )
		if(!is_null($this->select($name)))
			return false;	
		
		$description = $description!=='' ? $description : null;
		$bizRule = $bizRule!=='' ? $bizRule : null;
		
		$authItem = $this->_authManager->getAuthItem($old_name);

		$direct_parents = $this->_authManager->getItemDirectParents($old_name);
		$direct_children = $this->_authManager->getItemDirectChildren($old_name);
		$assignments = $this->_authManager->getAssignmentsByItemName($old_name);
		
		$this->_authManager->removeAuthItemFromParents($old_name);
		$this->_authManager->removeAuthItemFromChilds($old_name);
		$this->_authManager->removeAuthItemFromAssignments($old_name);


		$data=array('DateAdd'=>$authItem->data['DateAdd'],'UserAdd'=>$authItem->data['UserAdd'],'LastDateEdit'=>date('Y-m-d H:i:s'),'LastUserEdit'=>Yii::app()->user->id);

		$authItem->data = $data;
		$authItem->name = $name;
		$authItem->description = $description!=='' ? $description : null;
		$authItem->bizRule = $bizRule!=='' ? $bizRule : null;
						
		$this->_authManager->saveAuthItem($authItem, $oldName);
		
		foreach($direct_parents as $parent)
				$this->_authManager->addItemChild($parent->name,$authItem->name);
				
		foreach($direct_children as $child)
				$this->_authManager->addItemChild( $authItem->name,$child->name);

		foreach($assignments as $assgn)
				$this->_authManager->assign($name,$assgn->userid);	
		
		return true;
	}
	
	public function remove($itemName)
	{
		$this->_authManager->removeAuthItemFromParents($itemName);
		$this->_authManager->removeAuthItemFromChilds($itemName);
		$this->_authManager->removeAuthItemFromAssignments($itemName);
		
		return $this->_authManager->removeAuthItem($itemName);
	}		

	public function addItemChild($name, $childName){
		
		$this->_authManager->addItemChild($name, $childName);
		
	}

	public function removeItemChild($itemName, $childName){
		
		$this->_authManager->removeItemChild($itemName, $childName);
		
		}

	public function assign($itemname,$userID){
			
			$this->_authManager->assign($itemname,$userID);
			
		}

	public function revoke($itemName, $userId){
			
			return $this->_authManager->revoke($itemName, $userId);
			
		}
	
	public function isAssigned($itemName,$userId){
			
			return $this->_authManager->isAssigned($itemName,$userId);
			
		}
		
	public function revokeAllUserAssignments($userId){
		
			$items =  $this->_authManager->getAuthItems(NULL,$userId);

			foreach($items as $item)
				$this->revoke(cptoutf($item->name), $userId);
		}

	public function getItemRelationsWithParents(){
			return $this->_authManager->getItemRelationsWithParents();
		}

	public function getItemRelationsWithChildren(){
			return $this->_authManager->getItemRelationsWithChildren();
		}
		
	public static function getAuthItemTypeName($type)
	{
		$options = self::getAuthItemOptions();
		if( isset($options[ $type ])===true )
			return $options[ $type ];	
		else
			throw new CException('Ошибка передачи параметра');
	}

	public static function getAuthItemOptions()
	{
		return array(
			CAuthItem::TYPE_OPERATION=>'Операция',
			CAuthItem::TYPE_TASK=>'Задача',
			CAuthItem::TYPE_ROLE=>'Роль',
		);
	}

}
