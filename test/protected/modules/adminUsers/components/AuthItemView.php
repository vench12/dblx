<?php
class AuthItemView  extends CApplicationComponent
{

	public static function generateAuthItemSelectOptions($items)
	{
		$selectOptions = array();

       	foreach( $items as $item )
				$selectOptions[ self::getAuthItemTypeNamePlural($item['type']) ][ $item['name'] ] = $item['name'].': '.$item['description'];


		return $selectOptions;
	}

	public static function getAuthItemTypeNamePlural($type)
	{
		switch( (int)$type )
		{
			case CAuthItem::TYPE_OPERATION: return 'Операции';
			case CAuthItem::TYPE_TASK: return 'Задачи';
			case CAuthItem::TYPE_ROLE: return 'Роли';
			default: throw new CException('Ошибка передачи параметра');
		}
	}

	public function getRemoveAssigmentLink($extra = array())
	{	
		$submit = array_merge(array('users/userRoleRemove'),$extra);

		return CHtml::linkButton('удалить', array(
			'submit'=>$submit,
		));
	}

}
