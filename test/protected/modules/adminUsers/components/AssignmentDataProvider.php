<?php
class AssignmentDataProvider extends CDataProvider
{
	
	public $items;
	
	public function __construct($config=array())
	{
		
		$this->setId('assigment');
		
		foreach($config as $key=>$value)
			$this->$key = $value;
	}

	public function fetchData()
	{
		
		if( $this->items===null ){	
			
			$command  =	Yii::app()->db->createCommand('SELECT AuthAssignment.userid, 
															  AuthAssignment.bizrule, 
															  AuthAssignment.data, 
															  AuthAssignment.itemname as AuthItem_name, 
															  AuthItem.type as AuthItem_type, 
															  AuthItem.description as AuthItem_description, 
															  AuthItem.bizrule AS AuthItem_bizrule, 
															  AuthItem.data AS AuthItem_data FROM 
															  AuthAssignment INNER JOIN AuthItem ON AuthAssignment.itemname = AuthItem.name');
			
			$this->items = $command->queryAll();

		}
		
		$data = array();
		if(is_array($this->items) && sizeof($this->items)>0)
			foreach( $this->items as $item )
				$data[] = $item;

		return $data;
	}

	public function getDataSpecificUser($id)
	{
		$items = $this->getData();
		
		$data = array();
		
		if(!$items)
			return NULL;
		
		foreach( $items as $item )
			if($item['userid'] == $id)
				$data[] = $item;

		return $data;
	}

	public function getAssignNamesSpecificUser($id)
	{
		$items = $this->getData();
		
		$data = array();
		
		if(!$items)
			return NULL;
		
		foreach( $items as $item )
			if($item['userid'] == $id)
				$data[] = $item['AuthItem_name'];

		return $data;
	}
	/**
	* Fetches the data item keys from the persistent data storage.
	* @return array list of data item keys.
	*/
	public function fetchKeys()
	{
		$keys = array();
		foreach( $this->getData() as $name=>$item )
			$keys[] = $name;

		return $keys;
	}

	/**
	* Calculates the total number of data items.
	* @return integer the total number of data items.
	*/
	protected function calculateTotalItemCount()
	{
		return count($this->getData());
	}



}
