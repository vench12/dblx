<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'user_form', 
			'method'=>'post',
            'htmlOptions' => array('class' => 'well well-small'),
));
?>
		
<p><span class="required">*</span> - обязательные для заполнения поля.</p>

		<div>
			<?php echo $form->labelEx($model, 'name'); ?>
			<?php echo $form->textField($model, 'name', array('size'=>'100', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'name'); ?>
		</div>

		<div>
			<?php echo $form->labelEx($model, 'description'); ?>
			<?php echo $form->textField($model, 'description', array('size'=>'100', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'description'); ?>
		</div>
		
		<div>
			<?php echo $form->labelEx($model, 'bizrule'); ?>
			<?php echo $form->textField($model, 'bizrule', array('size'=>'250', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'bizrule'); ?>
		</div>
        	
		<div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('adminUsers/roles/roleRemove', array('name'=>$model->name)), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверенны, что хотите удaлить запись?");'));
			}
			?> 		
        </div>	

<?php $this->endWidget(); ?>
 	