<?php
$this->pageTitle = 'Управление ролями | ' . Yii::app()->name;
$this->breadcrumbs = array( 'Управление ролями'=>array('index'), 'Добавить роль');
?>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
        		    'type' => 'tabs',
           		 	'items' => array(
                					array('label' => 'Управление пользователями', 'icon' => 'user', 'url'=>array('users/index'), 'active' => false),
									array('label' => 'Управление ролями', 'icon' => 'user', 'url'=>array('roles/index'), 'active' => true),
            				),
        	));?>
<h1>Добавить роль</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/role', array(
	'model'=>$model,
));
?>
</div>
</div>