<?php
$this->pageTitle = 'Управление ролями | ' . Yii::app()->name;
$this->breadcrumbs = array('Управление ролями');
?>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
        		    'type' => 'tabs',
           		 	'items' => array(
                					array('label' => 'Управление пользователями', 'icon' => 'user', 'url'=>array('users/index'), 'active' => false),
									array('label' => 'Управление ролями', 'icon' => 'user', 'url'=>array('roles/index'), 'active' => true),
            				),
        	));?>

<h1>Управление ролями</h1>
<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('index'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
	   
		<?= $form->textFieldRow($dataValues, 'name', array('class'=>'input-medium', 'prepend'=>'<i class="icon-search"></i>')); ?>
        
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
		$this->endWidget(); 
		?>
	</div>
    <div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить новую роль',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('roles/roleadd'),
		)); ?>
    </div>
</div> 
<div class="row">
    <div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $dataValues->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'header' => 'Роль',
					'name' => 'name',
					'type' => 'html',
				),
				array(
					'header' => 'Описание',
					'name' => 'description',
					'type' => 'html',
				),
				array(
					'header' => 'Бизнес-правило',
					'name' => 'bizrule',
					'type' => 'html',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{edit}',
					'buttons' => array
					(
						'edit' => array
						(
							'label' => 'Редактировать',
							'url' => 'Yii::app()->createUrl("adminUsers/roles/roleEdit", array("name" => $data["name"]))',
							'options' => array(),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '150px',
					),
				),
			),
		)); ?>
    </div>
</div>
