<?php 
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'user_form', 
			'method'=>'post',
            'htmlOptions' => array('class' => 'well well-small'),
));
?>
		
<p><span class="required">*</span> - обязательные для заполнения поля.</p>

		<div>
			<?php echo $form->checkBoxRow($model, 'active'); ?>	
		</div>

		<div>
        	<?php echo $form->checkBoxRow($model, 'isSuperAdmin'); ?>
		</div>

    	<div>
    		<?php echo $form->labelEx($model,  'login'); ?>
			<?php echo $form->textField($model, 'login',  array('size'=>'64', 'class'=>'span5')); ?>
            <?php echo $form->error($model, 'login'); ?>
    	</div>

    	<div>
    		<?php echo $form->labelEx($model,  'new_password'); ?>
			<?php echo $form->passwordField($model, 'new_password',  array('size'=>'64', 'class'=>'span5')); ?>
            <?php echo $form->error($model, 'new_password'); ?>
    	</div>

    	<div>
    		<?php echo $form->labelEx($model,  'password_confirm'); ?>
			<?php echo $form->passwordField($model, 'password_confirm',  array('size'=>'64', 'class'=>'span5')); ?>
            <?php echo $form->error($model, 'password_confirm'); ?>
    	</div>
    
		<div style="clear:both">
			<?php echo $form->labelEx($model, 'surname'); ?>
			<?php echo $form->textField($model, 'surname', array('size'=>'80', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'surname'); ?>
		</div>

		<div>
			<?php echo $form->labelEx($model, 'name'); ?>
			<?php echo $form->textField($model, 'name', array('size'=>'80', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'name'); ?>
		</div>

		<div>
			<?php echo $form->labelEx($model, 'patronymic'); ?>
			<?php echo $form->textField($model, 'patronymic', array('size'=>'80', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'patronymic'); ?>
		</div>
		
		<div>
			<?php echo $form->labelEx($model, 'email'); ?>
			<?php echo $form->textField($model, 'email', array('size'=>'40', 'class'=>'span5')); ?>
			<?php echo $form->error($model, 'email'); ?>
		</div>
        	
		<div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
			<?php 
			if(!$model->isNewRecord) {
				echo CHtml::link("Удалить", 
							     Yii::app()->createUrl('adminUsers/users/userRemove', array('id'=>$model->getPrimaryKey())), 
						         array('class'=>'btn btn-danger', 'onclick'=>'return confirm("Вы уверенны, что хотите удaлить запись?");'));
			}
			?> 		
        </div>	

<?php $this->endWidget(); ?>
 	