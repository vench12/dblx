<?php
$this->pageTitle = 'Управление пользователями | ' . Yii::app()->name;
$this->breadcrumbs = array( 'Управление пользователями'=>array('index'), 'Добавить пользователя');
?>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
        		    'type' => 'tabs',
           		 	'items' => array(
                					array('label' => 'Управление пользователями', 'icon' => 'user', 'url'=>array('users/index'), 'active' => true),
									array('label' => 'Управление ролями', 'icon' => 'user', 'url'=>array('roles/index'), 'active' => false),
            				),
        	));?>
<h1>Добавить пользователя</h1>

<div class="row">
	<div class="span12">
<?php
$this->renderPartial('forms/user', array(
	'model'=>$model,
));
?>
</div>
</div>