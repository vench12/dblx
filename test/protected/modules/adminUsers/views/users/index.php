<?php
$this->pageTitle = 'Управление пользователями | ' . Yii::app()->name;
$this->breadcrumbs = array('Управление пользователями');
?>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
        		    'type' => 'tabs',
           		 	'items' => array(
                					array('label' => 'Управление пользователями', 'icon' => 'user', 'url'=>array('users/index'), 'active' => true),
									array('label' => 'Управление ролями', 'icon' => 'user', 'url'=>array('roles/index'), 'active' => false),
            				),
        	));?>

<h1>Управление пользователями</h1>
<div class="row">
	<div class="span10">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'filter_form',
			'type' => 'search',
			'method' => 'GET',
			'action' => array('index'),
			'htmlOptions' => array('class' => 'well well-small form-inline'),
		));
		?>
	   
		<?= $form->textFieldRow($dataValues, 'name', array('class'=>'input-medium', 'prepend'=>'<i class="icon-search"></i>')); ?>
        
		<?php
		$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Найти'));
		$this->endWidget(); 
		?>
	</div>
    <div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Добавить пользователя',
			'type' => 'primary',
			'size' => 'null',
			'url' => array('users/useradd'),
		)); ?>
    </div>
</div> 
<div class="row">
    <div class="span12">
		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped bordered condensed',
			'dataProvider' => $dataValues->search(),
			'template' => '{summary} {items} <div class="pagination-centered">{pager}</div>',
			'columns' => array(
				array(
					'name' => 'login',
					'type' => 'html',
				),
				array(
					'name' => 'surname',
					'type' => 'html',
				),
				array(
					'name' => 'name',
					'type' => 'html',
				),
				array(
					'name' => 'patronymic',
					'type' => 'html',
				),
				array(
					'name' => 'email',
					'type' => 'html',
					'value' => 'CHtml::mailto($data->email)',
				),
				array(
					'name' => 'active',
					'type' => 'html',
					'value' => '$data->active == 1 ? \'Да\' : \'Нет\''
				),
				array(
					'name' => 'date_create',
					'type' => 'html',
				),
				array
				(
					'header' => 'Действия',
					'class' => 'CButtonColumn',
					'template' => '{edit} | {roles}',
					'buttons' => array
					(
						'edit' => array
						(
							'label' => 'Редакт. данные',
							'url' => 'Yii::app()->createUrl("adminUsers/users/userEdit", array("id" => $data["user_id"]))',
							'options' => array(),
						),
						'roles' => array
						(
							'label' => 'Редакт. роли',
							'url' => 'Yii::app()->createUrl("adminUsers/users/userRoles", array("id" => $data["user_id"]))',
							'options' => array(),
						),
					),
					'htmlOptions' => array(
						'class' => 'text-center',
						'width' => '220px',
					),
				),
			),
		)); ?>
    </div>
</div>
