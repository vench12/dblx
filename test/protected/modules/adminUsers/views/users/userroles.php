<?php
$this->pageTitle = 'Управление пользователями | ' . Yii::app()->name;
$this->breadcrumbs = array( 'Управление пользователями'=>array('index'), 'Редактирование ролей');
?>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
        		    'type' => 'tabs',
           		 	'items' => array(
                					array('label' => 'Управление пользователями', 'icon' => 'user', 'url'=>array('users/index'), 'active' => true),
									array('label' => 'Управление ролями', 'icon' => 'user', 'url'=>array('roles/index'), 'active' => false),
            				),
        	));?>
<h1>Роли - <?php echo $model->surname.' '.$model->name.' '.$model->patronymic; ?></h1>

<div class="row">
	<div class="span12">
    	<div class="well well-small">
		<b>Текущие роли:</b>
        <?php $this->widget('adminUsers.widgets.AuthAssignmentsWidget', array('dataValues'=>$assignmentUser,
																			  'user_id'=>$model->user_id,
																			  'noItemstext'=>'Нет ролей сопоставленных с текущим пользователем',
																			  'edit'=>true));?>
        </div>
     </div>
</div>
<div class="row">
	<div class="span12">
			<?php if( $assignmentFormModel!==null ): ?>
			
			<?php 
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
																					'id' => 'roles_form', 
																					'method'=>'post',
            																		'htmlOptions' => array('class' => 'well well-small'),
																					));?>

                        <div>                                                           
							<?php echo $form->dropDownList($assignmentFormModel, 'itemname', $assignmentSelectOptions); ?>
							<?php echo $form->error($assignmentFormModel, 'itemname'); ?>
						</div>
                        
                        <div>	
							<button type="submit" class="btn btn-primary">Добавить роль</button>
						</div>	

			<?php $this->endWidget(); ?>

			<?php endif; ?>
    </div>
</div>


<?php
/*
$this->pageTitle = Yii::app()->name . ' - Админ - панель';
$this->breadcrumbs = array(
	'Админ - панель' => array('/admin'),
	'Пользователи' => array('admin/users'),
	'Создать привязку',
);
?>
<?php if(Yii::app()->user->hasFlash('success')) { ?>
    <div class="flash_success">
        <?= Yii::app()->user->getFlash('success'); ?>
    </div>
<?php } ?>
<?php if(Yii::app()->user->hasFlash('error')) { ?>
    <div class="flash_error">
        <?= Yii::app()->user->getFlash('error'); ?>
    </div>
<?php } ?>
<div class="bluepanel margin_bottom_5px">
	Создать привязку
</div> 
<div class="form">
    <div class="colon top">
        <b>ФИО пользователя</b>
    </div>
    <div class="margin_left_10px">
    <?php echo cptoutf($user->_FIO)?>
    </div>
     <div class="colon">
        <b>Должность</b>
    </div>
    <div class="margin_left_10px">
    <?php echo cptoutf($user->_Post)?>
    </div>
     <div class="colon">
        <b>Подразделение</b>
    </div>
     <div class="margin_left_10px">
    <?php echo cptoutf($user->_Department)?>
    </div>
     <div class="colon">
        <b>Привязки (роли, задачи, операции) пользователя:</b>
    </div>
    <div>
        <?php $this->widget('application.components.widgets.AuthAssignmentsWidget', array('dataValues'=>$assignmentUser,'userID'=>$user->UserID,'noItemstext'=>'Нет ролей сопоставленных с текущим пользователем','edit'=>1));?>
    </div>
     <div class="colon">
        <b>Создать привязку</b>
    </div>
			<?php if( $assignmentFormModel!==null ): ?>

						<?php $form=$this->beginWidget('CActiveForm'); ?>
	    				<div class="margin_left_10px">
							<?php echo $form->dropDownList($assignmentFormModel, 'itemname', $assignmentSelectOptions); ?>
                        </div>
                        <div class="warning">
							<?php echo $form->error($assignmentFormModel, 'itemname'); ?>
						</div>
						<div class="colon bottom">
    							<span class="input_button_wrap">
									<?php echo CHtml::submitButton('Создать'); ?>
								</span>
                        </div>

						<?php $this->endWidget(); ?>

			<?php else: ?>

				<div class="colon bottom">
                	<p class="info">
						<?php echo 'Для данного пользователя невозможно создать привязки'; ?>
					</p>
                </div>

			<?php endif; ?>

</div>

<?php
		Yii::app()->clientScript->registerScript(
                       'Reset_form',
                       '$(\'form:first input:reset:first\').click(function () {location.href = \''.Yii::app()->createAbsoluteUrl('/admin/users').'\';});',
                       CClientScript::POS_READY
                    );*/
?>