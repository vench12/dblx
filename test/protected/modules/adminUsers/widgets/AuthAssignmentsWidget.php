<?php

class AuthAssignmentsWidget extends CWidget  {
    
	public $dataValues;
	public $noItemstext;
	public $user_id;
	public $edit = false;

    //put your code here
    public function init(){

        parent::init();
    }
	
    public function run()
    {
		echo CHtml::openTag('div',array('class'=>'roles-list-view'));
				
		if(!is_null($this->dataValues))
			{
				
				$content = '';
				foreach($this->dataValues as $k => $val){
						
					$removeLink = array('name'=>$val['AuthItem_name'],'user_id'=>$this->user_id);
					
					$link = Yii::app()->user->isSuperAdmin() ? Yii::app()->createUrl("adminUsers/roles/roleEdit", array("name" => $val['AuthItem_name'])) : NULL;
					
					$content .= '<li>'
							 .CHtml::tag('i',array('class'=>'icon-user'),'&nbsp;',true)
							 .CHtml::tag('span',array('class'=>'offset-left-10px'),Chtml::link($val['AuthItem_name'],$link,array()),true)
							 .Chtml::tag('span',array('class'=>'offset-left-10px'),'<b>Описание: </b>'.(!empty($val['AuthItem_description']) ? $val['AuthItem_description'] : 'нет'),true)
							 .($this->edit ? CHtml::tag('span',array('class'=>'offset-left-10px'),AuthItemView::getRemoveAssigmentLink($removeLink),true) : '')
							 .'</li>';
					}
					
					echo Chtml::tag('ul',array(),$content,true);
						
				}else{
					 echo CHtml::tag('span',array('class'=>'roles-info'),$this->noItemstext,true);
					}
					
		echo CHtml::closeTag('div');
    }
}

?>
