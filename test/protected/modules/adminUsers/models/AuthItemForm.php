<?php
//
class AuthItemForm extends CFormModel
{

	public $name;
	public $description;
	public $type;
	public $bizrule;
	public $data;
	public $isNewRecord = true;

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'nameIsAvailable', 'on'=>'create'),
			array('name', 'newNameIsAvailable', 'on'=>'update'),
			array('description', 'required'),
			array('bizrule', 'length', 'min'=>0),
			array('data', 'length', 'min'=>0),
			array('isNewRecord','boolean')
		);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Наименование',
			'description' => 'Описание',
			'bizrule' => 'Бизнес-правило',
		);
	}

	public function nameIsAvailable($attribute, $params)
	{
		
		if(!is_null(Rights::getInstance()->select($this->name)))
			$this->addError('name','Роль, задача или операция с таким именем уже существует!');
	}

	public function newNameIsAvailable($attribute, $params)
	{
		if( strtolower(urldecode($_GET['name']))!==strtolower($this->name) )
			$this->nameIsAvailable($attribute, $params);
	}
}
?>