<?php

// 
class AuthItemModel extends CActiveRecord
{

	/**
	 * Количество записей в многострочной выборке.
	 * @var integer 
	 */
	public $pageSize = NULL;
	
	public static function model($className = __CLASS__)
	{
        return parent::model($className);
    }

    public function tableName()
	{
        return 'AuthItem';
    }

     public function attributeLabels() {
		return array(
             'name'=>'Роль',
			 'description'=>'Описание',
			 'bizrule'=>'Бизнес-правило',
        );
    }
	
	public function rules()
	{
		return array(
			array('description, name, bizrule', 'safe', 'on' => 'search'),
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('name', $this->name, true);
		
		$criteria_1 = new CDbCriteria();
		$criteria_1->compare('description', $this->name, true);

		$criteria_2 = new CDbCriteria();
		$criteria_2->compare('bizrule', $this->name, true);
		
		$criteria->mergeWith($criteria_1, 'OR');
		$criteria->mergeWith($criteria_2, 'OR');

		$this->pageSize = !is_null($this->pageSize) ? $this->pageSize : Yii::app()->params['postsPerPage'];
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => $this->pageSize,
			),
			'sort' => array(
				'defaultOrder' => 'name desc',
			),
		));		

	}

}

?>
