<?php

class UserIdentity extends CUserIdentity
{
	protected $_id;
	
	public function authenticate()
	{
		$users = array(
			// username => password
			'admin' => 'admin',
        );
		
        if (!isset($users[$this->username]))
        	$this->errorCode = self::ERROR_USERNAME_INVALID;
        elseif ($users[$this->username] !== $this->password)
        	$this->errorCode = self::ERROR_PASSWORD_INVALID;
        else
		{
			$this->_id = 1;
        	$this->errorCode = self::ERROR_NONE;
		}
        return !$this->errorCode;
	}
    
	public function getId()
	{
            return $this->_id;
        }
}
?>