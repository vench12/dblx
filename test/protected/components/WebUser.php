<?php

class WebUser extends CWebUser
{
	
   private $_model = null;
   
   protected function afterLogin(){
		
		$this->setState('isSuperAdmin',$this->_isSuperAdmin(),false);
		
		}

   private function _isSuperAdmin()
	{
        if($user = $this->getModel())
            return $user->isSuperAdmin ? true : false;
		else
			return false;
    }
	 
	private function getModel()
	{
        if (!$this->isGuest && $this->_model === null)
		{
            $this->_model = Users::model()->findByPk($this->id);
        }
        return $this->_model;
    }
	
	public function isSuperAdmin(){
		
		return $this->getState('isSuperAdmin',false);
		
		}
}

?>
