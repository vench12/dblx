<?php

class TreatyParties
{
	private static $inst = NULL;
	
	public static function getInstance()
	{
        if(is_null(self::$inst))
            self::$inst = new TreatyParties();
        return self::$inst;
    }
	
	public function getTreatyParties()
	{
		return array(
			1 => 'Заказчик',
			2 => 'Исполнитель',
		);
	}
}

?>