<?php

class PaymentType
{
	private static $inst = NULL;
	
	public static function getInstance()
	{
        if(is_null(self::$inst))
            self::$inst = new PaymentType();
        return self::$inst;
    }
	
	public function getPaymentTypes()
	{
		return array(
			1 => 'Почасовая',
			2 => 'За объем',
			3 => 'За смену',
		);
	}
}

?>