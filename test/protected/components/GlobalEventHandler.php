<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlobalEventHandler
 *
 * @author vench
 */
class GlobalEventHandler extends CBehavior { 
    
    /**
     * 
     */
    const PROJECT_UPDATE_SUM = 'onProjectUpdateSum';
    
    /**
     *
     * @var GlobalEventHandler 
     */
    private static $inst = NULL;
    
    private function __construct() {
        //raiseEvent
        $this->attachEventHandler(self::PROJECT_UPDATE_SUM, array($this, 'onProjectUpdateSum')); 
    }
    
    /**
     * 
     * @return GlobalEventHandler
     */
    public static function getInstance() {
        if(is_null(self::$inst)) {
            self::$inst = new GlobalEventHandler();
        }
        return self::$inst;
    }
    
    /**
     * Запуск события
     * @param string $eventName
     * @param mixed $params
     * @param mixed $sandler
     */
    public static function trigger($eventName, $params, $sandler = NULL) {
        $event = new CEvent($sandler, $params);
        self::getInstance()->raiseEvent($eventName, $event);
    }
    
    /**
     * Обновить сумму проекта
     * @param CEvent $event
     */
    protected function onProjectUpdateSum(CEvent $event) {
        if($event->params instanceof Project) {
            $event->params->total_sum = $event->params->calkSum();
            $event->params->save();
        } else if(is_array($event->params) && isset($event->params['id'])) {
            $model = Project::model()->findByPk($event->params['id']);
            if(!is_null($model)) {
                $model->total_sum = $model->calkSum();
                $model->save();
            }
        }        
    }
}
