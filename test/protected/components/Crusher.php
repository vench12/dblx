<?php

class Crusher
{
	private static $inst = NULL;
	
	public static function getInstance()
	{
        if(is_null(self::$inst))
            self::$inst = new Crusher();
        return self::$inst;
    }
	
	public function getCrusherTypes()
	{
		return array(
			1 => 'Измельчитель',
			2 => 'Дробилка',
		);
	}
}

?>