<?php
 

/**
 * Класс вернет список городов. 
 *
 * @author v.raskin
 */
class CitiesList extends CAction {
	
	/**
	 *
	 * @param string $value название города
	 */
	public function run($value) {
		$data = Cities::model()->findAll('name LIKE :name', array(
				':name'=>"%$value%", 
		));   
		$сities = array();
		if(is_array($data) && sizeof($data) > 0) {
			foreach($data as $item){
				array_push($сities, $item->name);
			}
		}
		echo json_encode( $сities ); 
		Yii::app()->end();
	}
}

?>
