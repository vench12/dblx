<?php
 

/**
 * Класс вернет список городов упакованных в options. 
 *
 * @author v.raskin
 */
class CitiesOptions extends CAction {
	
    /**
	 *
	 * @param int $region_id ИД региона
	 */
	public function run($region_id) {
		$listData = CHtml::listData(Cities::model()->findAll('region_id = :region_id', array(
				':region_id'=>$region_id, 
		)), 'city_id', 'name'); 
		$htmlOptions = array('empty'=>'--Выбор города--');
		echo CHtml::listOptions(NULL
				, $listData, $htmlOptions);
		Yii::app()->end(0);
	}
}

?>
