<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Набор утилит.
 *
 * @author v.raskin
 */
class Utilites {
	//put your code here
	
	private function __construct() { }
	
	/**
	 * Позволяет преобразовать колекцию CActiveRecord в простой масси.
	 * @param /CActiveRecord $objectsAR коллекция классов CActiveRecord.
	 * @param array $fields поля которые нужно положить в масси.
	 * @return array простой массив с полями из коллекции.
	 */
	public static function convertObjectARToSimpleArray($objectsAR, $fields) {
		$data = array();
		foreach($objectsAR as $ar) {
			$row = array();
			foreach($fields as $field){
				$row[$field] = $ar->{$field};
			}
			array_push($data, $row);
		}
		return $data;
	}
	
	/**
	 *
	 * @param integer $price цена
	 * @param string $format формат цены
	 * @return string 
	 */
	public static function priceFormat($price, $format = NULL) {
		if(is_null($format)) {
			$format = Yii::app()->params['priceFormat'];
		}
		return Yii::app()->numberFormatter->format($format, $price);
	}
        
        /**
         * Фиксит элмент редактирования bdatepicker
         * @staticvar boolean $use
         * @return void
         */
        public static function bdatepickerFix() {
            static $use = false;
            if($use) {
                return;
            }
            Yii::app()->clientScript->registerScript('install-ru-date-picker', ' 
                $.fn.bdatepicker.dates[\'ru\'] = {
                        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
                        daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
                        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
                        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                        monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                        today: "Сегодня",
                        weekStart: 1
                }; 
            ',CClientScript::POS_HEAD);
            $use = TRUE;
        }
}

?>
