<?php

class PaymentTypeGasDevice
{
	private static $inst = NULL;
	
	public static function getInstance()
	{
        if(is_null(self::$inst))
            self::$inst = new PaymentTypeGasDevice();
        return self::$inst;
    }
	
	public function getPaymentTypes()
	{
		return array(
			1 => 'За тонну',
			2 => 'За смену',
		);
	}
}

?>