<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SettingsController
 *
 * @author vench
 */
class SettingsController  extends BaseController {
    
    /**
     * 
     */
    public function actionIndex() {
        $model = new Settings('search');
        $this->render('index', array(
            'model'=>$model,
        ));
    }
    
    /**
     * 
     */
    public function actionUpdateValue() {
        $pk = Yii::app()->request->getParam('pk');
	$name = Yii::app()->request->getParam('name');
	$value = Yii::app()->request->getParam('value');
	$model = $this->loadModelByPk('Settings', $pk);
	$model->{$name} = $value;
	echo (int)$model->save();
	Yii::app()->end();
    }
}
