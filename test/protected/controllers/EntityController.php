<?php

class EntityController extends BaseController
{
	public function actionIndex()
	{ 
		$model = new Entity('search');
		
		if (isset($_GET['Entity']))
			$model->setAttributes($_GET['Entity']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionEntityAdd()
	{
		$model = new Entity();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/entity/EntityEdit', array('id' => $model->getPrimaryKey())));
		
		$this->render('entityadd', array(
			'model' => $model,
		));
	}
	
	public function actionEntityEdit($id)
	{
		$model = $this->loadModelByPk('Entity', $id);
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('entityedit', array(
			'model' => $model,
		));
	}
	
	public function actionEntityRemove($id)
	{
		$model = $this->loadModelByPk('Entity', $id);
		$model->delete();
		$this->redirect($this->createUrl('/entity'));
	}
	
	
	
	public function actionUnits()
	{ 
		$model = new Units('search');
		
		if (isset($_GET['Units']))
			$model->setAttributes($_GET['Units']);
		
		$this->render('units', array(
			'model' => $model,
		));
	}
	
	public function actionUnitAdd()
	{
		$model = new Units();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/entity/UnitEdit', array('id' => $model->getPrimaryKey())));
		
		$this->render('unitadd', array(
			'model' => $model,
		));
	}
	
	public function actionUnitEdit($id)
	{
		$model = $this->loadModelByPk('Units', $id);
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('unitedit', array(
			'model' => $model,
		));
	}
	
	public function actionUnitRemove($id)
	{
		$model = $this->loadModelByPk('Units', $id);
		$model->delete();
		$this->redirect($this->createUrl('/entity/units'));
	}
	
	
	
	public function actionTypes()
	{ 
		$model = new EntityType('search');
		
		if (isset($_GET['EntityType']))
			$model->setAttributes($_GET['EntityType']);
		
		$this->render('types', array(
			'model' => $model,
		));
	}
	
	public function actionTypeAdd()
	{
		$model = new EntityType();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/entity/TypeEdit', array('id' => $model->getPrimaryKey())));
		
		$this->render('typeadd', array(
			'model' => $model,
		));
	}
	
	public function actionTypeEdit($id)
	{
		$model = $this->loadModelByPk('EntityType', $id);
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('typeedit', array(
			'model' => $model,
		));
	}
	
	public function actionTypeRemove($id)
	{
		$model = $this->loadModelByPk('EntityType', $id);
		$model->delete();
		$this->redirect($this->createUrl('/entity/types'));
	}
	
	
	
	public function actionMaterials()
	{ 
		$model = new Materials('search');
		
		if (isset($_GET['Materials']))
			$model->setAttributes($_GET['Materials']);
		
		$this->render('materials', array(
			'model' => $model,
		));
	}
	
	public function actionMaterialAdd()
	{
		$model = new Materials();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/entity/MaterialEdit', array('id' => $model->getPrimaryKey())));
		
		$this->render('materialadd', array(
			'model' => $model,
		));
	}
	
	public function actionMaterialEdit($id)
	{
		$model = $this->loadModelByPk('Materials', $id);
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('materialedit', array(
			'model' => $model,
		));
	}
	
	public function actionMaterialRemove($id)
	{
		$model = $this->loadModelByPk('Materials', $id);
		$model->delete();
		$this->redirect($this->createUrl('/entity/materials'));
	}
	
	
	
	public function actionProductivity()
	{
		$id = Yii::app()->request->getParam('Entity');
		$id = intval($id['entity_id']);
		
		$model = new EntityProductivity('search');
		$modelEntity = $this->loadModelByPk('Entity', $id);
		
		if (!is_null($modelEntity))
		{
			if (isset($_GET['EntityProductivity']))
				$model->setAttributes($_GET['EntityProductivity']);
			
			$this->render('productivity', array(
				'model' => $model,
				'modelEntity' => $modelEntity,
			));
		}
	}
	
	public function actionProductivityAdd($id)
	{
		$model = new EntityProductivity();
		$modelEntity = $this->loadModelByPk('Entity', $id);
		
		if (!is_null($modelEntity))
		{
			$model->setAttribute('entity_id', $id);
			if ($this->validateAndSaveModel($model))
				$this->redirect($this->createUrl('/entity/ProductivityEdit', array('id' => $model->getPrimaryKey())));
			
			$this->render('productivityadd', array(
				'model' => $model,
				'modelEntity' => $modelEntity,
			));
		}
	}
	
	public function actionProductivityEdit()
	{
		$arr_id = Yii::app()->request->getParam('id');
		$model = $this->loadModelByPk('EntityProductivity', $arr_id);
		$modelEntity = $this->loadModelByPk('Entity', $arr_id['entity_id']);
		
		if (!is_null($modelEntity))
		{
			if ($this->validateAndSaveModel($model))
				$this->refresh();
			
			$this->render('productivityedit', array(
				'model' => $model,
				'modelEntity' => $modelEntity,
			));
		}
	}
	
	public function actionProductivityRemove()
	{
		$arr_id = Yii::app()->request->getParam('id');
		$model = $this->loadModelByPk('EntityProductivity', $arr_id);
		$model->delete();
		$this->redirect($this->createUrl('/entity/productivity', array('Entity[entity_id]' => $arr_id['entity_id'])));
	}
}
?>