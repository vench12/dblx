<?php

class QuestionController extends BaseController
{
	public function actionIndex()
	{ 
		$model = new Question('search');
		
		if (isset($_GET['Question']))
			$model->setAttributes($_GET['Question']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionQuestionAdd()
	{
		$model = new Question();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/question/questionedit', array('id' => $model->getPrimaryKey())));
		
		$this->render('questionadd', array(
			'model' => $model,
		));
	}
	
	public function actionQuestionEdit($id)
	{
		$model = $this->loadModelByPk('Question', $id); 
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('questionedit', array(
			'model' => $model,
		));
	}
	
	public function actionQuestionRemove($id)
	{
		$model = $this->loadModelByPk('Question', $id); 
		$model->delete();
		$this->redirect($this->createUrl('/question'));
	}
}
?>