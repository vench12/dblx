<?php

class QuestionsDocsController extends BaseController
{
	public function actionIndex()
	{ 
		$model = new QuestionsDocs('search');
		
		if (isset($_GET['QuestionsDocs']))
			$model->setAttributes($_GET['QuestionsDocs']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionQuestionsDocAdd()
	{
		$model = new QuestionsDocs();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/questionsdocs/questionsdocedit', array('id' => $model->getPrimaryKey())));
		
		$this->render('questionsdocadd', array(
			'model' => $model,
		));
	}
	
	public function actionQuestionsDocEdit()
	{
		$id = Yii::app()->request->getParam('id');
		
		$model = $this->loadModelByPk('QuestionsDocs', $id);
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('questionsdocedit', array(
			'model' => $model,
		));
	}
	
	public function actionQuestionsDocRemove()
	{
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadModelByPk('QuestionsDocs', $id);
		$model->delete();
		$this->redirect($this->createUrl('/questionsdocs'));
	}
}
?>