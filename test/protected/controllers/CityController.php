<?php

class CityController extends BaseController
{
	public function actions()
	{
        return array(
            'сitiesList' => 'application.components.actions.CitiesList', 
			'citiesOptions' => 'application.components.actions.CitiesOptions', 
        );
    }
	
	public function actionIndex()
	{ 
		$model = new Cities('search');
		
		if (isset($_GET['Cities']))
			$model->setAttributes($_GET['Cities']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionCityAdd()
	{
		$model = new Cities();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/city/cityedit', array('id' => $model->getPrimaryKey())));
		
		$this->render('cityadd', array(
			'model' => $model,
		));
	}
	
	public function actionCityEdit($id)
	{
		$model = $this->loadModelByPk('Cities', $id); 
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('cityedit', array(
			'model' => $model,
		));
	}
	
	public function actionCityRemove($id)
	{
		$model = $this->loadModelByPk('Cities', $id); 
		$model->delete();
		$this->redirect($this->createUrl('/city'));
	}
}
?>