<?php
 

/**
 * Контроллер управления справочником шаблонов.
 *
 * @author v.raskin
 */
class TemplateController extends BaseController
{
	
	/**
	 * Редактировать объект шаблона. 
	 */
	public function actionTemplateItemEdit() {
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadModelByPk('TemplateItem', $id);
		
		if($this->validateAndSaveModel($model)) {
			$this->refresh();
		}
		
		$this->render('templateItemEdit', array(
			'model'=>$model, 
		));
	}
	
	/**
	 * Добавить объект к шаблону.
	 * @param integer $id 
	 */
	public function actionTemplateItemAdd($id) {
		$model = $this->loadModelByPk('Template', $id);
		
		$templateItem = new TemplateItem();
		$templateItem->template_id = $model->getPrimaryKey();
		if($this->validateAndSaveModel($templateItem)) {
			$this->redirect($this->createUrl('/template/templateItemEdit', array('id'=>$templateItem->getPrimaryKey())));
		}
		
		$this->render('templateItemAdd', array(
			'model'=>$model,
			'templateItem'=>$templateItem,
		));
	}
	
	/**
	 * Удаление объекта шаблона. 
	 */
	public function actionTemplateItemRemove() {
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadModelByPk('TemplateItem', $id);
		$model->delete();
		$this->redirect($this->createUrl('/template/templateEdit', array('id'=>$model->template_id, 'p'=>2)));
	}
	
	/**
	 * Добавление нового шаблона 
	 */
	public function actionTemplateadd() {
		$model = new Template();
		
		if($this->validateAndSaveModel($model)) {
			$this->redirect($this->createUrl('/template/templateEdit', array('id'=>$model->getPrimaryKey())));
		}		 
		
		$this->render('templateadd', array(
			'model'=>$model,
		)); 
	}
	
	/**
	 * Редактирование шаблона.
	 * @param integer $id 
	 */
	public function actionTemplateEdit($id) {
		$model = $this->loadModelByPk('Template', $id);  
		 
		if($this->validateAndSaveModel($model)) {
			 $this->redirect($this->createUrl('/template/templateEdit', array('id'=>$model->getPrimaryKey())));
		}		 
		
		$templateItem = new TemplateItem('search');
		$templateItem->template_id = $model->getPrimaryKey();
		
		
		$this->render('templateEdit', array(
			'model'=>$model, 
			'templateItem'=>$templateItem,
		));
	}
	
	/**
	 * Удалить шаблон
	 * @param integer $id 
	 */
	public function actionTemplateRemove($id) {
		$model = $this->loadModelByPk('Template', $id);
		$model->delete();
		$this->redirect($this->createUrl('/template'));
	}
	
	/**
	 * Выводим все шаблоны.
	 */
	public function actionIndex() {
		$model = new Template('search');
		
		if(isset($_REQUEST['Template'])) {
			$model->setAttributes($_REQUEST['Template']); 
		}
		$dataProvider = $model->buildDataProvider();
		
		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
		));
	}
}

?>
