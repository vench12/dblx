<?php

class RegionController extends BaseController
{
	public function actions()
	{
        return array(
            'сitiesList' => 'application.components.actions.CitiesList', 
			'citiesOptions' => 'application.components.actions.CitiesOptions', 
        );
    }
	
	public function actionIndex()
	{ 
		$model = new Regions('search');
		
		if (isset($_GET['Regions']))
			$model->setAttributes($_GET['Regions']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionRegionAdd()
	{
		$model = new Regions();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/region/regionedit', array('id' => $model->getPrimaryKey())));
		
		$this->render('regionadd', array(
			'model' => $model,
		));
	}
	
	public function actionRegionEdit($id)
	{
		$model = $this->loadModelByPk('Regions', $id); 
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('regionedit', array(
			'model' => $model,
		));
	}
	
	public function actionRegionRemove($id)
	{
		$model = $this->loadModelByPk('Regions', $id); 
		$model->delete();
		$this->redirect($this->createUrl('/region'));
	}
}
?>