<?php

class ProjectController extends BaseController
{
	public function actions() {
        return array(
            'сitiesList' => 'application.components.actions.CitiesList', 
			'citiesOptions' => 'application.components.actions.CitiesOptions', 
        );
    }
	
	public function actionIndex()
	{
		$model = new Project('search');
		
		if (isset($_GET['Project']))
			$model->setAttributes($_GET['Project']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionProjectAdd()
	{
		$model = new Project();
		$model->user_id = Yii::app()->user->id;
		
		$modelProjectQuestion = new ProjectQuestion();
		$questions = $modelProjectQuestion->getQuestions();
		$params = Yii::app()->request->getParam('questions');
		
		$document = new Document();
		
		if ($this->validateAndSaveModel($model))
		{
                        if(is_array($params) && sizeof($params) > 0) {
                            foreach ($params as $param) {
                                    $project_id = $model->getPrimaryKey();
                                    $modelProjectQuestion->add($project_id, $param, 1);

                                    $questsDocs = QuestionsDocs::model()->getQuestsDocs($param);
                                    //$questsDocs_ = Utilites::convertObjectARToSimpleArray($questsDocs, array('question_id', 'doc_type_id'));
                                    foreach ($questsDocs as $questsDoc)
                                            $document->add($project_id, $questsDoc->doc_type_id);
                            }
                        }
                        
                        
			$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $model->getPrimaryKey(), '#' => 'project')));
		}
		
		$this->render('projectadd', array(
			'model' => $model,
			'questions' => $questions
		));
	}
	
	/**
	 *
	 * @param type $id 
	 */
	public function actionBdr($id) {
		$model = $this->loadModelByPk('Project', $id); 
		$this->render('bdr', array(
			'model' => $model,
	    ));	
	}
	
	/**
	 *
	 * @param type $id 
	 */
	public function actionProjectEdit($id)
	{
		$model = $this->loadModelByPk('Project', $id); 		 
		$model->user_id = Yii::app()->user->id;
		$model->date_create = date('Y-m-d');
		
		$modelProjectQuestion = new ProjectQuestion();
		$questions = $modelProjectQuestion->getQuestions();
		$prjQuests = $modelProjectQuestion->getPrjQuests($id);
		$params = Yii::app()->request->getParam('questions', array());
		
		if ($this->validateAndSaveModel($model))
		{
			$modelProjectQuestion->remove($model->getPrimaryKey());
			foreach ($params as $param) {
				$modelProjectQuestion->add($model->getPrimaryKey(), $param, 1);
                        }	
                        GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, $model);
                        
			$this->refresh();
		}
		
		$buildings = Building::model()->findAll('project_id=:project_id', array(
			':project_id'=>$model->getPrimaryKey(),
		));
		
		$documents = new Document('search');
                
                $eMobilization = new EntityBMobilization('search');
                $eMobilization->project_id = $model->getPrimaryKey();
		
		$this->render('projectedit', array(
			'model' => $model,
			'buildings' => $buildings,
			'documents' => $documents,
			'questions' => $questions,
			'prjQuests' => $prjQuests,
                        'eMobilization'=>$eMobilization,
		));
	}
	
	/**
	 * Добавить здание к проекту.
	 * @param integer $id ИД проекта.
	 */
	public function actionBuildingAdd($id) {
		$model = $this->loadModelByPk('Project', $id); 
		$building = new Building();
		$building->project_id = $model->getPrimaryKey();
		if($this->validateAndSaveModel($building)) {
                    GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, $model);
		    $this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $model->getPrimaryKey(), '#' => 'buildings')));
		}
		
		$this->render('buildingAdd', array(
			'model' => $model,
			'building'=>$building,
		));
	}
	
	/**
	 * Обновить информацию по зданию.
	 * @param integer $id ИД здания.
	 */
	public function actionBuildingUpdate($id) {
		$model = $this->loadModelByPk('Building', $id); 
		$templateIdOld = $model->template_id;
		$this->validateAndSaveModel($model); 
		$model->tryChangeTemplate($templateIdOld); 
                GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, array('id' => $model->project_id));
		$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $model->project_id, '#' => 'buildings')));
	}
	
	/**
	 * Удаляем здание у проекта.
	 * @param integer $id ИД здания.
	 */
	public function actionbuildingRemove($pid) {
		$model = $this->loadModelByPk('Building', $pid);
		$model->delete();
                GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, array('id' => $model->project_id));
		$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $model->project_id, '#' => 'buildings')));
	}
	
	/**
	 * Запрос вернет список доступных шаблонов(методов сноса)
	 */
	public function actionGetTemplates() {
	    $params = Yii::app()->request->getParam('Building');  
		$this->ensure(!(!isset($params['type_build_id']) || !isset($params['type_build_place_id']) || !isset($params['height'])), "Ошибка передачи данных...");
 
		$data =Template::getTemplates($params['type_build_id'],$params['type_build_place_id'],$params['height']);
		$htmlOptions = array('empty'=>'--Выбор метода сноса--',);
		echo CHtml::listOptions(isset($params['template_id']) ? $params['template_id'] : NULL, CHTml::listData($data, 'template_id', 'name'), $htmlOptions);
		Yii::app()->end();
	}
	
	/**
	 * Добавить материал к зданию
	 * @param integer $id Ид здания.
	 */
	public function actionMaterialAdd($id) {
		$building = $this->loadModelByPk('Building', $id);  
		$model = new  BuildingMaterials();
		$model->build_id = $building->build_id;
		if($this->validateAndSaveModel($model)) {
			$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $building->project_id, 'tab' => 'Материалы', '#' => 'buildings')));
		}
		$this->render('materialAdd', array(
			'model' => $model,
			'building'=>$building,
		));
	}
	
	/**
	 * Редактирование материала. 
	 */
	public function actionMaterialEdit() {
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadModelByPk('BuildingMaterials', $id);
		if($this->validateAndSaveModel($model)) {
			$this->refresh();
		}
		$this->render('materialEdit', array(			 
			'model'=>$model,
			'building'=>$model->building,
		));
	}
	
	/**
	 * Удалить материал 
	 */
	public function actionMaterialRemove() {
		$id = Yii::app()->request->getParam('pid');
		$model = $this->loadModelByPk('BuildingMaterials', $id);
		$project_id = $model->building->project_id;
		$model->delete();
		$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $project_id, 'tab' => 'Материалы', '#' => 'buildings')));
	}
	
	/**
	 * Добавить объект к зданию.
	 * @param integer $id Ид здания.
	 */
	public function actionEntityAdd($id) {
		$building = $this->loadModelByPk('Building', $id);
		$modelProject = $building->project;
		$model = new EntityBuilding();
		$model->prices_id = 0;
		$model->build_id = $building->getPrimaryKey();
		if($this->validateAndSaveModel($model)) {
                        GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM,$modelProject);
			$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $building->project_id, 'tab' => 'Объекты', '#' => 'buildings')));
		}
		$this->render('entityAdd', array(			 
			'model'=>$model,
			'building'=>$building,
		));
	}
	
	/**
	 * Редактируем объект.
	 * @param integer $id ID EntityBuilding
	 */
	public function actionEntityEdit($id) {
		$model = $this->loadModelByPk('EntityBuilding', $id);
		$modelProject = $model->building->project;
		$modelBuilding = $model->building;
		
		$providerName = '';
		if (intval($model->prices_id) > 0)
		{
			$pricesEntity = PricesEntity::model()->find( 'prices_id=:prices_id', array(':prices_id' => intval($model->prices_id)) );
			$provider_id = (is_object($pricesEntity)) ? $pricesEntity->provider_id : 0;
			$provider = Provider::model()->find( 'provider_id=:provider_id', array(':provider_id' => intval($provider_id)) );
			$providerName = (is_object($provider)) ? $provider->name : '';
		}
		
		if($this->validateAndSaveModel($model)) {
                        GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM,$modelProject);
			$this->refresh();
		}
		$this->render('entityEdit', array(			 
			'model'=>$model,
			'building'=>$modelBuilding,
			'providerName' => $providerName,
		));
	}
	
	/**
	 * Удаляем объект.
	 * @param integer $id ID EntityBuilding
	 */
	public function actionEntityRemove($id) {
		$model = $this->loadModelByPk('EntityBuilding', $id);
		$project_id = $model->building->project_id;
		$model->delete();
                GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, array('id' => $project_id));
		$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $project_id, 'tab' => 'Объекты', '#' => 'buildings')));
	}
	
	/**
	 * Обновить поле в  EntityBuilding.
	 */
	public function actionUpdateEntity() {
		$pk = Yii::app()->request->getParam('pk');
		$name = Yii::app()->request->getParam('name');
		$value = Yii::app()->request->getParam('value');
		$model = $this->loadModelByPk('EntityBuilding', $pk);
		$model->{$name} = $value;
		$model->save();
                
                GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, array('id' => $model->building->project_id));
                 
		Yii::app()->end();
	}
	
	/**
	 * Обновить поле в  BuildingMaterials.
	 */
	public function actionUpdateMaterial() {
		$pk = Yii::app()->request->getParam('pk');
		$name = Yii::app()->request->getParam('name');
		$value = Yii::app()->request->getParam('value');
		$model = $this->loadModelByPk('BuildingMaterials', $pk);
		$model->{$name} = $value;
		$model->save();
                
		Yii::app()->end();
	}
	
	/**
	 * График проекта
	 * @param integer $id ID проекта
	 */
	public function actionSchedule($id) {
		$model = $this->loadModelByPk('Project', $id);
		$this->render('schedule', array(
			'model'=>$model,
		));
	}
	
	/**
	 * Добавлет новый объект к  EntityBuilding, указывает объем от общего количества материала.
	 * @param integer $build_id
	 * @param integer $material_id 
	 */
	public function actionEntityBuildingAdd($entity_building_id, $material_id) {
		$model = new EntityBuildingAdd();
		$model->entity_building_id = $entity_building_id;
		$model->material_id = $material_id;
		$model->size = 0;
		echo (int)$model->save(false);
		Yii::app()->end();
	}
	
	/**
	 * Удалить EntityBuildingAdd.
	 * @param integer $id   ИД EntityBuildingAdd.
	 */
	public function actionEntityBuildingRemove($id) {
		$model = $this->loadModelByPk('EntityBuildingAdd', $id); 
		$model->delete();
		$this->redirect($this->createUrl('/project/entityEdit', array('id'=>$model->entity_building_id)));
	}
	
	/**
	 * Обновить поле в  EntityBuildingAdd.
	 */
	public function actionUpdateEntityBuildingAdd() {
		$pk = Yii::app()->request->getParam('pk');
		$name = Yii::app()->request->getParam('name');
		$value = Yii::app()->request->getParam('value');
		$model = $this->loadModelByPk('EntityBuildingAdd', $pk);
		$model->{$name} = $value;
		$model->save(); 
                print_r($model->getErrors());
		Yii::app()->end();
	}
	
	
	/**
	 *
	 * @param type $id 
	 */
	public function actionProjectRemove($id)
	{
		$model = $this->loadModelByPk('Project', $id); 
		$model->delete();
		$this->redirect($this->createUrl('/project'));
	}
	
	
	
	public function actionDocumentAdd($id)
	{
		$model = $this->loadModelByPk('Project', $id);
		$document = new Document();
		$document->project_id = $model->getPrimaryKey();
		
		if($this->validateAndSaveModel($document))
			$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $model->getPrimaryKey(), 'tab' => 'Документация', '#' => 'project')));
		
		$this->render('documentadd', array(
			'model' => $model,
			'document' => $document,
		));
	}
	
	public function actionDocumentEdit()
	{
		$arr_id = Yii::app()->request->getParam('id');
		$document = $this->loadModelByPk('Document', $arr_id);
		$model = $this->loadModelByPk('Project', $arr_id['project_id']);
		
		if ($this->validateAndSaveModel($document))
			$this->refresh();
		
		$this->render('documentedit', array(
			'model' => $model,
			'document' => $document,
		));
	}
	
	public function actionDocumentRemove()
	{
		$arr_id = Yii::app()->request->getParam('id');
		$document = $this->loadModelByPk('Document', $arr_id);
		$document->delete();
		$this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $document->project_id, 'tab' => 'Документация', '#' => 'project')));
	}
	
	public function actionUpdateDocumentPrice()
	{
		$pk = Yii::app()->request->getParam('pk');
		$name = Yii::app()->request->getParam('name');
		$value = Yii::app()->request->getParam('value');
		$model = $this->loadModelByPk('Document', $pk);
		$model->{$name} = $value;
		$model->save();
		Yii::app()->end();
	}
	
	// метод выводит аккордион поставщиков с ценами
	public function actionAccordionProviders($city_id, $prices_id)
	{
		$data = Provider::model()->listProviders(intval($city_id));
		$pricesEntity = PricesEntity::model()->find( 'prices_id=:prices_id', array(':prices_id' => intval($prices_id)) );
		$provider_id = (is_object($pricesEntity)) ? $pricesEntity->provider_id : 0;
		if (count($data) > 0)
		{
			foreach ($data as $key => $value)
			{
				$prices = PricesEntity::model()->prices(intval($key));
				$class_in = ($key == $provider_id) ? 'in' : '';
				echo '
				<div class="accordion-group">
					<div class="accordion-heading">
						<a class="accordion-toggle" data-toggle="collapse" id="provider-'.$key.'" data-parent="#accordionProviders" href="#collapse'.$key.'">'.$value.'</a>
					</div>
					<div id="collapse'.$key.'" class="accordion-body collapse '.$class_in.'">
						<div class="accordion-inner">
				';
				if (count($prices) > 0)
				{
					$i = 0;
					foreach ($prices as $price)
					{
						$phtml = '<input id="price-'.$price->prices_id.'" type="hidden" value="'.$price->price.'" />';
						$phtml .= '<input id="for_provider_id-'.$price->prices_id.'" type="hidden" value="'.$key.'" />';
						$phtml .= '<span class="cost">'.$price->price.'</span>';
						$phtml .= '<span class="date">'.date(Yii::app()->params['phpDateFormat'], strtotime($price->date_create)).'</span>';
						$phtml .= '<span class="name">'.$price->entity->name.'</span>';
						$phtml .= '<span class="comment">'.(!empty($price->comment) ? '('.$price->comment.')' : '').'</span>';
						echo '<label class="radio">';
						$checked = ($price->prices_id == $prices_id) ? true : false;
						echo CHtml::radioButton('priceByProvider', $checked, array('id' => 'priceByProvider-'.$key.'-'.$i, 'value' => $price->prices_id));
						echo CHtml::label($phtml, 'priceByProvider-'.$key.'-'.$i);
						echo '</label>';
						$i++;
					}
				}
				else
					echo '<i>Нет результатов.</i>';
				echo '</div></div></div>';
			}
		}
		else
			echo '<i>Нет результатов.</i>';
		Yii::app()->end();
	}
        
	
	public function actionCitiesListFromProvider($region_id)
	{
		$listData = Provider::model()->listCities($region_id);
		$htmlOptions = array('empty' => '--Выбор города--');
		echo CHtml::listOptions(NULL, $listData, $htmlOptions);
	}
        
        /**
         * Добавить элемент мобилизации
         * @param int $id ИД проекта
         */
        public function actionMobiliztionadd($id) {
            $project = $this->loadModelByPk('Project', $id);
            
            $model = new EntityBMobilization();
            $model->project_id = $project->getPrimaryKey();
            
            if($this->validateAndSaveModel($model)) {
                GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, $project);
                $this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $project->getPrimaryKey(), 'tab' => 'Мобилизация', '#' => 'project')));
            }
            
            $this->render('mobiliztionadd', array(
			'model' => $model,
			'project' => $project,
	    ));
        }
        
        /**
         * Редактировать элемент мобилизации
         * @param int $id ИД моб
         */
        public function actionMobiliztionEdit($id) {
            $model = $this->loadModelByPk('EntityBMobilization', $id);
            $project = $this->loadModelByPk('Project', $model->project_id);
            if($this->validateAndSaveModel($model)) {
                GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, $project);
                $model->refresh();
            }
            $this->render('mobiliztionedit', array(
			'model' => $model,
			'project' => $project,
	    ));
        }
        
        /**
         * Удалить моб
         * @param int $id ИД моб
         */
        public function actionMobilizationRemove($id) {
            $model = $this->loadModelByPk('EntityBMobilization', $id);
            $model->delete();
            GlobalEventHandler::trigger(GlobalEventHandler::PROJECT_UPDATE_SUM, array('id'=>$model->project_id));
            $this->redirect($this->createUrl('/project/ProjectEdit', array('id' => $model->project_id, 'tab' => 'Мобилизация', '#' => 'project')));
        }
}
?>