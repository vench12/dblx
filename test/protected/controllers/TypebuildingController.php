<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TypeBuilding
 *
 * @author v.raskin
 */
class TypeBuildingController  extends BaseController {
 	/**
	 * Выводим все типы зданий.
	 */
	public function actionIndex() {
		$model = new TypeBuilding('search');
		if(isset($_REQUEST['TypeBuilding'])) {
			$model->setAttributes($_REQUEST['TypeBuilding']);
		}
		$dataProvider = $model->buildDataProvider();
		
		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Редактировать тип здания.
	 * @param integer $id 
	 */
	public function actionTypebuildingedit($id) {
		$model = $this->loadModelByPk('TypeBuilding', $id);
		if($this->validateAndSaveModel($model)) {
			$this->refresh();
		}
		$this->render('typebuildingedit', array(
			'model'=>$model, 
		));
	}
	
	/**
	 * Добавить тип здания.
	 */
	public function actionTypebuildingadd() {
		$model = new TypeBuilding();
		if($this->validateAndSaveModel($model)) {
			$this->redirect(array('/typebuilding/typebuildingedit', 'id'=>$model->getPrimaryKey()));
		}
		$this->render('typebuildingadd', array(
			'model'=>$model, 
		));
	}
	
	/**
	 * Удалить тип здания.
	* @param integer $id 
	*/	
	public function actionTypebuildingRemove($id) {
		$model = $this->loadModelByPk('TypeBuilding', $id);
		$model->delete();
		$this->redirect(array('/typebuilding'));
	}
}

?>
