<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TypeBuildingPlaceController
 *
 * @author v.raskin
 */
class TypeBuildingPlaceController  extends BaseController {
 	/**
	 * Выводим все типы площадок.
	 */
	public function actionIndex() {
		$model = new TypeBuildingPlace('search');
		if(isset($_REQUEST['TypeBuildingPlace'])) {
			$model->setAttributes($_REQUEST['TypeBuildingPlace']);
		}
		$dataProvider = $model->buildDataProvider();
		
		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Добавить тип площадки.
	 */
	public function actionTypebuildingplaceadd() {
		$model = new TypeBuildingPlace();
		if($this->validateAndSaveModel($model)) {
			$this->redirect(array('/typebuildingplace/typebuildingplaceedit', 'id'=>$model->getPrimaryKey()));
		}
		$this->render('typebuildingplaceadd', array(
			'model'=>$model, 
		));
	}
	
	/**
	 * Редактировать тип площадки.
	 * @param integer $id 
	 */
	public function actionTypebuildingplaceedit($id) {
		$model = $this->loadModelByPk('TypeBuildingPlace', $id);
		if($this->validateAndSaveModel($model)) {
			$this->refresh();
		}
		$this->render('typebuildingplaceedit', array(
			'model'=>$model, 
		));
	}
	
	/**
	 * Удалить тип площадки.
	 * @param integer $id 
	 */
	public function actionTypebuildingplaceRemove($id) {
		$model = $this->loadModelByPk('TypeBuildingPlace', $id);
		$model->delete();
		$this->redirect(array('/typebuildingplace'));
	}
}

?>
