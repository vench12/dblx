<?php

 

/**
 * Контроллер для работы с поставщиками.
 *
 * @author v.raskin
 */
class ProviderController extends BaseController
{
	/**
     *
     * @return array 
     */
    public function actions() {
        return array(
            'сitiesList'=>'application.components.actions.CitiesList', 
			'citiesOptions'=>'application.components.actions.CitiesOptions', 
        );
    }
	
	/**
	 *
	 * @param type $id 
	 */
	public function actionPriceEdit($id) {
		$model = $this->loadModelByPk('Provider', $id);
		$pid = Yii::app()->request->getParam('pid');
		$pricesEntity = $this->loadModelByPk('PricesEntity', $pid);
		if($this->validateAndSaveModel($pricesEntity)) {
			$this->redirect($this->createUrl('/provider/PriceEdit', array('id'=>$model->getPrimaryKey(), 'pid'=>$pricesEntity->getPrimaryKey() )));
		}
		
		$this->render('priceEdit', array(
			'model'=>$model,
			'pricesEntity'=>$pricesEntity,
		));
	}
	
	/**
	 * 
	 */
	public function actionPriceRemove() {
		$pid = Yii::app()->request->getParam('pid');
		$pricesEntity = $this->loadModelByPk('PricesEntity', $pid);
		$pricesEntity->delete();
		$this->redirect($this->createUrl('/provider/ProviderEdit', array('id'=>$pricesEntity->provider_id, 'tab' => 'Предложения')));
	}
	
	/**
	 * Добавить цену.
	 * @param integer $id ИД поставщика которому добавляем.
	 */
	public function actionPriceAdd($id) {
		$model = $this->loadModelByPk('Provider', $id);
		$pricesEntity = new PricesEntity();
		$pricesEntity->provider_id = $model->getPrimaryKey();
		
		if($this->validateAndSaveModel($pricesEntity)) {
			$this->redirect($this->createUrl('/provider/PriceEdit', array('id'=>$model->getPrimaryKey(), 'pid'=>$pricesEntity->getPrimaryKey() )));
		} 
		
		$this->render('priceAdd', array(
			'model'=>$model,
			'pricesEntity'=>$pricesEntity,
		));
	}
	
	/**
	 * Добавить нового поставщика. 
	 */
	public function actionProvideradd() {
		$model = new Provider();
		
		if($this->validateAndSaveModel($model)) {
			$this->redirect($this->createUrl('/provider/ProviderEdit', array('id'=>$model->getPrimaryKey())));
		}		 
		
		$this->render('provideradd', array(
			'model'=>$model,
		)); 
	}
	
	/**
	 * Редактируем запись поставщика.
	 * @param integer $id 
	 */
	public function actionProviderEdit($id) {
		$model = $this->loadModelByPk('Provider', $id);  
		 
		if($this->validateAndSaveModel($model)) {
			$this->refresh();
		}			 
		
		
		$pricesEntity = new PricesEntity('search');
		$pricesEntity->provider_id = $model->getPrimaryKey();
		
		$this->render('providerEdit', array(
			'model'=>$model,
			'pricesEntity'=>$pricesEntity,
		));
	}
	
	/**
	 * Удалить поставщика.
	 * @param integer $id 
	 */
	public function actionProviderRemove($id) {
		$model = $this->loadModelByPk('Provider', $id); 
		$model->delete();
		$this->redirect($this->createUrl('/provider'));
	}
	
	/**
	 * Вывод всех поставщиков.
	 */
	public function actionIndex() {
		$model = new Provider('search');
		if(isset($_GET['Provider']))
			$model->setAttributes($_GET['Provider']);
		
		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$model->buildDataProvider(),
		));
	}
	 
}

?>
