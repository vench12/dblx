<?php

class TypedocumentController extends BaseController
{
	public function actionIndex()
	{ 
		$model = new TypeDocument('search');
		
		if (isset($_GET['TypeDocument']))
			$model->setAttributes($_GET['TypeDocument']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionTypeDocumentAdd()
	{
		$model = new TypeDocument();
		
		if ($this->validateAndSaveModel($model))
			$this->redirect($this->createUrl('/typedocument/typedocumentedit', array('id' => $model->getPrimaryKey())));
		
		$this->render('typedocumentadd', array(
			'model' => $model,
		));
	}
	
	public function actionTypeDocumentEdit($id)
	{
		$model = $this->loadModelByPk('TypeDocument', $id); 
		
		if ($this->validateAndSaveModel($model))
			$this->refresh();
		
		$this->render('typedocumentedit', array(
			'model' => $model,
		));
	}
	
	public function actionTypeDocumentRemove($id)
	{
		$model = $this->loadModelByPk('TypeDocument', $id); 
		$model->delete();
		$this->redirect($this->createUrl('/typedocument'));
	}
}
?>