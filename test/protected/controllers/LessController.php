<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Компилятор less
 *
 * @author vench
 */
class LessController extends BaseController {
    
    /**
     * 
     */
    public function actionIndex() {          
         $lessDir = __DIR__."/../extensions/less/";
         $less = $lessDir."/bootstrap_y/less/bootstrap.less";
         $css = __DIR__."/../../themes/light/css/style.css";
         self::IncludeScripts( $lessDir.'/lib/Less' );         
          
         $options = array('compress'=>true);
         $parser = new Less_Parser( $options );
         $parser->parseFile($less);
	 $compiled = $parser->getCss();
         
         file_put_contents($css, $compiled);         
         $this->render('/site/index');
    }
    
    /**
	 * Include the necessary php files
	 *
	 */
	static function IncludeScripts( $dir ){

		$files = scandir($dir);

		usort($files,function($a,$b){
			return strlen($a)-strlen($b);
		});


		$dirs = array();
		foreach($files as $file){
			if( $file == '.' || $file == '..' ){
				continue;
			}

			$full_path = $dir.'/'.$file;
			if( is_dir($full_path) ){
				$dirs[] = $full_path;
				continue;
			}

			if( strpos($file,'.php') !== (strlen($file) - 4) ){
				continue;
			}

			include_once($full_path);
		}

		foreach($dirs as $dir){
			self::IncludeScripts( $dir );
		}

	}
}
