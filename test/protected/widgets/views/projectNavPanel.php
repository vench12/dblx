<?php
 
?>
<div id="nav_panel_project" class="btn-toolbar">
<div class="btn-group">

 <?php        
        if($currentAction == 'projectedit') {
            echo CHtml::link("Проект", "", array('class'=>'btn btn-primary btn-large active'));
        } else {
            echo CHtml::link("Проект", 
		Yii::app()->createUrl('/project/projectedit', array('id'=>$model->getPrimaryKey())),
		 array('class'=>'btn btn-primary btn-large'));
        }
?>
    
<?php        
        if($currentAction == 'buildingadd') {
            echo CHtml::link("Добавить здание", "", array('class'=>'btn btn-primary btn-large active'));
        } else {
            echo CHtml::link("Добавить здание", 
		Yii::app()->createUrl('/project/buildingAdd', array('id'=>$model->getPrimaryKey())),
		 array('class'=>'btn btn-primary btn-large'));
        }
?>
	
<?php
        if($currentAction == 'bdr') {
            echo CHtml::link("БДР", "", array('class'=>'btn btn-primary btn-large active'));
        } else {
            echo CHtml::link("БДР", 
		 Yii::app()->createUrl('/project/bdr', array('id'=>$model->getPrimaryKey())),
		 array('class'=>'btn btn-primary btn-large'));
        }    
?>
<?php
        if($currentAction == 'schedule') {
            echo CHtml::link("График", "", array('class'=>'btn btn-primary btn-large active'));
        } else {
            echo CHtml::link("График", 
		 Yii::app()->createUrl('/project/schedule', array('id'=>$model->getPrimaryKey())),
		 array('class'=>'btn btn-primary btn-large'));
        }    
?>
<?php
	echo CHtml::link("~Затраты на проект: ".Utilites::priceFormat($model->total_sum), 
		 "",
		 array('class'=>'btn  btn-large disabled', 'title'=>'',  ));
?>
 
</div>
</div>

 