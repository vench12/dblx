<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Виджет выводит панель навигации в проекте.
 *
 * @author v.raskin
 */
class ProjectNavPanel extends CWidget {
	/**
	 *
	 * @var Project 
	 */
	public $model;
	
	/**
	 * 
	 */
	public function run() { 
            
                $currentAction = strtolower(Yii::app()->controller->getAction()->getId());
                
		$this->render('projectNavPanel', array(
			'model'=>$this->model,
                        'currentAction'=>$currentAction,
		));
	}
}

?>
